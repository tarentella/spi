/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author DOSI
 */
@Entity
@Table(name = "AUTHENTIFICATION")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Authentification.findAll", query = "SELECT a FROM Authentification a"),
	@NamedQuery(name = "Authentification.findByIdConnection", query = "SELECT a FROM Authentification a WHERE a.idConnection = :idConnection"),
	@NamedQuery(name = "Authentification.findByRole", query = "SELECT a FROM Authentification a WHERE a.role = :role"),
	@NamedQuery(name = "Authentification.findByLoginConnection", query = "SELECT a FROM Authentification a WHERE a.loginConnection = :loginConnection"),
	@NamedQuery(name = "Authentification.findByPseudoConnection", query = "SELECT a FROM Authentification a WHERE a.pseudoConnection = :pseudoConnection"),
	@NamedQuery(name = "Authentification.findByMotPasse", query = "SELECT a FROM Authentification a WHERE a.motPasse = :motPasse") })
public class Authentification implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "ID_CONNECTION")
	private Long idConnection;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 64)
	@Column(name = "LOGIN_CONNECTION")
	private String loginConnection;

	@Size(max = 32)
	@Column(name = "MOT_PASSE")
	private String motPasse;

	@JoinColumn(name = "NO_ENSEIGNANT", referencedColumnName = "NO_ENSEIGNANT")
	@ManyToOne
	@JsonManagedReference(value = "auth-ens")
	private Enseignant noEnseignant;
	@JoinColumn(name = "NO_ETUDIANT", referencedColumnName = "NO_ETUDIANT")
	@ManyToOne
	private Etudiant noEtudiant;
	@Size(max = 240)
	@Column(name = "PSEUDO_CONNECTION")
	private String pseudoConnection;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 5)
	@Column(name = "ROLE")
	private String role;

	public Authentification() {
	}

	public Authentification(final Long idConnection) {
		this.idConnection = idConnection;
	}

	public Authentification(final Long idConnection, final String role, final String loginConnection) {
		this.idConnection = idConnection;
		this.role = role;
		this.loginConnection = loginConnection;
	}

	@Override
	public boolean equals(final Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Authentification)) {
			return false;
		}
		final Authentification other = (Authentification) object;
		if (this.idConnection == null && other.idConnection != null || this.idConnection != null && !this.idConnection.equals(other.idConnection)) {
			return false;
		}
		return true;
	}

	public Long getIdConnection() {
		return idConnection;
	}

	public String getLoginConnection() {
		return loginConnection;
	}

	public String getMotPasse() {
		return motPasse;
	}

	public Enseignant getNoEnseignant() {
		return noEnseignant;
	}

	public Etudiant getNoEtudiant() {
		return noEtudiant;
	}

	public String getPseudoConnection() {
		return pseudoConnection;
	}

	public String getRole() {
		return role;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += idConnection != null ? idConnection.hashCode() : 0;
		return hash;
	}

	public void setIdConnection(final Long idConnection) {
		this.idConnection = idConnection;
	}

	public void setLoginConnection(final String loginConnection) {
		this.loginConnection = loginConnection;
	}

	public void setMotPasse(final String motPasse) {
		this.motPasse = motPasse;
	}

	public void setNoEnseignant(final Enseignant noEnseignant) {
		this.noEnseignant = noEnseignant;
	}

	public void setNoEtudiant(final Etudiant noEtudiant) {
		this.noEtudiant = noEtudiant;
	}

	public void setPseudoConnection(final String pseudoConnection) {
		this.pseudoConnection = pseudoConnection;
	}

	public void setRole(final String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "fr.univbrest.dosi.spi.bean.Authentification[ idConnection=" + idConnection + " ]";
	}

}
