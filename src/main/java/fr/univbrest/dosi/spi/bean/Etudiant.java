/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author DOSI
 */
@Entity
@Table(name = "ETUDIANT")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Etudiant.findAll", query = "SELECT e FROM Etudiant e"),
	@NamedQuery(name = "Etudiant.findByNoEtudiant", query = "SELECT e FROM Etudiant e WHERE e.noEtudiant = :noEtudiant"),
	@NamedQuery(name = "Etudiant.findByNom", query = "SELECT e FROM Etudiant e WHERE e.nom = :nom"),
	@NamedQuery(name = "Etudiant.findByPrenom", query = "SELECT e FROM Etudiant e WHERE e.prenom = :prenom"),
	@NamedQuery(name = "Etudiant.findBySexe", query = "SELECT e FROM Etudiant e WHERE e.sexe = :sexe"),
	@NamedQuery(name = "Etudiant.findByDateNaissance", query = "SELECT e FROM Etudiant e WHERE e.dateNaissance = :dateNaissance"),
	@NamedQuery(name = "Etudiant.findByLieuNaissance", query = "SELECT e FROM Etudiant e WHERE e.lieuNaissance = :lieuNaissance"),
	@NamedQuery(name = "Etudiant.findByNationalite", query = "SELECT e FROM Etudiant e WHERE e.nationalite = :nationalite"),
	@NamedQuery(name = "Etudiant.findByTelephone", query = "SELECT e FROM Etudiant e WHERE e.telephone = :telephone"),
	@NamedQuery(name = "Etudiant.findByMobile", query = "SELECT e FROM Etudiant e WHERE e.mobile = :mobile"),
	@NamedQuery(name = "Etudiant.findByEmail", query = "SELECT e FROM Etudiant e WHERE e.email = :email"),
	@NamedQuery(name = "Etudiant.findByEmailUbo", query = "SELECT e FROM Etudiant e WHERE e.emailUbo = :emailUbo"),
	@NamedQuery(name = "Etudiant.findByAdresse", query = "SELECT e FROM Etudiant e WHERE e.adresse = :adresse"),
	@NamedQuery(name = "Etudiant.findByCodePostal", query = "SELECT e FROM Etudiant e WHERE e.codePostal = :codePostal"),
	@NamedQuery(name = "Etudiant.findByVille", query = "SELECT e FROM Etudiant e WHERE e.ville = :ville"),
	@NamedQuery(name = "Etudiant.findByPaysOrigine", query = "SELECT e FROM Etudiant e WHERE e.paysOrigine = :paysOrigine"),
	@NamedQuery(name = "Etudiant.findByUniversiteOrigine", query = "SELECT e FROM Etudiant e WHERE e.universiteOrigine = :universiteOrigine"),
	@NamedQuery(name = "Etudiant.findByGroupeTp", query = "SELECT e FROM Etudiant e WHERE e.groupeTp = :groupeTp"),
	@NamedQuery(name = "Etudiant.findByGroupeAnglais", query = "SELECT e FROM Etudiant e WHERE e.groupeAnglais = :groupeAnglais") })
public class Etudiant implements Serializable {
	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "ADRESSE")
	private String adresse;
	@OneToMany(mappedBy = "noEtudiant", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<Authentification> authentificationCollection;
	@Size(max = 10)
	@Column(name = "CODE_POSTAL")
	private String codePostal;
	@Basic(optional = false)
	@NotNull
	@Column(name = "DATE_NAISSANCE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateNaissance;
	// @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field
	// contains email address consider using this annotation to enforce field validation
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "EMAIL")
	private String email;
	@Size(max = 255)
	@Column(name = "EMAIL_UBO")
	private String emailUbo;
	@Column(name = "GROUPE_ANGLAIS")
	private BigInteger groupeAnglais;
	@Column(name = "GROUPE_TP")
	private BigInteger groupeTp;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "LIEU_NAISSANCE")
	private String lieuNaissance;
	@Size(max = 20)
	@Column(name = "MOBILE")
	private String mobile;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 50)
	@Column(name = "NATIONALITE")
	private String nationalite;
	@Id
	@Basic(optional = false)
	@Size(min = 1, max = 50)
	@NotNull
	@Column(name = "NO_ETUDIANT")
	private String noEtudiant;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 50)
	@Column(name = "NOM")
	private String nom;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 5)
	@Column(name = "PAYS_ORIGINE")
	private String paysOrigine;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 50)
	@Column(name = "PRENOM")
	private String prenom;
	@JoinColumns({ @JoinColumn(name = "ANNEE_UNIVERSITAIRE", referencedColumnName = "ANNEE_UNIVERSITAIRE"), @JoinColumn(name = "CODE_FORMATION", referencedColumnName = "CODE_FORMATION") })
	@ManyToOne(optional = false)
	private Promotion promotion;
	@OneToMany(mappedBy = "noEtudiant", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<ReponseEvaluation> reponseEvaluationCollection;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 1)
	@Column(name = "SEXE")
	private String sexe;
	@Size(max = 20)
	@Column(name = "TELEPHONE")
	private String telephone;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 6)
	@Column(name = "UNIVERSITE_ORIGINE")
	private String universiteOrigine;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "VILLE")
	private String ville;

	public Etudiant() {
	}

	public Etudiant(final String noEtudiant) {
		this.noEtudiant = noEtudiant;
	}

	public Etudiant(final String noEtudiant, final String nom, final String prenom, final String sexe, final Date dateNaissance, final String lieuNaissance, final String nationalite,
			final String email, final String adresse, final String ville, final String paysOrigine, final String universiteOrigine) {
		this.noEtudiant = noEtudiant;
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
		this.dateNaissance = dateNaissance;
		this.lieuNaissance = lieuNaissance;
		this.nationalite = nationalite;
		this.email = email;
		this.adresse = adresse;
		this.ville = ville;
		this.paysOrigine = paysOrigine;
		this.universiteOrigine = universiteOrigine;
	}

	@Override
	public boolean equals(final Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Etudiant)) {
			return false;
		}
		final Etudiant other = (Etudiant) object;
		if (this.noEtudiant == null && other.noEtudiant != null || this.noEtudiant != null && !this.noEtudiant.equals(other.noEtudiant)) {
			return false;
		}
		return true;
	}

	public String getAdresse() {
		return adresse;
	}

	@XmlTransient
	public Collection<Authentification> getAuthentificationCollection() {
		return authentificationCollection;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public String getEmail() {
		return email;
	}

	public String getEmailUbo() {
		return emailUbo;
	}

	public BigInteger getGroupeAnglais() {
		return groupeAnglais;
	}

	public BigInteger getGroupeTp() {
		return groupeTp;
	}

	public String getLieuNaissance() {
		return lieuNaissance;
	}

	public String getMobile() {
		return mobile;
	}

	public String getNationalite() {
		return nationalite;
	}

	public String getNoEtudiant() {
		return noEtudiant;
	}

	public String getNom() {
		return nom;
	}

	public String getPaysOrigine() {
		return paysOrigine;
	}

	public String getPrenom() {
		return prenom;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	@XmlTransient
	public Collection<ReponseEvaluation> getReponseEvaluationCollection() {
		return reponseEvaluationCollection;
	}

	public String getSexe() {
		return sexe;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getUniversiteOrigine() {
		return universiteOrigine;
	}

	public String getVille() {
		return ville;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += noEtudiant != null ? noEtudiant.hashCode() : 0;
		return hash;
	}

	public void setAdresse(final String adresse) {
		this.adresse = adresse;
	}

	public void setAuthentificationCollection(final Collection<Authentification> authentificationCollection) {
		this.authentificationCollection = authentificationCollection;
	}

	public void setCodePostal(final String codePostal) {
		this.codePostal = codePostal;
	}

	public void setDateNaissance(final Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public void setEmailUbo(final String emailUbo) {
		this.emailUbo = emailUbo;
	}

	public void setGroupeAnglais(final BigInteger groupeAnglais) {
		this.groupeAnglais = groupeAnglais;
	}

	public void setGroupeTp(final BigInteger groupeTp) {
		this.groupeTp = groupeTp;
	}

	public void setLieuNaissance(final String lieuNaissance) {
		this.lieuNaissance = lieuNaissance;
	}

	public void setMobile(final String mobile) {
		this.mobile = mobile;
	}

	public void setNationalite(final String nationalite) {
		this.nationalite = nationalite;
	}

	public void setNoEtudiant(final String noEtudiant) {
		this.noEtudiant = noEtudiant;
	}

	public void setNom(final String nom) {
		this.nom = nom;
	}

	public void setPaysOrigine(final String paysOrigine) {
		this.paysOrigine = paysOrigine;
	}

	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	public void setPromotion(final Promotion promotion) {
		this.promotion = promotion;
	}

	public void setReponseEvaluationCollection(final Collection<ReponseEvaluation> reponseEvaluationCollection) {
		this.reponseEvaluationCollection = reponseEvaluationCollection;
	}

	public void setSexe(final String sexe) {
		this.sexe = sexe;
	}

	public void setTelephone(final String telephone) {
		this.telephone = telephone;
	}

	public void setUniversiteOrigine(final String universiteOrigine) {
		this.universiteOrigine = universiteOrigine;
	}

	public void setVille(final String ville) {
		this.ville = ville;
	}

	@Override
	public String toString() {
		return "fr.univbrest.dosi.spi.bean.Etudiant[ noEtudiant=" + noEtudiant + " ]";
	}

}
