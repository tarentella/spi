package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author DOSI
 */
@Entity
@Table(name = "EVALUATION")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Evaluation.findAll", query = "SELECT e FROM Evaluation e"),
	@NamedQuery(name = "Evaluation.findByIdEvaluation", query = "SELECT e FROM Evaluation e WHERE e.idEvaluation = :idEvaluation"),
	@NamedQuery(name = "Evaluation.findByNoEvaluation", query = "SELECT e FROM Evaluation e WHERE e.noEvaluation = :noEvaluation"),
	@NamedQuery(name = "Evaluation.findByDesignation", query = "SELECT e FROM Evaluation e WHERE e.designation = :designation"),
	@NamedQuery(name = "Evaluation.findByEtat", query = "SELECT e FROM Evaluation e WHERE e.etat = :etat"),
	@NamedQuery(name = "Evaluation.findByPeriode", query = "SELECT e FROM Evaluation e WHERE e.periode = :periode"),
	@NamedQuery(name = "Evaluation.findByDebutReponse", query = "SELECT e FROM Evaluation e WHERE e.debutReponse = :debutReponse"),
	@NamedQuery(name = "Evaluation.findByFinReponse", query = "SELECT e FROM Evaluation e WHERE e.finReponse = :finReponse") })
public class Evaluation implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "ANNEE_UNIVERSITAIRE", nullable = false)
	private String annee;
	@Column(name = "CODE_EC")
	private String code_ec;

	@Column(name = "CODE_FORMATION", nullable = false)
	private String code_formation;

	@Column(name = "CODE_UE", nullable = false)
	private String code_ue;

	@Basic(optional = false)
	@Column(name = "DEBUT_REPONSE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date debutReponse;

	@Basic(optional = false)
	@Size(min = 1, max = 16)
	@Column(name = "DESIGNATION")
	private String designation;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluation", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<Droit> droitCollection;

	// @JoinColumns({ @JoinColumn(name = "CODE_FORMATION", referencedColumnName = "CODE_FORMATION", insertable = false, updatable = false),
	// @JoinColumn(name = "CODE_UE", referencedColumnName = "CODE_UE", insertable = false, updatable = false),
	// @JoinColumn(name = "CODE_EC", referencedColumnName = "CODE_EC", insertable = false, updatable = false) })
	// @ManyToOne(optional = false)
	// @JsonIgnore
	// private ElementConstitutif elementConstitutif;
	@Basic(optional = false)
	@Size(min = 1, max = 3)
	@Column(name = "ETAT")
	private String etat;

	@Basic(optional = false)
	@Column(name = "FIN_REPONSE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date finReponse;

	@Id
	@Basic(optional = false)
	@SequenceGenerator(name = "EVE_SEQ", sequenceName = "EVE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVE_SEQ")
	@Column(name = "ID_EVALUATION")
	private Long idEvaluation;
	@JoinColumn(name = "NO_ENSEIGNANT", referencedColumnName = "NO_ENSEIGNANT")
	@ManyToOne(optional = false)
	private Enseignant noEnseignant;
	@Basic(optional = false)
	@NotNull
	@Column(name = "NO_EVALUATION")
	private short noEvaluation;
	@Size(max = 64)
	@Column(name = "PERIODE")
	private String periode;
	// @JoinColumns({ @JoinColumn(name = "ANNEE_UNIVERSITAIRE", referencedColumnName = "ANNEE_UNIVERSITAIRE", insertable = true, updatable = false),
	// @JoinColumn(name = "CODE_FORMATION", referencedColumnName = "CODE_FORMATION", insertable = true, updatable = false) })
	// @ManyToOne(optional = false)
	// private Promotion promotion;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idEvaluation", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<ReponseEvaluation> reponseEvaluationCollection;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idEvaluation", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<RubriqueEvaluation> rubriqueEvaluationCollection;

	public Evaluation() {
	}

	public Evaluation(final Long idEvaluation) {
		this.idEvaluation = idEvaluation;
	}

	public Evaluation(final Long idEvaluation, final short noEvaluation, final String designation, final String etat, final Date debutReponse, final Date finReponse) {
		this.idEvaluation = idEvaluation;
		this.noEvaluation = noEvaluation;
		this.designation = designation;
		this.etat = etat;
		this.debutReponse = debutReponse;
		this.finReponse = finReponse;
	}

	@Override
	public boolean equals(final Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Evaluation)) {
			return false;
		}
		final Evaluation other = (Evaluation) object;
		if (this.idEvaluation == null && other.idEvaluation != null || this.idEvaluation != null && !this.idEvaluation.equals(other.idEvaluation)) {
			return false;
		}
		return true;
	}

	public String getAnnee() {
		return annee;
	}

	public String getCode_ec() {
		return code_ec;
	}

	public String getCode_formation() {
		return code_formation;
	}

	public String getCode_ue() {
		return code_ue;
	}

	// @JoinColumns({ @JoinColumn(name = "CODE_FORMATION", referencedColumnName = "CODE_FORMATION", insertable = false, updatable = false),
	// @JoinColumn(name = "CODE_UE", referencedColumnName = "CODE_UE", insertable = false, updatable = false) })
	// @ManyToOne(optional = false)
	// @JsonIgnore
	// private UniteEnseignement uniteEnseignement;

	public Date getDebutReponse() {
		return debutReponse;
	}

	public String getDesignation() {
		return designation;
	}

	@XmlTransient
	public Collection<Droit> getDroitCollection() {
		return droitCollection;
	}

	public String getEtat() {
		return etat;
	}

	public Date getFinReponse() {
		return finReponse;
	}

	public Long getIdEvaluation() {
		return idEvaluation;
	}

	public Enseignant getNoEnseignant() {
		return noEnseignant;
	}

	public short getNoEvaluation() {
		return noEvaluation;
	}

	public String getPeriode() {
		return periode;
	}

	@XmlTransient
	public Collection<ReponseEvaluation> getReponseEvaluationCollection() {
		return reponseEvaluationCollection;
	}

	@XmlTransient
	public Collection<RubriqueEvaluation> getRubriqueEvaluationCollection() {
		return rubriqueEvaluationCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += idEvaluation != null ? idEvaluation.hashCode() : 0;
		return hash;
	}

	public void setAnnee(final String annee) {
		this.annee = annee;
	}

	public void setCode_ec(final String code_ec) {
		this.code_ec = code_ec;
	}

	public void setCode_formation(final String code_formation) {
		this.code_formation = code_formation;
	}

	public void setCode_ue(final String code_ue) {
		this.code_ue = code_ue;
	}

	public void setDebutReponse(final Date debutReponse) {
		this.debutReponse = debutReponse;
	}

	public void setDesignation(final String designation) {
		this.designation = designation;
	}

	public void setDroitCollection(final Collection<Droit> droitCollection) {
		this.droitCollection = droitCollection;
	}

	public void setEtat(final String etat) {
		this.etat = etat;
	}

	public void setFinReponse(final Date finReponse) {
		this.finReponse = finReponse;
	}

	public void setIdEvaluation(final Long idEvaluation) {
		this.idEvaluation = idEvaluation;
	}

	public void setNoEnseignant(final Enseignant noEnseignant) {
		this.noEnseignant = noEnseignant;
	}

	public void setNoEvaluation(final short noEvaluation) {
		this.noEvaluation = noEvaluation;
	}

	public void setPeriode(final String periode) {
		this.periode = periode;
	}

	public void setReponseEvaluationCollection(final Collection<ReponseEvaluation> reponseEvaluationCollection) {
		this.reponseEvaluationCollection = reponseEvaluationCollection;
	}

	public void setRubriqueEvaluationCollection(final Collection<RubriqueEvaluation> rubriqueEvaluationCollection) {
		this.rubriqueEvaluationCollection = rubriqueEvaluationCollection;
	}

	@Override
	public String toString() {
		return "fr.univbrest.dosi.spi.bean.Evaluation[ idEvaluation=" + idEvaluation + " ]";
	}

}