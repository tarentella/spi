/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author DOSI
 */
@Entity
@Table(name = "FORMATION")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Formation.findAll", query = "SELECT f FROM Formation f"),
		@NamedQuery(name = "Formation.findByCodeFormation", query = "SELECT f FROM Formation f WHERE f.codeFormation = :codeFormation"),
		@NamedQuery(name = "Formation.findByDiplome", query = "SELECT f FROM Formation f WHERE f.diplome = :diplome"),
		@NamedQuery(name = "Formation.findByN0Annee", query = "SELECT f FROM Formation f WHERE f.n0Annee = :n0Annee"),
		@NamedQuery(name = "Formation.findByNomFormation", query = "SELECT f FROM Formation f WHERE f.nomFormation = :nomFormation"),
		@NamedQuery(name = "Formation.findByDoubleDiplome", query = "SELECT f FROM Formation f WHERE f.doubleDiplome = :doubleDiplome"),
		@NamedQuery(name = "Formation.findByDebutAccreditation", query = "SELECT f FROM Formation f WHERE f.debutAccreditation = :debutAccreditation"),
		@NamedQuery(name = "Formation.findByFinAccreditation", query = "SELECT f FROM Formation f WHERE f.finAccreditation = :finAccreditation") })
public class Formation implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 8)
	@Column(name = "CODE_FORMATION")
	private String codeFormation;
	@Column(name = "DEBUT_ACCREDITATION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date debutAccreditation;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 3)
	@Column(name = "DIPLOME")
	private String diplome;
	@Basic(optional = false)
	@NotNull
	@Column(name = "DOUBLE_DIPLOME")
	private Character doubleDiplome;
	@Column(name = "FIN_ACCREDITATION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date finAccreditation;
	@Basic(optional = false)
	@NotNull
	@Column(name = "N0_ANNEE")
	private short n0Annee;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 64)
	@Column(name = "NOM_FORMATION")
	private String nomFormation;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "formation", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<Promotion> promotionCollection;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "formation", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<UniteEnseignement> uniteEnseignementCollection;

	public Formation() {
	}

	public Formation(final String codeFormation) {
		this.codeFormation = codeFormation;
	}

	public Formation(final String codeFormation, final String diplome, final short n0Annee, final String nomFormation, final Character doubleDiplome) {
		this.codeFormation = codeFormation;
		this.diplome = diplome;
		this.n0Annee = n0Annee;
		this.nomFormation = nomFormation;
		this.doubleDiplome = doubleDiplome;
	}

	@Override
	public boolean equals(final Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Formation)) {
			return false;
		}
		final Formation other = (Formation) object;
		if (this.codeFormation == null && other.codeFormation != null || this.codeFormation != null && !this.codeFormation.equals(other.codeFormation)) {
			return false;
		}
		return true;
	}

	public String getCodeFormation() {
		return codeFormation;
	}

	public Date getDebutAccreditation() {
		return debutAccreditation;
	}

	public String getDiplome() {
		return diplome;
	}

	public Character getDoubleDiplome() {
		return doubleDiplome;
	}

	public Date getFinAccreditation() {
		return finAccreditation;
	}

	public short getN0Annee() {
		return n0Annee;
	}

	public String getNomFormation() {
		return nomFormation;
	}

	@XmlTransient
	public Collection<Promotion> getPromotionCollection() {
		return promotionCollection;
	}

	@XmlTransient
	public Collection<UniteEnseignement> getUniteEnseignementCollection() {
		return uniteEnseignementCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += codeFormation != null ? codeFormation.hashCode() : 0;
		return hash;
	}

	public void setCodeFormation(final String codeFormation) {
		this.codeFormation = codeFormation;
	}

	public void setDebutAccreditation(final Date debutAccreditation) {
		this.debutAccreditation = debutAccreditation;
	}

	public void setDiplome(final String diplome) {
		this.diplome = diplome;
	}

	public void setDoubleDiplome(final Character doubleDiplome) {
		this.doubleDiplome = doubleDiplome;
	}

	public void setFinAccreditation(final Date finAccreditation) {
		this.finAccreditation = finAccreditation;
	}

	public void setN0Annee(final short n0Annee) {
		this.n0Annee = n0Annee;
	}

	public void setNomFormation(final String nomFormation) {
		this.nomFormation = nomFormation;
	}

	public void setPromotionCollection(final Collection<Promotion> promotionCollection) {
		this.promotionCollection = promotionCollection;
	}

	public void setUniteEnseignementCollection(final Collection<UniteEnseignement> uniteEnseignementCollection) {
		this.uniteEnseignementCollection = uniteEnseignementCollection;
	}

	@Override
	public String toString() {
		return "fr.univbrest.dosi.spi.bean.Formation[ codeFormation=" + codeFormation + " ]";
	}

}
