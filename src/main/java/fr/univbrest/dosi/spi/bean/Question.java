/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author DOSI
 */
@Entity
@Table(name = "QUESTION")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Question.findAll", query = "SELECT q FROM Question q"),
		@NamedQuery(name = "Question.findByIdQuestion", query = "SELECT q FROM Question q WHERE q.idQuestion = :idQuestion"),
		@NamedQuery(name = "Question.findByType", query = "SELECT q FROM Question q WHERE q.type = :type"),
		@NamedQuery(name = "Question.findByIntitule", query = "SELECT q FROM Question q WHERE q.intitule = :intitule") })
public class Question implements Serializable {
	private static final long serialVersionUID = 1L;
	@JoinColumn(name = "ID_QUALIFICATIF", referencedColumnName = "ID_QUALIFICATIF")
	@ManyToOne(optional = false)
	// @JsonManagedReference(value="qua-que")
	private Qualificatif idQualificatif;
	@Id
	@Basic(optional = false)
	@SequenceGenerator(name = "QUE_SEQ", sequenceName = "QUE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUE_SEQ")
	@Column(name = "ID_QUESTION")
	private Long idQuestion;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 64)
	@Column(name = "INTITULE")
	private String intitule;
	@JoinColumn(name = "NO_ENSEIGNANT", referencedColumnName = "NO_ENSEIGNANT")
	@ManyToOne
	private Enseignant noEnseignant;
	@OneToMany(mappedBy = "idQuestion", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<QuestionEvaluation> questionEvaluationCollection;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "question", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<RubriqueQuestion> rubriqueQuestionCollection;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 10)
	@Column(name = "TYPE")
	private String type;

	public Question() {
	}

	public Question(final Long idQuestion) {
		this.idQuestion = idQuestion;
	}

	public Question(final Long idQuestion, final String type, final String intitule) {
		this.idQuestion = idQuestion;
		this.type = type;
		this.intitule = intitule;
	}

	@Override
	public boolean equals(final Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Question)) {
			return false;
		}
		final Question other = (Question) object;
		if (this.idQuestion == null && other.idQuestion != null || this.idQuestion != null && !this.idQuestion.equals(other.idQuestion)) {
			return false;
		}
		return true;
	}

	public Qualificatif getIdQualificatif() {
		return idQualificatif;
	}

	public Long getIdQuestion() {
		return idQuestion;
	}

	public String getIntitule() {
		return intitule;
	}

	public Enseignant getNoEnseignant() {
		return noEnseignant;
	}

	@XmlTransient
	public Collection<QuestionEvaluation> getQuestionEvaluationCollection() {
		return questionEvaluationCollection;
	}

	@XmlTransient
	public Collection<RubriqueQuestion> getRubriqueQuestionCollection() {
		return rubriqueQuestionCollection;
	}

	public String getType() {
		return type;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += idQuestion != null ? idQuestion.hashCode() : 0;
		return hash;
	}

	public void setIdQualificatif(final Qualificatif idQualificatif) {
		this.idQualificatif = idQualificatif;
	}

	public void setIdQuestion(final Long idQuestion) {
		this.idQuestion = idQuestion;
	}

	public void setIntitule(final String intitule) {
		this.intitule = intitule;
	}

	public void setNoEnseignant(final Enseignant noEnseignant) {
		this.noEnseignant = noEnseignant;
	}

	public void setQuestionEvaluationCollection(final Collection<QuestionEvaluation> questionEvaluationCollection) {
		this.questionEvaluationCollection = questionEvaluationCollection;
	}

	public void setRubriqueQuestionCollection(final Collection<RubriqueQuestion> rubriqueQuestionCollection) {
		this.rubriqueQuestionCollection = rubriqueQuestionCollection;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "fr.univbrest.dosi.spi.bean.Question[ idQuestion=" + idQuestion + " ]";
	}

}
