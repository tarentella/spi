/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author DOSI
 */
@Entity
@Table(name = "REPONSE_EVALUATION")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "ReponseEvaluation.findAll", query = "SELECT r FROM ReponseEvaluation r"),
		@NamedQuery(name = "ReponseEvaluation.findByIdReponseEvaluation", query = "SELECT r FROM ReponseEvaluation r WHERE r.idReponseEvaluation = :idReponseEvaluation"),
		@NamedQuery(name = "ReponseEvaluation.findByCommentaire", query = "SELECT r FROM ReponseEvaluation r WHERE r.commentaire = :commentaire"),
		@NamedQuery(name = "ReponseEvaluation.findByNom", query = "SELECT r FROM ReponseEvaluation r WHERE r.nom = :nom"),
		@NamedQuery(name = "ReponseEvaluation.findByPrenom", query = "SELECT r FROM ReponseEvaluation r WHERE r.prenom = :prenom") })
public class ReponseEvaluation implements Serializable {
	private static final long serialVersionUID = 1L;
	@Size(max = 512)
	@Column(name = "COMMENTAIRE")
	private String commentaire;
	@JoinColumn(name = "ID_EVALUATION", referencedColumnName = "ID_EVALUATION")
	@ManyToOne(optional = false)
	@JsonManagedReference(value = "eve-reve")
	private Evaluation idEvaluation;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "ID_REPONSE_EVALUATION")
	private Long idReponseEvaluation;
	@JoinColumn(name = "NO_ETUDIANT", referencedColumnName = "NO_ETUDIANT")
	@ManyToOne
	private Etudiant noEtudiant;
	@Size(max = 32)
	@Column(name = "NOM")
	private String nom;
	@Size(max = 32)
	@Column(name = "PRENOM")
	private String prenom;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "reponseEvaluation", fetch = FetchType.LAZY)
	@JsonBackReference(value = "reve-rque")
	private Collection<ReponseQuestion> reponseQuestionCollection;

	public ReponseEvaluation() {
	}

	public ReponseEvaluation(final Long idReponseEvaluation) {
		this.idReponseEvaluation = idReponseEvaluation;
	}

	@Override
	public boolean equals(final Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof ReponseEvaluation)) {
			return false;
		}
		final ReponseEvaluation other = (ReponseEvaluation) object;
		if (this.idReponseEvaluation == null && other.idReponseEvaluation != null || this.idReponseEvaluation != null && !this.idReponseEvaluation.equals(other.idReponseEvaluation)) {
			return false;
		}
		return true;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public Evaluation getIdEvaluation() {
		return idEvaluation;
	}

	public Long getIdReponseEvaluation() {
		return idReponseEvaluation;
	}

	public Etudiant getNoEtudiant() {
		return noEtudiant;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	@XmlTransient
	public Collection<ReponseQuestion> getReponseQuestionCollection() {
		return reponseQuestionCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += idReponseEvaluation != null ? idReponseEvaluation.hashCode() : 0;
		return hash;
	}

	public void setCommentaire(final String commentaire) {
		this.commentaire = commentaire;
	}

	public void setIdEvaluation(final Evaluation idEvaluation) {
		this.idEvaluation = idEvaluation;
	}

	public void setIdReponseEvaluation(final Long idReponseEvaluation) {
		this.idReponseEvaluation = idReponseEvaluation;
	}

	public void setNoEtudiant(final Etudiant noEtudiant) {
		this.noEtudiant = noEtudiant;
	}

	public void setNom(final String nom) {
		this.nom = nom;
	}

	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	public void setReponseQuestionCollection(final Collection<ReponseQuestion> reponseQuestionCollection) {
		this.reponseQuestionCollection = reponseQuestionCollection;
	}

	@Override
	public String toString() {
		return "fr.univbrest.dosi.spi.bean.ReponseEvaluation[ idReponseEvaluation=" + idReponseEvaluation + " ]";
	}

}
