/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author DOSI
 */
@Entity
@Table(name = "RUBRIQUE_EVALUATION")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "RubriqueEvaluation.findAll", query = "SELECT r FROM RubriqueEvaluation r"),
	@NamedQuery(name = "RubriqueEvaluation.findByIdRubriqueEvaluation", query = "SELECT r FROM RubriqueEvaluation r WHERE r.idRubriqueEvaluation = :idRubriqueEvaluation"),
	@NamedQuery(name = "RubriqueEvaluation.findByOrdre", query = "SELECT r FROM RubriqueEvaluation r WHERE r.ordre = :ordre"),
	@NamedQuery(name = "RubriqueEvaluation.findByDesignation", query = "SELECT r FROM RubriqueEvaluation r WHERE r.designation = :designation") })
public class RubriqueEvaluation implements Serializable {
	private static final long serialVersionUID = 1L;
	@Size(max = 64)
	@Column(name = "DESIGNATION")
	private String designation;
	@JoinColumn(name = "ID_EVALUATION", referencedColumnName = "ID_EVALUATION")
	@ManyToOne(optional = false)
	private Evaluation idEvaluation;
	@JoinColumn(name = "ID_RUBRIQUE", referencedColumnName = "ID_RUBRIQUE")
	@ManyToOne
	private Rubrique idRubrique;
	@Id
	@Basic(optional = false)
	@Column(name = "ID_RUBRIQUE_EVALUATION")
	@SequenceGenerator(name = "REV_SEQ", sequenceName = "REV_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REV_SEQ")
	private Long idRubriqueEvaluation;
	@Basic(optional = false)
	@NotNull
	@Column(name = "ORDRE")
	private short ordre;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idRubriqueEvaluation", fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<QuestionEvaluation> questionEvaluationCollection;

	public RubriqueEvaluation() {
	}

	public RubriqueEvaluation(final Long idRubriqueEvaluation) {
		this.idRubriqueEvaluation = idRubriqueEvaluation;
	}

	public RubriqueEvaluation(final Long idRubriqueEvaluation, final short ordre) {
		this.idRubriqueEvaluation = idRubriqueEvaluation;
		this.ordre = ordre;
	}

	@Override
	public boolean equals(final Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof RubriqueEvaluation)) {
			return false;
		}
		final RubriqueEvaluation other = (RubriqueEvaluation) object;
		if (this.idRubriqueEvaluation == null && other.idRubriqueEvaluation != null || this.idRubriqueEvaluation != null && !this.idRubriqueEvaluation.equals(other.idRubriqueEvaluation)) {
			return false;
		}
		return true;
	}

	public String getDesignation() {
		return designation;
	}

	public Evaluation getIdEvaluation() {
		return idEvaluation;
	}

	public Rubrique getIdRubrique() {
		return idRubrique;
	}

	public Long getIdRubriqueEvaluation() {
		return idRubriqueEvaluation;
	}

	public short getOrdre() {
		return ordre;
	}

	@XmlTransient
	public Collection<QuestionEvaluation> getQuestionEvaluationCollection() {
		return questionEvaluationCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += idRubriqueEvaluation != null ? idRubriqueEvaluation.hashCode() : 0;
		return hash;
	}

	public void setDesignation(final String designation) {
		this.designation = designation;
	}

	public void setIdEvaluation(final Evaluation idEvaluation) {
		this.idEvaluation = idEvaluation;
	}

	public void setIdRubrique(final Rubrique idRubrique) {
		this.idRubrique = idRubrique;
	}

	public void setIdRubriqueEvaluation(final Long idRubriqueEvaluation) {
		this.idRubriqueEvaluation = idRubriqueEvaluation;
	}

	public void setOrdre(final short ordre) {
		this.ordre = ordre;
	}

	public void setQuestionEvaluationCollection(final Collection<QuestionEvaluation> questionEvaluationCollection) {
		this.questionEvaluationCollection = questionEvaluationCollection;
	}

	@Override
	public String toString() {
		return "fr.univbrest.dosi.spi.bean.RubriqueEvaluation[ idRubriqueEvaluation=" + idRubriqueEvaluation + " ]";
	}

}
