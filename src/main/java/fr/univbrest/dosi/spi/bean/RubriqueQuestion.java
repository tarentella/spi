/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DOSI
 */
@Entity
@Table(name = "RUBRIQUE_QUESTION")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "RubriqueQuestion.findAll", query = "SELECT r FROM RubriqueQuestion r"),
		@NamedQuery(name = "RubriqueQuestion.findByIdRubrique", query = "SELECT r FROM RubriqueQuestion r WHERE r.rubriqueQuestionPK.idRubrique = :idRubrique"),
		@NamedQuery(name = "RubriqueQuestion.findByIdQuestion", query = "SELECT r FROM RubriqueQuestion r WHERE r.rubriqueQuestionPK.idQuestion = :idQuestion"),
		@NamedQuery(name = "RubriqueQuestion.findByOrdre", query = "SELECT r FROM RubriqueQuestion r WHERE r.ordre = :ordre") })
public class RubriqueQuestion implements Serializable {
	private static final long serialVersionUID = 1L;
	@Basic(optional = false)
	@NotNull
	@Column(name = "ORDRE")
	private BigInteger ordre;
	@JoinColumn(name = "ID_QUESTION", referencedColumnName = "ID_QUESTION", insertable = false, updatable = false)
	@ManyToOne(optional = false)
	private Question question;
	@JoinColumn(name = "ID_RUBRIQUE", referencedColumnName = "ID_RUBRIQUE", insertable = false, updatable = false)
	@ManyToOne(optional = false)
	private Rubrique rubrique;
	@EmbeddedId
	protected RubriqueQuestionPK rubriqueQuestionPK;

	public RubriqueQuestion() {
	}

	public RubriqueQuestion(final long idRubrique, final long idQuestion) {
		this.rubriqueQuestionPK = new RubriqueQuestionPK(idRubrique, idQuestion);
	}

	public RubriqueQuestion(final RubriqueQuestionPK rubriqueQuestionPK) {
		this.rubriqueQuestionPK = rubriqueQuestionPK;
	}

	public RubriqueQuestion(final RubriqueQuestionPK rubriqueQuestionPK, final BigInteger ordre) {
		this.rubriqueQuestionPK = rubriqueQuestionPK;
		this.ordre = ordre;
	}

	@Override
	public boolean equals(final Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof RubriqueQuestion)) {
			return false;
		}
		final RubriqueQuestion other = (RubriqueQuestion) object;
		if (this.rubriqueQuestionPK == null && other.rubriqueQuestionPK != null || this.rubriqueQuestionPK != null && !this.rubriqueQuestionPK.equals(other.rubriqueQuestionPK)) {
			return false;
		}
		return true;
	}

	public BigInteger getOrdre() {
		return ordre;
	}

	public Question getQuestion() {
		return question;
	}

	public Rubrique getRubrique() {
		return rubrique;
	}

	public RubriqueQuestionPK getRubriqueQuestionPK() {
		return rubriqueQuestionPK;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += rubriqueQuestionPK != null ? rubriqueQuestionPK.hashCode() : 0;
		return hash;
	}

	public void setOrdre(final BigInteger ordre) {
		this.ordre = ordre;
	}

	public void setQuestion(final Question question) {
		this.question = question;
	}

	public void setRubrique(final Rubrique rubrique) {
		this.rubrique = rubrique;
	}

	public void setRubriqueQuestionPK(final RubriqueQuestionPK rubriqueQuestionPK) {
		this.rubriqueQuestionPK = rubriqueQuestionPK;
	}

	@Override
	public String toString() {
		return "fr.univbrest.dosi.spi.bean.RubriqueQuestion[ rubriqueQuestionPK=" + rubriqueQuestionPK + " ]";
	}

}
