package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author DOSI
 */
@Entity
@Table(name = "UNITE_ENSEIGNEMENT")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "UniteEnseignement.findAll", query = "SELECT u FROM UniteEnseignement u"),
	@NamedQuery(name = "UniteEnseignement.findByCodeFormation", query = "SELECT u FROM UniteEnseignement u WHERE u.uniteEnseignementPK.codeFormation = :codeFormation"),
	@NamedQuery(name = "UniteEnseignement.findByCodeUe", query = "SELECT u FROM UniteEnseignement u WHERE u.uniteEnseignementPK.codeUe = :codeUe"),
	@NamedQuery(name = "UniteEnseignement.findByDesignation", query = "SELECT u FROM UniteEnseignement u WHERE u.designation = :designation"),
	@NamedQuery(name = "UniteEnseignement.findBySemestre", query = "SELECT u FROM UniteEnseignement u WHERE u.semestre = :semestre"),
	@NamedQuery(name = "UniteEnseignement.findByDescription", query = "SELECT u FROM UniteEnseignement u WHERE u.description = :description"),
	@NamedQuery(name = "UniteEnseignement.findByNbhCm", query = "SELECT u FROM UniteEnseignement u WHERE u.nbhCm = :nbhCm"),
	@NamedQuery(name = "UniteEnseignement.findByNbhTd", query = "SELECT u FROM UniteEnseignement u WHERE u.nbhTd = :nbhTd"),
	@NamedQuery(name = "UniteEnseignement.findByNbhTp", query = "SELECT u FROM UniteEnseignement u WHERE u.nbhTp = :nbhTp") })
public class UniteEnseignement implements Serializable {
	private static final long serialVersionUID = 1L;
	@Size(max = 256)
	@Column(name = "DESCRIPTION")
	private String description;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 64)
	@Column(name = "DESIGNATION")
	private String designation;
	// @OneToMany(cascade = CascadeType.ALL, mappedBy = "uniteEnseignement", fetch = FetchType.LAZY)
	// @JsonBackReference(value="eve-ue")
	// private Collection<Evaluation> evaluationCollection;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "uniteEnseignement", fetch = FetchType.LAZY)
	//@JsonBackReference(value = "ec-ue")
	@JsonIgnore
	private Collection<ElementConstitutif> elementConstitutifCollection;
	@JoinColumn(name = "CODE_FORMATION", referencedColumnName = "CODE_FORMATION", insertable = false, updatable = false)
	@ManyToOne(optional = false)
	//@JsonManagedReference(value = "frm-ue")
	//@JsonIgnore
	private Formation formation;
	@Column(name = "NBH_CM")
	private BigInteger nbhCm;
	@Column(name = "NBH_TD")
	private Short nbhTd;
	@Column(name = "NBH_TP")
	private Short nbhTp;
	@JoinColumn(name = "NO_ENSEIGNANT", referencedColumnName = "NO_ENSEIGNANT")
	@ManyToOne(optional = false)
	//@JsonManagedReference(value = "ens-ue")
	//@JsonIgnore
	private Enseignant noEnseignant;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 3)
	@Column(name = "SEMESTRE")
	private String semestre;
	@EmbeddedId
	protected UniteEnseignementPK uniteEnseignementPK;

	public UniteEnseignement() {
	}

	public UniteEnseignement(final String codeFormation, final String codeUe) {
		this.uniteEnseignementPK = new UniteEnseignementPK(codeFormation, codeUe);
	}

	public UniteEnseignement(final UniteEnseignementPK uniteEnseignementPK) {
		this.uniteEnseignementPK = uniteEnseignementPK;
	}

	public UniteEnseignement(final UniteEnseignementPK uniteEnseignementPK, final String designation, final String semestre) {
		this.uniteEnseignementPK = uniteEnseignementPK;
		this.designation = designation;
		this.semestre = semestre;
	}

	@Override
	public boolean equals(final Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof UniteEnseignement)) {
			return false;
		}
		final UniteEnseignement other = (UniteEnseignement) object;
		if (this.uniteEnseignementPK == null && other.uniteEnseignementPK != null || this.uniteEnseignementPK != null && !this.uniteEnseignementPK.equals(other.uniteEnseignementPK)) {
			return false;
		}
		return true;
	}

	public String getDescription() {
		return description;
	}

	public String getDesignation() {
		return designation;
	}

	@XmlTransient
	public Collection<ElementConstitutif> getElementConstitutifCollection() {
		return elementConstitutifCollection;
	}

	//
	// @XmlTransient
	// public Collection<Evaluation> getEvaluationCollection() {
	// return evaluationCollection;
	// }

	public Formation getFormation() {
		return formation;
	}

	public BigInteger getNbhCm() {
		return nbhCm;
	}

	public Short getNbhTd() {
		return nbhTd;
	}

	public Short getNbhTp() {
		return nbhTp;
	}

	public Enseignant getNoEnseignant() {
		return noEnseignant;
	}

	public String getSemestre() {
		return semestre;
	}

	public UniteEnseignementPK getUniteEnseignementPK() {
		return uniteEnseignementPK;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += uniteEnseignementPK != null ? uniteEnseignementPK.hashCode() : 0;
		return hash;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public void setDesignation(final String designation) {
		this.designation = designation;
	}

	public void setElementConstitutifCollection(final Collection<ElementConstitutif> elementConstitutifCollection) {
		this.elementConstitutifCollection = elementConstitutifCollection;
	}

	public void setFormation(final Formation formation) {
		this.formation = formation;
	}

	public void setNbhCm(final BigInteger nbhCm) {
		this.nbhCm = nbhCm;
	}

	public void setNbhTd(final Short nbhTd) {
		this.nbhTd = nbhTd;
	}

	public void setNbhTp(final Short nbhTp) {
		this.nbhTp = nbhTp;
	}

	public void setNoEnseignant(final Enseignant noEnseignant) {
		this.noEnseignant = noEnseignant;
	}

	public void setSemestre(final String semestre) {
		this.semestre = semestre;
	}

	public void setUniteEnseignementPK(final UniteEnseignementPK uniteEnseignementPK) {
		this.uniteEnseignementPK = uniteEnseignementPK;
	}

	@Override
	public String toString() {
		return "fr.univbrest.dosi.spi.bean.UniteEnseignement[ uniteEnseignementPK=" + uniteEnseignementPK + " ]";
	}

}
