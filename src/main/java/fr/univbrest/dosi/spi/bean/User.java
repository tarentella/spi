package fr.univbrest.dosi.spi.bean;

import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class User {

	private Integer noEns;
	private String nom;
	private String pwd;
	private String roles;
	private String username;

	public User() {
	}

	public User(final String username, final String pwd, final String roles, final String nom, final Integer noEns) {
		this.username = username;
		this.pwd = pwd;
		this.roles = roles;
		this.nom = nom;
		this.noEns = noEns;
	}

	public Integer getNoEns() {
		return noEns;
	}

	public String getNom() {
		return nom;
	}

	/**
	 * @return the pwd
	 */
	public String getPwd() {
		return this.pwd;
	}

	public String getRoles() {
		return roles;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return this.username;
	}

	public void setNoEns(final Integer noEns) {
		this.noEns = noEns;
	}

	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * @param pwd
	 *            the pwd to set
	 */
	public void setPwd(final String pwd) {
		this.pwd = pwd;
	}

	public void setRoles(final String roles) {
		this.roles = roles;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(final String username) {
		this.username = username;
	}

}
