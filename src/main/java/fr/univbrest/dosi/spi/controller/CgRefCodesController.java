package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.CgRefCodes;
import fr.univbrest.dosi.spi.service.CgrefCodesService;


@RestController
public class CgRefCodesController {
	
	@Autowired
	private CgrefCodesService cgrefCodesService;
	
	
	@RequestMapping(value = "/getCgrefRvMeaning/{rvAbbreviation}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public final CgRefCodes getCgrefRvMeaning(@PathVariable(value = "rvAbbreviation") final String rvAbbreviation) {
		return cgrefCodesService.getRvMeaningByAbrv(rvAbbreviation);
	}
	
	@RequestMapping(value = "/getRvDomain/{rvDomain}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public final List<CgRefCodes> getRvDomain(@PathVariable(value = "rvDomain") final String rvDomain) {
		return cgrefCodesService.getDomain(rvDomain);
	}
	
}
