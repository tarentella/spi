package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.service.DashboardService;

@RestController
public class DashboardController {

	@Autowired
	DashboardService dashboardService;
	
	@RequestMapping("/nbrEtu")
	public long nbrEtu(){
		return dashboardService.nbrEtu();
	}
	
	@RequestMapping("/nbrFrm")
	public long nbrFrm(){
		return dashboardService.nbrFrm();
	}
	
	@RequestMapping("/nbrUe")
	public long nbrUe(){
		return dashboardService.nbrUe();
	}
	
	@RequestMapping("/nbrEns")
	public long nbrEns(){
		return dashboardService.nbrEns();
	}
}
