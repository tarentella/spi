package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.ElementConstitutif;
import fr.univbrest.dosi.spi.service.ElementConstitutifService;

@RestController
public class ElementConstitutifController {

	@Autowired
	ElementConstitutifService elementConstitutifService;
	
	@RequestMapping(value = "/ajouterEC", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final String addEC(@RequestBody final ElementConstitutif ec) {
		elementConstitutifService.addElementConstitutif(ec);
		return "l'ec " + ec.getElementConstitutifPK().getCodeEc()+  " est ajouter";
	}

	/**
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 */
//	@RequestMapping(value = "/deleteEC/{noenseignant}")
//	public final void deleteEnseignant(@PathVariable(value = "noenseignant") final Integer noEnseignant) {
//		// this.checkDroits(TypeDroit.DELETE);
//		enseignantService.deleteEnseignant(noEnseignant);
//	}
//	
//	@RequestMapping(value = "/updateEnseignant", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
//	public final String updateEnseignant(@RequestBody final Enseignant enseignant) {
//		// this.checkDroits(TypeDroit.MODIFY);
//		enseignantService.updateEnseignant(enseignant);
//		return "l'enseignant " + enseignant.getNom() + " " + enseignant.getPrenom() + " est modifier";
//	}
}
