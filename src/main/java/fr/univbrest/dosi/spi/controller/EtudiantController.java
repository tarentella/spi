package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.service.EtudiantService;

/**
 * @author DOSI
 *
 */

@RestController
public class EtudiantController {
	/**
	 *
	 */
	@Autowired
	private EtudiantService etudiantService;

	@RequestMapping(value = "/ajouterEtudiant", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final void addEtudiant(@RequestBody final Etudiant etudiant) {
		etudiantService.addEtudiant(etudiant);
	}

	/**
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 */
	@RequestMapping(value = "/deleteEtudiant/{noEtudiant}")
	public final void deleteEtudiant(@PathVariable(value = "noEtudiant") final String noEtudiant) {
		etudiantService.deleteEtudiant(noEtudiant);
	}

	/**
	 *
	 * @return liste des etudiants
	 */
	@RequestMapping("/etudiants")
	public final Iterable<Etudiant> etudiant() {
		return etudiantService.listEtud();
	}

	@RequestMapping(value = "/existEtud/{noEtudiant}")
	public final Boolean existEnseignant(@PathVariable(value = "noEtudiant") final String noEtudiant) {
		return etudiantService.existEtudiant(noEtudiant);
	}

	@RequestMapping(value = "/getEtud/{noEtudiant}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public final Etudiant getEtudiant(@PathVariable(value = "noEtudiant") final String noEtudiant) {
		return etudiantService.getEtudiant(noEtudiant);
	}

	@RequestMapping(value = "/getEtudNom/{nom}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public final List<Etudiant> getEtudiantByNom(@PathVariable(value = "nom") final String nom) {
		return etudiantService.getEtudiantByNom(nom);
	}

	public void setEtudiantService(final EtudiantService etudiantService) {
		this.etudiantService = etudiantService;
	}

	@RequestMapping(value = "/updateEtudiant", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final void updateEtudiant(@RequestBody final Etudiant etudiant) {
		etudiantService.updateEtudiant(etudiant);
	}
}
