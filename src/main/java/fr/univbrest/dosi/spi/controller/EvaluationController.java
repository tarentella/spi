package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Evaluation;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.bean.Question;
import fr.univbrest.dosi.spi.service.EvaluationService;
import fr.univbrest.dosi.spi.service.PromotionService;
import fr.univbrest.dosi.spi.service.QualificatifService;
import fr.univbrest.dosi.spi.service.QuestionService;

/**
 * @author DOSI
 *
 */

@RestController
public class EvaluationController {

	@Autowired
	private EvaluationService evaluationService;

	@Autowired
	private PromotionService promotionService;

	@Autowired
	private QualificatifService qualificatifService;

	@Autowired
	private QuestionService questionService;

	@RequestMapping(value = "/addEval", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final void addEval(@RequestBody final Evaluation evaluation) {
		evaluationService.addEvaluation(evaluation);
	}

	@RequestMapping(value = "/addQualif", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	public final String addQaulif(@RequestBody final Qualificatif qualif) {
		qualificatifService.addQualificatif(qualif);
		return "le qualificatif " + qualif.getIdQualificatif() + "  avec max : " + qualif.getMaximal() + " et min " + qualif.getMinimal() + "est crée";
	}

	@RequestMapping(value = "/ajouterQuestion", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public final void ajouterQuestion(@RequestBody final Question question) {
		questionService.addQuestion(question);
	}

	@RequestMapping(value = "/deleteEval/{codeEval}", produces = { "application/json;charset=UTF-8" })
	public final void deleteEval(@PathVariable(value = "codeEval") final String codeEval) {
		evaluationService.deleteEvaluation(Long.parseLong(codeEval));
	}

	@RequestMapping(value = "/deleteQualif/{codeQualif}", produces = { "application/json;charset=UTF-8" })
	public final void deleteQualif(@PathVariable(value = "codeQualif") final String codeQualif) {
		qualificatifService.deleteQualificatif(Long.parseLong(codeQualif));
	}

	@RequestMapping(value = "/getAllEval")
	public final Iterable<Evaluation> getAllEval() {
		return evaluationService.getAll();
	}

	@RequestMapping(value = "/getAllQualif")
	public final List<Qualificatif> getAllQualif() {
		return qualificatifService.getAll();
	}

	@RequestMapping(value = "/getEval/{codeEval}", produces = { "application/json;charset=UTF-8" })
	public final Evaluation getEval(@PathVariable(value = "codeEval") final String codeEval) {
		return evaluationService.getEvaluation(Long.parseLong(codeEval));
	}

	// @RequestMapping(value = "/question/ajouterquestion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	// public final Question ajouterQuestion(@RequestBody final QuestionBis questionBis) {
	// return questionService.addQuestion(questionBis);
	// }

	@RequestMapping(value = "/getPromoByFrm/{codeF}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public final List<Promotion> getPromoByFrm(@PathVariable("codeF") final String codeF) {
		return promotionService.getPromotionByFrm(codeF);
	}

	@RequestMapping(value = "/getQualif/{codeQualif}", produces = { "application/json;charset=UTF-8" })
	public final Qualificatif getQualif(@PathVariable(value = "codeQualif") final String codeQualif) {
		return qualificatifService.getQualificatif(Long.parseLong(codeQualif));
	}

	/**
	 *
	 * @return list de question
	 */

	@RequestMapping(value = "/deleteQuestion/{id}", headers = "Accept=application/json")
	public final void removeQuestion(@PathVariable("id") final Long id) {
		questionService.deleteQuestion(id);
	}

	/**
	 *
	 * @return list de question
	 */
	// @RequestMapping(produces = "application/json", value = "/questions")
	// public final Iterable<Question> questions() {
	//
	// return questionService.listQuestion();
	//
	// }

	// @RequestMapping(value = "/question/delete/{id}", headers = "Accept=application/json")
	// public final void removeQuestion(@PathVariable("id") final Long id) {
	// questionService.deleteQuestion(id);
	// }

	@RequestMapping(value = "/updateEval", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final void updateEval(@RequestBody final Evaluation evaluation) {
		evaluationService.updateEvaluation(evaluation);
	}

	// @RequestMapping(value = "/question/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	// public final Question question(@PathVariable(value = "id") final Long id) {
	// return questionService.getQuestion(id);
	//
	// }

	@RequestMapping(value = "/updateQualificatif", method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	public final void updateQualificatif(@RequestBody final Qualificatif qualif) {
		qualificatifService.updateQualificatif(qualif);
	}

	@RequestMapping(value = "/updateQuestion/", method = RequestMethod.POST, consumes = { "application/json" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public final void updateQuestion(@RequestBody final Question question) {
		questionService.updateQuestion(question);
	}

	// @RequestMapping(value = "/updateQuestion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	// public final Question updateQuestion(@RequestBody final QuestionBis questionBis) {
	// return questionService.updateQuestion(questionBis);
	//
	// }

}
