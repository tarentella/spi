package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.PromotionPK;
import fr.univbrest.dosi.spi.service.EtudiantService;
import fr.univbrest.dosi.spi.service.PromotionService;

@RestController
public class PromotionController {

	@Autowired
	EtudiantService etudiantService;

	@Autowired
	PromotionService promotionService;

	@RequestMapping(value = "/promotion/ajouterPromotion/", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" })
	public final void addPromotion(@RequestBody final Promotion promotion) {
		promotionService.addPromotion(promotion);
	}

	@RequestMapping(value = "/promotion/deletePromotion/{codeF}/{anneeU}")
	public final void deletePromotion(@PathVariable(value = "codeF") final String codeF, @PathVariable(value = "anneeU") final String anneeU) {
		promotionService.deletePromotion(new PromotionPK(codeF, anneeU));
	}

	@RequestMapping(value = "/promotion/getAll", produces = { "application/json;charset=UTF-8" })
	public final Iterable<Promotion> getAllPromotion() {
		return promotionService.getAllPromo();
	}

	@RequestMapping(value = "/promotion/getEtuPro/{codeF}/{anneeU}", produces = { "application/json;charset=UTF-8" })
	public final Iterable<Etudiant> getEtuPromotion(@PathVariable(value = "codeF") final String codeF, @PathVariable(value = "anneeU") final String anneeU) {
		return etudiantService.getEtudiantByPromo(promotionService.getPromotion(new PromotionPK(codeF, anneeU)));
	}

	@RequestMapping(value = "/promotion/getNbreEtu/{codeF}/{anneeU}")
	public final Integer getNbreEtu(@PathVariable(value = "codeF") final String codeF, @PathVariable(value = "anneeU") final String anneeU) {
		final List<Etudiant> listeEtu = (List) etudiantService.getEtudiantByPromo(promotionService.getPromotion(new PromotionPK(codeF, anneeU)));
		return listeEtu.size();
	}

	@RequestMapping(value = "/promotion/getPromo/{codeF}_{anneeU}", produces = { "application/json;charset=UTF-8" })
	public final Promotion getPromotionById(@PathVariable(value = "codeF") final String codeF, @PathVariable(value = "anneeU") final String anneeU) {
		return promotionService.getPromotion(new PromotionPK(codeF, anneeU));
	}

}
