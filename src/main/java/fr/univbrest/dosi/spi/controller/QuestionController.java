package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Question;
import fr.univbrest.dosi.spi.service.QuestionService;

@RestController
public class QuestionController {

	@Autowired
	QuestionService questionService;

	@RequestMapping(value = "/question/ajouterquestion", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public final void ajouterQuestion(@RequestBody final Question question) {
		questionService.addQuestion(question);
	}

	@RequestMapping(value = "/question/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public final Question question(@PathVariable(value = "id") final Long id) {
		return questionService.getQuestion(id);

	}

	@RequestMapping(value = "/questions", produces = "application/json")
	public final Iterable<Question> questions() {
		return questionService.getAllQuestion();

	}

	@RequestMapping(value = "/question/delete/{id}", headers = "Accept=application/json")
	public final void removeQuestion(@PathVariable("id") final Long id) {
		questionService.deleteQuestion(id);
	}

	@RequestMapping(value = "/question/updatequestion", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public final void updateQuestion(@RequestBody final Question question) {
		questionService.updateQuestion(question);
	}

}
