package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.RubriqueEvaluation;
import fr.univbrest.dosi.spi.service.EvaluationService;
import fr.univbrest.dosi.spi.service.RubriqueEvaluationService;

@RestController
public class RubriqueEvaluationController {

	@Autowired

	EvaluationService evaluationService;

	@Autowired

	RubriqueEvaluationService rubriqueEvaluationService;

	@RequestMapping(value = "/rubriqueEvaluation/addRubriqueEvaluation/", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final void ajouterRubriqueEvaluation(@RequestBody final RubriqueEvaluation rubriqueEvaluation) {
		rubriqueEvaluationService.addRubriqueEvaluation(rubriqueEvaluation);
	}

	@RequestMapping(value = "/rubriqueEvaluation/delete/{id}", produces = "application/json")
	public final void delete(@PathVariable("id") final String idRubriqueEvaluation) {
		rubriqueEvaluationService.deleteRubriqueEvaluation(Long.parseLong(idRubriqueEvaluation));

	}


	@RequestMapping(value = "/rubriqueEvaluation/deleteByRubEval/{idEval}/{idRub}", produces = "application/json")
	public final void deleteRubEval(@PathVariable("idEval") final String idEval, @PathVariable("idRub") final String idRub) {
		rubriqueEvaluationService.deleteRubEvalByRubEval(Long.parseLong(idEval), Long.parseLong(idRub));

	}

	@RequestMapping(value = "/rubriqueEvaluation/getRubEval/{id}", headers = "Accept=application/json", produces = "application/json")
	public final RubriqueEvaluation getRubriqueEvaluation(@PathVariable("id") final String idRubriqueEvaluation) {
		return rubriqueEvaluationService.getRubriqueEvaluation(Long.parseLong(idRubriqueEvaluation));
	}

	@RequestMapping(value = "/rubriqueEvaluation/getRubEvalByEval/{id}", headers = "Accept=application/json", produces = "application/json")
	public final Iterable<RubriqueEvaluation> getRubriqueEvaluationByEval(@PathVariable("id") final String idEvaluation) {
		return rubriqueEvaluationService.getRubEvalByEval(evaluationService.getEvaluation(Long.parseLong(idEvaluation)));
	}

	@RequestMapping(value = "/rubriqueEvaluation/getAll", produces = "application/json")
	public final Iterable<RubriqueEvaluation> questions() {
		return rubriqueEvaluationService.getAll();

	}

	@RequestMapping(value = "/rubriqueEvaluation/updateRubriqueEvaluation/", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final void updateRubriqueEvaluation(@RequestBody final RubriqueEvaluation rubriqueEvaluation) {
		rubriqueEvaluationService.updateRubriqueEvaluation(rubriqueEvaluation);
	}

}
