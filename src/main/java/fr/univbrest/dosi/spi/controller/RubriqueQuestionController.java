package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.RubriqueQuestion;
import fr.univbrest.dosi.spi.bean.RubriqueQuestionPK;
import fr.univbrest.dosi.spi.service.RubriqueQuestionService;

/**
 * @author DOSI
 *
 */
@RestController
public class RubriqueQuestionController {

	@Autowired
	RubriqueQuestionService rubriqueQuestionService;

	@RequestMapping(value = "/RubriqueQuestion/ajouterRubriqueQuestion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" })
	public final void addRubriqueQuestion(@RequestBody final RubriqueQuestion RubriqueQuestion) {
		rubriqueQuestionService.addRubriqueQuestion(RubriqueQuestion);
	}

	@RequestMapping(value = "/RubriqueQuestion/deleteRubriqueQuestion/{idRubrique}/{idQuestion}")
	public final void deleteRubriqueQuestion(@PathVariable(value = "idRubrique") final Long idRubrique, @PathVariable(value = "idQuestion") final Long idQuestion) {
		rubriqueQuestionService.deleteRubriqueQuestion(new RubriqueQuestionPK(idRubrique, idQuestion));
	}

	@RequestMapping(value = "/RubriqueQuestion/getAllRubriqueQuestion", produces = { "application/json;charset=UTF-8" })
	public final Iterable<RubriqueQuestion> getAllRubriqueQuestion() {
		return rubriqueQuestionService.getAll();
	}

	@RequestMapping(value = "/RubriqueQuestion/getRubriqueQuestion/{idRubrique}/{idQuestion}", produces = { "application/json;charset=UTF-8" })
	public final RubriqueQuestion getRubriqueQuestion(@PathVariable(value = "idRubrique") final Long idRubrique, @PathVariable(value = "idQuestion") final Long idQuestion) {
		return rubriqueQuestionService.getRubriqueQuestion(new RubriqueQuestionPK(idRubrique, idQuestion));
	}

	@RequestMapping(value = "/RubriqueQuestion/updateRubriqueQuestion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" })
	public final void updateRubriqueQuestion(@RequestBody final RubriqueQuestion RubriqueQuestion) {
		rubriqueQuestionService.updateRubriqueQuestion(RubriqueQuestion);
	}

}
