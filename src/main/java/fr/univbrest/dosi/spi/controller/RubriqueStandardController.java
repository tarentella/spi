package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Rubrique;
import fr.univbrest.dosi.spi.service.EvaluationService;
import fr.univbrest.dosi.spi.service.RubriqueEvaluationService;
import fr.univbrest.dosi.spi.service.RubriqueStandardService;

@RestController
public class RubriqueStandardController {

	@Autowired
	RubriqueStandardService rubriqueStandardService;
	@Autowired
	RubriqueEvaluationService rubriqueEvaluationServie;
	
	@Autowired
	EvaluationService evaluationService;

	@RequestMapping(value = "/ajouterRubrique", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final String addRubrique(@RequestBody final Rubrique rubrique) {
		rubriqueStandardService.addRubrique(rubrique);
		return "la rubrique " + rubrique.getOrdre() + " " + rubrique.getDesignation() + " est ajoutee";
	}

	@RequestMapping(value = "/deleteRubrique", produces = { "application/json;charset=UTF-8" })
	public final void deleteRubrique(@Param(value = "idRubrique") final Long idRubrique) {
		rubriqueStandardService.deleteRubrique(idRubrique);
	}

	@RequestMapping(value = "/getrub", produces = { "application/json;charset=UTF-8" })
	public final Rubrique getRubrique(@Param(value = "idRubrique") final Long idRubrique) {
		return rubriqueStandardService.getRubrique(idRubrique);

	}

	@RequestMapping(value = "/rubrique/getRubrique/{id}", headers = "Accept=application/json", produces = "application/json")
	public final Rubrique getRubriqueParId(@PathVariable("id") final String idRubrique) {
		return rubriqueStandardService.getRubrique(Long.parseLong(idRubrique));
	}
	
	@RequestMapping(value = "/rubrique/getRubriqueByEva/{id}", headers = "Accept=application/json", produces = "application/json")
	public final List<Rubrique> getRubriqueByEva(@PathVariable("id") final String idEva) {
		return rubriqueEvaluationServie.getRubByEva(evaluationService.getEvaluation(Long.parseLong(idEva)));
	}

	@RequestMapping("/rub")
	public final Iterable<Rubrique> rubrique() {
		return rubriqueStandardService.listrub();
	}

	@RequestMapping(value = "/updateRubrique", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final String updateRubrique(@RequestBody final Rubrique rubrique) {
		rubriqueStandardService.updateRubrique(rubrique);
		return "la rubrique " + rubrique.getOrdre() + " " + rubrique.getDesignation() + " est modifiee";
	}

}
