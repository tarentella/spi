package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.ElementConstitutif;
import fr.univbrest.dosi.spi.bean.ElementConstitutifPK;
import fr.univbrest.dosi.spi.bean.UniteEnseignement;
import fr.univbrest.dosi.spi.bean.UniteEnseignementPK;
import fr.univbrest.dosi.spi.service.ElementConstitutifService;
import fr.univbrest.dosi.spi.service.FormationService;
import fr.univbrest.dosi.spi.service.UniteEnseignementService;

@RestController
public class UniteEnseignementController {

	@Autowired
	ElementConstitutifService elementConstitutifService;
//
	@Autowired
	FormationService formationService;

	@Autowired
	UniteEnseignementService uniteEnseignementService;
	
	
	@RequestMapping(value = "/ajouterUE", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final String addEC(@RequestBody final UniteEnseignement ue) {
		// this.checkDroits(TypeDroit.CREATE);
		uniteEnseignementService.addUnitEnseignement(ue);
		return "l'ue " + ue.getUniteEnseignementPK().getCodeUe()+  " est ajouter";
	}

	@RequestMapping(value = "/addEC/")
	public void addEc(@RequestBody final ElementConstitutif ec) {
		elementConstitutifService.addElementConstitutif(ec);
	}

	@RequestMapping(value = "/addUE/")
	public final void addUe(@RequestBody final UniteEnseignement UE) {
		uniteEnseignementService.addUnitEnseignement(UE);
	}

	@RequestMapping(produces = "application/json", value = "/getAllUe")
	public final Iterable<UniteEnseignement> getAll() {

		final Iterable<UniteEnseignement> ue = uniteEnseignementService.getAll();
		for (final UniteEnseignement x : ue) {
			System.out.println("OK traitement " + x.getDesignation());
		}
		return ue;

	}

	@RequestMapping(value = "/getECUE/{codeUE}")
	public final Iterable<ElementConstitutif> getEC(@PathVariable(value = "codeUE") final String codeUe) {
		return elementConstitutifService.getECByUE(codeUe);
	}
	
	@RequestMapping(value = "/updateEC/", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public void updateEc(@RequestBody final ElementConstitutif ec) {
		elementConstitutifService.updateElementConstitutif(ec);
	}
	
	@RequestMapping(value = "/deleteEC/{codeFormation}/{codeUe}/{codeEc}")
	public void elementConstitutif(@PathVariable(value = "codeFormation") final String codeFormation, @PathVariable(value = "codeUe") final String codeUe , @PathVariable(value = "codeEc") final String codeEc) {
		elementConstitutifService.deleteElementConstitutif(new ElementConstitutifPK(codeFormation,codeUe,codeEc));
	}

	@RequestMapping(value = "/getue/{noEnseignant}")
	public final Iterable<UniteEnseignement> getUE(@PathVariable(value = "noEnseignant") final Integer noEnseignant) {
		return uniteEnseignementService.getUEByEnseignant(noEnseignant);
	}

	@RequestMapping(value = "/getUEF/{codeF}")
	public final Iterable<UniteEnseignement> getUE(@PathVariable(value = "codeF") final String codeF) {
		return uniteEnseignementService.getUEByCodeFormation(codeF);
	}

	@RequestMapping(value = "/ue", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public final UniteEnseignement ue(@RequestBody final UniteEnseignementPK code) {
		return uniteEnseignementService.uniteEnseignement(code);
	}
	
	@RequestMapping(value = "/updateUE/")
	public void updateUe(@RequestBody final UniteEnseignement ue) {
		uniteEnseignementService.updateUniteEnseignement(ue);
	}
	
	@RequestMapping(value = "/ue/delete/{codeFormation}/{codeUe}")
	public void deletUnitEnseignement(@PathVariable(value = "codeFormation") final String codeF, @PathVariable(value = "codeUe") final String codeUe) {
		uniteEnseignementService.deletUnitEnseignement(new UniteEnseignementPK(codeF,codeUe));
	}

}
