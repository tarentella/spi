package fr.univbrest.dosi.spi.converter;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.GenericConversionService;

@Configuration
public class ConverterConfiguration {

	@Resource(name = "defaultConversionService")
	private GenericConversionService genericConversionService;

	@Bean
	public PromotionPKConverter string2PromotionPKConverter() {
		final PromotionPKConverter promotionPkConverter = new PromotionPKConverter();
		genericConversionService.addConverter(promotionPkConverter);
		return promotionPkConverter;
	}

}