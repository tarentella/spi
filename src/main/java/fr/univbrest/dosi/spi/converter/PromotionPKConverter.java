package fr.univbrest.dosi.spi.converter;

import fr.univbrest.dosi.spi.bean.PromotionPK;

public class PromotionPKConverter implements org.springframework.core.convert.converter.Converter<String, PromotionPK> {

	@Override
	public PromotionPK convert(final String source) {
		final PromotionPK promotionPK = new PromotionPK();
		final String[] cle = source.split("_");
		promotionPK.setCodeFormation(cle[0]);
		promotionPK.setAnneeUniversitaire(cle[1]);
		return promotionPK;
	}

}
