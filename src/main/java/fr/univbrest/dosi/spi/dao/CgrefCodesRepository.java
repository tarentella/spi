package fr.univbrest.dosi.spi.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.CgRefCodes;



@RepositoryRestResource(collectionResourceRel = "cgrefCodes", path = "cgrefCodes")
public interface CgrefCodesRepository extends PagingAndSortingRepository<CgRefCodes, BigDecimal> {

	CgRefCodes findByRvAbbreviation(@Param("rvAbbreviation") String rvAbbreviation);
	
	List<CgRefCodes> findByRvDomain(@Param("rvDomain") String rvDomain);
	
	}


