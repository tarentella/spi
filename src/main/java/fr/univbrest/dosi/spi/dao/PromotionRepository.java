package fr.univbrest.dosi.spi.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.PromotionPK;

/**
 * @author DOSI
 *
 */
@RepositoryRestResource(collectionResourceRel = "promotion", path = "promotion")
public interface PromotionRepository extends PagingAndSortingRepository<Promotion, PromotionPK> {

	List<Promotion> findByCodeFormation(@Param("codeFormation") String codeFormation);

	Promotion findByCodeFormationAndAnneeUniversitaire(@Param("codeFormation") String codeFormation, @Param("anneeUniversitaire") String anneeUniversitaire);

	/**
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 * @return liste des promotions
	 */
	List<Promotion> findByNoEnseignant(@Param("noEnseignant") Enseignant noEnseignant);

}
