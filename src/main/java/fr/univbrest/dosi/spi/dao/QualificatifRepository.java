package fr.univbrest.dosi.spi.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.Qualificatif;

/**
 * @author DOSI
 *
 */
@RepositoryRestResource(collectionResourceRel = "qualificatif", path = "qualificatif")
public interface QualificatifRepository extends PagingAndSortingRepository<Qualificatif, Long> {
	Qualificatif findByIdQualificatif(@Param("idQualificatif") Long idQualificatif);
	
	 List<Qualificatif> findAll();
}
