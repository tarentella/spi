package fr.univbrest.dosi.spi.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


import fr.univbrest.dosi.spi.bean.Evaluation;
import fr.univbrest.dosi.spi.bean.RubriqueEvaluation;

@RepositoryRestResource(collectionResourceRel = "rubriqueEvaluation", path = "rubriqueEvaluation")
public interface RubriqueEvaluationRepository extends PagingAndSortingRepository<RubriqueEvaluation, Long> {


	Iterable<RubriqueEvaluation> findByIdEvaluation(@Param("idEvaluation") Evaluation idEvaluation);


	RubriqueEvaluation findByIdRubriqueEvaluation(@Param("idRubriqueEvaluation") Long idRubriqueEvaluation);

}
