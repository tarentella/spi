package fr.univbrest.dosi.spi.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.RubriqueQuestion;
import fr.univbrest.dosi.spi.bean.RubriqueQuestionPK;


/**
 * @author DOSI
 *
 */
@RepositoryRestResource(collectionResourceRel = "rubriqueQuestion", path = "rubriqueQuestion")
public interface RubriqueQuestionRepository extends PagingAndSortingRepository<RubriqueQuestion, RubriqueQuestionPK>{
	
}
