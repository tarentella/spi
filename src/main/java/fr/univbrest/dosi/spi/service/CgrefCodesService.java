package fr.univbrest.dosi.spi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.CgRefCodes;
import fr.univbrest.dosi.spi.dao.CgrefCodesRepository;


@Service
public class CgrefCodesService {
	
	
	@Autowired
	private CgrefCodesRepository cgref;
	
	public final CgRefCodes getRvMeaningByAbrv(final String rvAbbreviation) {
		
		return cgref.findByRvAbbreviation(rvAbbreviation);
		
	}
	

	public final List<CgRefCodes> getDomain(final String rvDomain) {
	
			return cgref.findByRvDomain(rvDomain);
	
	}
	
	
}
