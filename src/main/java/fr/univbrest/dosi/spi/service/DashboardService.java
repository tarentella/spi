package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.dao.EnseignantRepository;
import fr.univbrest.dosi.spi.dao.EtudiantRepository;
import fr.univbrest.dosi.spi.dao.FormationRepository;
import fr.univbrest.dosi.spi.dao.UniteEnseignementRepository;

@Service
public class DashboardService {
	
	@Autowired
	FormationRepository formationRepository;
	@Autowired
	EtudiantRepository etudiantRepository;
	@Autowired
	UniteEnseignementRepository uniteEnseignementRepository;
	@Autowired
	EnseignantRepository enseignantRepository;
	
	
	public long nbrEtu(){
		return etudiantRepository.count();
	}
	
	public long nbrFrm(){
		return formationRepository.count();
	}
	
	public long nbrUe(){
		return uniteEnseignementRepository.count();
	}
	
	public long nbrEns(){
		return enseignantRepository.count();
	}

}
