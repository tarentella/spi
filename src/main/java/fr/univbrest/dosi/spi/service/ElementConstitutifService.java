package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.ElementConstitutif;
import fr.univbrest.dosi.spi.bean.ElementConstitutifPK;
import fr.univbrest.dosi.spi.dao.ElementConstitutifRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 *
 */
@Service
public class ElementConstitutifService {

	@Autowired
	private ElementConstitutifRepository elementConstitutifRepository;

	public final void addElementConstitutif(final ElementConstitutif elementConstitutif) {
		elementConstitutifRepository.save(elementConstitutif);
	}

	public final void deleteElementConstitutif(final ElementConstitutifPK elementConstitutifPK) {
		ElementConstitutif Ec=elementConstitutifRepository.findOne(elementConstitutifPK);
		if(Ec != null){
		elementConstitutifRepository.delete(Ec);
		}
	}
	
	
	public final void updateElementConstitutif(final ElementConstitutif elementConstitutif) {
		try {
			if (!elementConstitutifRepository.exists(elementConstitutif.getElementConstitutifPK())) {
				throw new SPIException("Echec de la modification. Element constitutif introuvable");
			}
			elementConstitutifRepository.save(elementConstitutif);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}
	
	public final Boolean existElementCostitutif(final ElementConstitutifPK elementConstitutifPK) {
		return elementConstitutifRepository.exists(elementConstitutifPK);
	}

	public Iterable<ElementConstitutif> getAll() {
		return elementConstitutifRepository.findAll();
	}

	public Iterable<ElementConstitutif> getECByUE(final String codeUe) {
		return elementConstitutifRepository.findByCodeUe(codeUe);
	}

	public final ElementConstitutif getElementConstitutif(final ElementConstitutifPK elementConstitutifPK) {
		return elementConstitutifRepository.findOne(elementConstitutifPK);
	}
}
