package fr.univbrest.dosi.spi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.dao.EtudiantRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 *
 */
@Service
public class EtudiantService {

	@Autowired
	private EtudiantRepository etudiantRepository;

	public final Etudiant addEtudiant(final Etudiant etudiant) {
		if (etudiantRepository.exists(etudiant.getNoEtudiant())) {
			throw new SPIException("l'etudiant que vous souhaitez ajouter exsite déja ");
		}
		return etudiantRepository.save(etudiant);
	}

	public final void deleteEtudiant(final String noEtudiant) {
		if (etudiantRepository.exists(noEtudiant)) {
			etudiantRepository.delete(noEtudiant);
		} else {
			throw new SPIException("Cant delete Etudiant");
		}

	}

	public final Boolean existEtudiant(final String noEtudiant) {
		final Boolean exist = etudiantRepository.exists(noEtudiant);
		if (exist) {
			return exist;
		} else {
			throw new SPIException("Il y a aucun etudiant avec ce numero");
		}
	}

	public final Etudiant getEtudiant(final String noEtudiant) {
		return etudiantRepository.findOne(noEtudiant);
	}

	public final List<Etudiant> getEtudiantByNom(final String nom) {
		return etudiantRepository.findByNom(nom);
	}

	public final Iterable<Etudiant> getEtudiantByPromo(final Promotion promotion) {
		return etudiantRepository.findByPromotion(promotion);
	}

	public final Iterable<Etudiant> listEtud() {
		final Iterable<Etudiant> etudiants = etudiantRepository.findAll();
		return etudiants;
	}

	public final void setEtudiantRepository(final EtudiantRepository etudiantRepository) {
		this.etudiantRepository = etudiantRepository;
	}

	public final Etudiant updateEtudiant(final Etudiant etudiant) {
		if (etudiantRepository.exists(etudiant.getNoEtudiant())) {
			return etudiantRepository.save(etudiant);
		} else {
			throw new SPIException("l'etudiant que vous souhaitez modifier n'exsite pas ");
		}
	}

}
