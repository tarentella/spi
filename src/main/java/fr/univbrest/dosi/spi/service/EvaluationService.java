package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Evaluation;
import fr.univbrest.dosi.spi.dao.EvaluationRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 *
 */
@Service
public class EvaluationService {

	@Autowired
	private EvaluationRepository evaluationRepository;

	/**
	 *
	 * @return list de question
	 */

	public void addEvaluation(final Evaluation evaluation) {
		evaluationRepository.save(evaluation);
	}

	public final void deleteEvaluation(final Long codeEval) {
		try {
			if (!evaluationRepository.exists(codeEval)) {
				throw new SPIException("Echec de la suppression. Evaluation introuvable");
			}
			evaluationRepository.delete(codeEval);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public Boolean existEvaluation(final Long codeEval) {
		return evaluationRepository.exists(codeEval);
	}

	public Iterable<Evaluation> getAll() {
		try {
			return evaluationRepository.findAll();
		} catch (final Exception e) {
			throw new SPIException("Erreur base de données", e);
		}
	}

	public final Evaluation getEvaluation(final Long codeEval) {
		try {
			if (!evaluationRepository.exists(codeEval)) {
				throw new SPIException("Evaluation introuvable");
			}
			return evaluationRepository.findByIdEvaluation(codeEval);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public final void updateEvaluation(final Evaluation evaluation) {
		try {
			if (!evaluationRepository.exists(evaluation.getIdEvaluation())) {
				throw new SPIException("Echec de la modification. Evaluation introuvable");
			}
			evaluationRepository.save(evaluation);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}
}
