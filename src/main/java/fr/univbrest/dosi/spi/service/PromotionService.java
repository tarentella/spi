package fr.univbrest.dosi.spi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.PromotionPK;
import fr.univbrest.dosi.spi.dao.EnseignantRepository;
import fr.univbrest.dosi.spi.dao.PromotionRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

@Service
public class PromotionService {

	@Autowired
	private EnseignantRepository enseignantRepository;

	@Autowired
	private PromotionRepository promotionRepository;

	public final void addPromotion(final Promotion promotion) {
		try {
			if (promotionRepository.exists(promotion.getPromotionPK())) {
				throw new SPIException("Echec de l'ajout. Promotion existante");
			}
			promotionRepository.save(promotion);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public final void deletePromotion(final PromotionPK promotionPK) {
		promotionRepository.delete(promotionPK);
	}

	public final Boolean existPromotion(final PromotionPK promotionPK) {
		return promotionRepository.exists(promotionPK);
	}

	public final Iterable<Promotion> getAllPromo() {
		return promotionRepository.findAll();
	}

	public final Promotion getPromotion(final PromotionPK promotionPK) {
		return promotionRepository.findOne(promotionPK);
	}

	public final List<Promotion> getPromotionByEnseignant(final Integer noEnseignant) {

		return promotionRepository.findByNoEnseignant(enseignantRepository.findOne(noEnseignant));
	}

	public final List<Promotion> getPromotionByFrm(final String codeFormation) {
		return promotionRepository.findByCodeFormation(codeFormation);
	}

	public final Promotion getPromotionByFrmAndAnnee(final String codeFormation, final String annee) {
		return promotionRepository.findByCodeFormationAndAnneeUniversitaire(codeFormation, annee);
	}

}
