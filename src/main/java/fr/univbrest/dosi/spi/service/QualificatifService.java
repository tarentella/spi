package fr.univbrest.dosi.spi.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.dao.QualificatifRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 *
 */
@Service
public class QualificatifService {

	@Autowired
	private QualificatifRepository qualificatifRepository;

	public void addQualificatif(final Qualificatif qualificatif) {
		try {
			qualificatifRepository.save(qualificatif);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public final void deleteQualificatif(final Long codeQualif) {
		try {
			if (!qualificatifRepository.exists(codeQualif)) {
				throw new SPIException("Echec de la suppression. Qualificatif introuvable");
			}
			qualificatifRepository.delete(codeQualif);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public Boolean existQualificatif(final Long codeQualif) {
		return qualificatifRepository.exists(codeQualif);
	}

	public List<Qualificatif> getAll() {
		try {
			final List<Qualificatif> qualif = qualificatifRepository.findAll(); 

			Collections.sort(qualif, new Comparator<Qualificatif>() {
				public int compare(Qualificatif e1, Qualificatif e2) {
					return e1.getMaximal().compareTo(e2.getMaximal());
				}
			});
			return qualif;
		} catch (final Exception e) {
			throw new SPIException("Erreur base de données", e);
		}
	}

	public final Qualificatif getQualificatif(final Long codeQualif) {
		try {
			if (!qualificatifRepository.exists(codeQualif)) {
				throw new SPIException("Qualificatif introuvable");
			}
			return qualificatifRepository.findByIdQualificatif(codeQualif);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public final void updateQualificatif(final Qualificatif qualif) {
		try {
			if (!qualificatifRepository.exists(qualif.getIdQualificatif())) {
				throw new SPIException("Echec de la modification. Qualificatif introuvable");
			}
			qualificatifRepository.save(qualif);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}
}
