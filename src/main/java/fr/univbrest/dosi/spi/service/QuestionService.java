package fr.univbrest.dosi.spi.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.bean.Question;
import fr.univbrest.dosi.spi.dao.QuestionRepository;

@Service
public class QuestionService {

	@Autowired
	private QuestionRepository questionRepository;
	
	public List<Question> getAllQuestion(){
		//return questionRepository.findAll();
		
		final List<Question> qualif = questionRepository.findAll(); 

		Collections.sort(qualif, new Comparator<Question>() {
			public int compare(Question q1, Question q2) {
				return q1.getIntitule().compareTo(q2.getIntitule());
			}
		});
		return qualif;
		
		
		
		
	}
	
	public Question getQuestion(Long id){
		return questionRepository.findOne(id);
	}
	
	public void addQuestion(Question question){
		questionRepository.save(question);
	}
	
	public void deleteQuestion(Long idQuestion){
		questionRepository.delete(idQuestion);
	}
	
	public void updateQuestion(Question question){
		questionRepository.save(question);
	}
	
}
