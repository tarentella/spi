package fr.univbrest.dosi.spi.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Evaluation;
import fr.univbrest.dosi.spi.bean.Rubrique;
import fr.univbrest.dosi.spi.bean.RubriqueEvaluation;
import fr.univbrest.dosi.spi.dao.RubriqueEvaluationRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

@Service
public class RubriqueEvaluationService {

	@Autowired
	EvaluationService evaluationService;

	@Autowired
	RubriqueEvaluationRepository rubriqueEvaluationRepository;

	@Autowired
	RubriqueStandardService rubriqueService;


	public final void addRubriqueEvaluation(final RubriqueEvaluation rubriqueEvaluation) {
		try {
			rubriqueEvaluationRepository.save(rubriqueEvaluation);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public void deleteRubEvalByRubEval(final Long idEval, final Long idRub) {
		try {
			Iterable<RubriqueEvaluation> lre = new ArrayList();
			lre = rubriqueEvaluationRepository.findByIdEvaluation(evaluationService.getEvaluation(idEval));

			for (final RubriqueEvaluation x : lre) {
				if (x.getIdRubrique().getIdRubrique() == idRub) {
					if (!rubriqueEvaluationRepository.exists(x.getIdRubriqueEvaluation())) {
						throw new SPIException("Echec de la suppression. Rubrique Evaluation introuvable");
					}
					rubriqueEvaluationRepository.delete(x);
					break;
				}
			}

		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}


	public void deleteRubriqueEvaluation(final Long idRubriqueEvaluation) {
		try {
			if (!rubriqueEvaluationRepository.exists(idRubriqueEvaluation)) {
				throw new SPIException("Echec de la suppression. Rubrique Evaluation introuvable");
			}
			rubriqueEvaluationRepository.delete(idRubriqueEvaluation);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public Iterable<RubriqueEvaluation> getAll() {
		try {
			return rubriqueEvaluationRepository.findAll();
		} catch (final Exception e) {
			throw new SPIException("Erreur base de données", e);
		}
	}


	public List<Rubrique> getRubByEva(final Evaluation eva) {
		try {
			final Iterable<RubriqueEvaluation> lre = rubriqueEvaluationRepository.findByIdEvaluation(eva);
			final List<Rubrique> lr = new ArrayList();
			for (final RubriqueEvaluation x : lre) {
				lr.add(rubriqueService.getRubrique(x.getIdRubrique().getIdRubrique()));
			}
			return lr;
		} catch (final Exception e) {
			throw new SPIException("Erreur base de données", e);
		}
	}

	public Iterable<RubriqueEvaluation> getRubEvalByEval(final Evaluation eva) {
		try {
			return rubriqueEvaluationRepository.findByIdEvaluation(eva);
		} catch (final Exception e) {
			throw new SPIException("Erreur base de données", e);
		}
	}


	public final RubriqueEvaluation getRubriqueEvaluation(final Long idRubriqueEvaluation) {
		try {
			if (!rubriqueEvaluationRepository.exists(idRubriqueEvaluation)) {
				throw new SPIException("Rubrique evaluation introuvable");
			}
			return rubriqueEvaluationRepository.findByIdRubriqueEvaluation(idRubriqueEvaluation);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public final void updateRubriqueEvaluation(final RubriqueEvaluation rubriqueEvaluation) {
		try {
			if (!rubriqueEvaluationRepository.exists(rubriqueEvaluation.getIdRubriqueEvaluation())) {
				throw new SPIException("Echec de la modification. Rubrique Evaluation introuvable");
			}
			rubriqueEvaluationRepository.save(rubriqueEvaluation);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

}
