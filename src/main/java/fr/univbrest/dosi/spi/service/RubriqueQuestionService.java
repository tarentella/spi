package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.RubriqueQuestion;
import fr.univbrest.dosi.spi.bean.RubriqueQuestionPK;
import fr.univbrest.dosi.spi.dao.RubriqueQuestionRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 *
 */
@Service
public class RubriqueQuestionService {

	@Autowired
	private RubriqueQuestionRepository rubriqueQuestionRepository;

	public final void addRubriqueQuestion(final RubriqueQuestion RubriqueQuestion) {
		try {
			rubriqueQuestionRepository.save(RubriqueQuestion);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public final void deleteRubriqueQuestion(final RubriqueQuestionPK RubriqueQuestionPK) {
		try {
			if (!rubriqueQuestionRepository.exists(RubriqueQuestionPK)) {
				throw new SPIException("Echec de la suppression. Qualificatif introuvable");
			}
			rubriqueQuestionRepository.delete(RubriqueQuestionPK);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public final Boolean existRubriqueQuestion(final RubriqueQuestionPK RubriqueQuestionPK) {
		return rubriqueQuestionRepository.exists(RubriqueQuestionPK);
	}

	public Iterable<RubriqueQuestion> getAll() {
		try {
			return rubriqueQuestionRepository.findAll();
		} catch (final Exception e) {
			throw new SPIException("Erreur base de données", e);
		}
	}

	public final RubriqueQuestion getRubriqueQuestion(final RubriqueQuestionPK RubriqueQuestionPK) {
		try {
			if (!rubriqueQuestionRepository.exists(RubriqueQuestionPK)) {
				throw new SPIException("Qualificatif introuvable");
			}
			return rubriqueQuestionRepository.findOne(RubriqueQuestionPK);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

	public final void updateRubriqueQuestion(final RubriqueQuestion RubriqueQuestion) {
		try {
			if (!rubriqueQuestionRepository.exists(RubriqueQuestion.getRubriqueQuestionPK())) {
				throw new SPIException("Echec de la modification. Rubrique question introuvable");
			}
			rubriqueQuestionRepository.save(RubriqueQuestion);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}
}
