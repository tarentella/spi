package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import fr.univbrest.dosi.spi.bean.Rubrique;
import fr.univbrest.dosi.spi.dao.RubriqueStandardRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

@Service
public class RubriqueStandardService {

	@Autowired
	private RubriqueStandardRepository rubriqueStandardRepository;
	
	public final Rubrique addRubrique(final Rubrique rubrique) {
		//Integer idRubrique = rubrique.getIdRubrique().intValue();
		return rubriqueStandardRepository.save(rubrique);
	}
	
	public final void deleteRubrique(final Long idRubrique) {
		//Integer idRubrique = rubrique.getIdRubrique().intValue();
		if (rubriqueStandardRepository.exists(idRubrique)) {
			rubriqueStandardRepository.delete(idRubrique);
		} else {
			throw new SPIException("Cant delete Rubrique");
		}
	}
	
	public final Rubrique updateRubrique(final Rubrique rubrique) {
		//Integer idRubrique = rubrique.getIdRubrique().intValue();
		if (rubriqueStandardRepository.exists(rubrique.getIdRubrique())) {
			return rubriqueStandardRepository.save(rubrique);
		} else {
			throw new SPIException("la rubrique que vous souhaitez modifier n'exsite pas ");
		}
	}
	
	public final Iterable<Rubrique> listrub() {
		final Iterable<Rubrique> rubrique = rubriqueStandardRepository.findAll();
		return rubrique;
	}
	
	public final Rubrique getRubrique(final Long idRubrique) {
		return rubriqueStandardRepository.findOne(idRubrique);
	}
	
	
	
}
