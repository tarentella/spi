package fr.univbrest.dosi.spi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.UniteEnseignement;
import fr.univbrest.dosi.spi.bean.UniteEnseignementPK;
import fr.univbrest.dosi.spi.dao.UniteEnseignementRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 *
 */
@Service
public class UniteEnseignementService {

	@Autowired
	private EnseignantService enseignantService;

	@Autowired
	private UniteEnseignementRepository uniteEnseignementRepository;

	public void addUnitEnseignement(final UniteEnseignement uniteEnseignement) {
		uniteEnseignementRepository.save(uniteEnseignement);
	}

	public final void deletUnitEnseignement(final UniteEnseignementPK uniteEnseignementPK) {
		uniteEnseignementRepository.delete(uniteEnseignement(uniteEnseignementPK));
	}

	public Boolean existUnitEnseignement(final UniteEnseignementPK uniteEnseignementPK) {
		return uniteEnseignementRepository.exists(uniteEnseignementPK);
	}

	public Iterable<UniteEnseignement> getAll() {
		return uniteEnseignementRepository.findAll();
	}

	public List<UniteEnseignement> getUEByCodeFormation(final String codeFormation) {
		return uniteEnseignementRepository.findByCodeFormation(codeFormation);
	}

	public final List<UniteEnseignement> getUEByEnseignant(final Integer noEnseignant) {

		return uniteEnseignementRepository.findByNoEnseignant(enseignantService.getEnseignant(noEnseignant));

	}

	public List<UniteEnseignement> listeUniteEnseignement() {
		return uniteEnseignementRepository.findAll();
	}

	public final UniteEnseignement uniteEnseignement(final UniteEnseignementPK uniteEnseignementPK) {
		return uniteEnseignementRepository.findOne(uniteEnseignementPK);
	}
	
	public final void updateUniteEnseignement(final UniteEnseignement uniteEnseignement) {
		try {
			if (!uniteEnseignementRepository.exists(uniteEnseignement.getUniteEnseignementPK())) {
				throw new SPIException("Echec de la modification. introuvable");
			}
			uniteEnseignementRepository.save(uniteEnseignement);
		} catch (final Exception e) {
			throw new SPIException(e.getMessage(), e);
		}
	}

}
