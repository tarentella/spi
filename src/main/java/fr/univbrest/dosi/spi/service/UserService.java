package fr.univbrest.dosi.spi.service;

//@Service
//public class UserService {
//
//	private final Map<String, User> mapBouchonUser;
//
//	public UserService() {
//		mapBouchonUser = new HashMap<String, User>();
//		mapBouchonUser.put("chakib", new User("chakib", "1234", Arrays.asList("Admin")));
//		mapBouchonUser.put("zou", new User("zou", "1234", Arrays.asList("visiteur")));
//		mapBouchonUser.put("mouad", new User("mouad", "1234", Arrays.asList("Prof")));
//		mapBouchonUser.put("daouda", new User("daouda", "daouda", Arrays.asList("Admin")));
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Authentification;
import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.User;
import fr.univbrest.dosi.spi.dao.AuthentificationRepository;

@Service
public class UserService {
	/**
	 *
	 */
	@Autowired
	private AuthentificationRepository authentificationRepository;

	@Autowired
	private EnseignantService enseignantService;

	public UserService() {

	}

	/**
	 * @param login
	 * @param pwd
	 * @return
	 */
	public User authentifier(final String login, final String pwd) {

		Authentification user = authentificationRepository.findByPseudoConnection(login);
		if (user == null) {
			user = authentificationRepository.findByLoginConnection(login);
		}
		if (user != null) {
			if (user.getRole().equalsIgnoreCase("etu") || user.getRole().equalsIgnoreCase("sec")) {
				return null;
			}
			if (user.getMotPasse().equals(pwd)) {
				if (user.getRole().equalsIgnoreCase("adm")) {
					return new User(login, pwd, user.getRole(), "Administrateur", 1);
				}
				if (user.getRole().equalsIgnoreCase("ens")) {

					final Enseignant ens = enseignantService.getEnseignant(user.getNoEnseignant().getNoEnseignant());
					final String nomENS = ens.getNom() + " " + ens.getPrenom();
					final Integer noENS = ens.getNoEnseignant();

					return new User(login, pwd, user.getRole(), nomENS, noENS);
				}
			}
		}
		return null;

	}
}
