(function() {
  'use strict';

  var app = angular.module('app.enseignants', []);

  Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? null : v;
	   });
	   this.length = 0; //clear original array
	   this.push.apply(this, array); //push all elements except the one we want to delete
	}
  
  Array.prototype.retourValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? v : null;
	   });
	   return array[0];
	}
  
  app.service('ensService',function(){
	  var ens = {};
  })
  
  app.factory('enseignantsFactory', ['$http',function($http){
    var list = function() {
        //var defer = $q.defer();

        return  $http.get('http://localhost:8090/ens')
        /*.then(function(response) {
           defer.resolve(response.data);
         });

         return defer.promise;*/
       };
    	/*[ 
      // TODO Constituer la liste des enseignants ici
      {
    	  no_enseignant : "1", nom : "SALIOU", prenom : "Philippe", 
    	  email :"philippe.saliou@gmail.com", sexe : "H", adresse :"6 rue de l'Argoat" ,
    	  code_postal : "29860", ville : "LE DRENNEC", telephone : "06.29.24.01.00",
    	  pays : "FR", type : "MCF"
    		  
      },
      {
    	  no_enseignant : "2", nom : "LALLALI", prenom : "Mounir", 
    	  email :"mouni.lallali@gmail.com", sexe : "H", adresse :"18rue Jean Jaurès" ,
    	  code_postal : "29200", ville : "BREST", telephone : "06.32.03.56.32",
    	  pays : "FR", type : "MCF"
      },
      {
    	  no_enseignant : "3", nom : "LEROUX", prenom : "Pierre", 
    	  email :"pileroux@gmail.com" , sexe : "H", adresse :"65 route de Gouesnou" ,
    	  code_postal : "29200", ville : "BREST", telephone : "06.45.95.47.29",
    	  pays : "FR", type : "INT"
      }
    ];*/
            
    var details = [ 
      // Constituer le délail de la liste des enseignants ici
    ];

    return {
      // renvoi la liste de tous les enseignants
      all:list,// function() { //return list; 

     // },
      // renvoi l'enseignant avec le no_enseignant demandé
      get: function(noEnseignant) { 
    	  // TODO retourner les enseignants
    	  console.log("TODO : get enseignant",noEnseignant);
    	  //return list.retourValue("no_enseignant",idx);
    	  return  $http.get('http://localhost:8090/getens/'+noEnseignant);
    	  
   	  },
      set: function(enseignant) {
        var idx = enseignant.noEnseignant;
        // si modification d'un enseignant existant
        if(idx){
          // TODO alimenter l'objet enseignant trouvé
        	console.log("TODO : update enseignant",idx);
        	//list.removeValue("no_enseignant",enseignant.no_enseignant);
        	//return list.push(enseignant);
        	
        	var newEnseignant = {
          		      "noEnseignant" : idx,
         			  "nom" : enseignant.nom,
         			  "prenom" : enseignant.prenom,
         			  "type" : enseignant.type,
         			  "sexe" : enseignant.sexe,
         			  "adresse" : enseignant.adresse,
         			  "codePostal" : enseignant.codePostal,
         			  "ville" : enseignant.ville,
         			  "pays" : enseignant.pays,
         			  "mobile" : enseignant.mobile,
         			  "telephone" : enseignant.telephone,
         			  "emailPerso" : enseignant.emailPerso,
         			  "emailUbo" : enseignant.emailUbo,
         		  };      	  
  	        	return $http.post('http://localhost:8090/updateEnseignant',newEnseignant);
        } else { // si ajout d'un nouvel enseignant
          // TODO ajouter un enseignant à la liste
        	
        	  var newEnseignant = {
       			  "nom" : enseignant.nom,
       			  "prenom" : enseignant.prenom,
       			  "type" : enseignant.type,
       			  "sexe" : enseignant.sexe,
       			  "adresse" : enseignant.adresse,
       			  "codePostal" : enseignant.codePostal,
       			  "ville" : enseignant.ville,
       			  "pays" : enseignant.pays,
       			  "mobile" : enseignant.mobile,
       			  "telephone" : enseignant.telephone,
       			  "emailPerso" : enseignant.emailPerso,
       			  "emailUbo" : enseignant.emailUbo,
       		  };      	  
	        	return $http.post('http://localhost:8090/ajouterEnseignant',newEnseignant);
        	
        	//return list.push(enseignant);
        }
      },
      delete: function(noEnseignant) { 
        // TODO Supprimer 
    	  console.log("TODO : supprimer enseignant",noEnseignant);
    	  return  $http.get('http://localhost:8090/deleteEnseignant/'+noEnseignant)
    	  //list.removeValue("no_enseignant",enseignant.no_enseignant);
    	  //return list;
      },

      getPromotion : function(noEnseignant){
    	  var url = "http://localhost:8090/getpromotionenseignant/"+noEnseignant;
    	  return $http.get(url);
      },
      
      getUE : function(noEnseignant){
	      var url = "http://localhost:8090/getuebyenseignant/"+noEnseignant;
		  return $http.get(url);
      },
      getMeaning : function(rvAbbreviation){
	      var url = "http://localhost:8090/getCgrefRvMeaning/"+rvAbbreviation;
		  return $http.get(url);
      }
    };
  }]);

  

  app.controller('EnseignantsController', 
    ['$scope', '$filter','$location', 'enseignantsFactory','ensService','rvDomainService',
    function($scope, $filter, $location, enseignantsFactory, ensService,rvDomainService){
    	
    	var pays={};
    	//pagination enseignant
    	var init;
    
    	enseignantsFactory.all()
		.success(function(data) {
			 $scope.enseignants =data;
/////////////////////////////domaine////////////////////////////////////////////////////////////////////////
		   
		    //domaines
  		  
  		  $scope.pays = rvDomainService.pays;
  		   	 
  		   	 for(var i=0; i<$scope.enseignants.length; i++){
  		       	//console.log("Boucle : "+i);
  		       	//console.log("Boucle i: "+$scope.formations[i].rvDomain);
  		       	for(var j=0; j<$scope.pays.length; j++){
//  			       	console.log("Boucle J : "+i+" Boucle i : "+i);
//  			       	console.log("Boucle J: "+$scope.formations[i].doubleDiplome);
  		        if( $scope.enseignants[i].pays == $scope.pays[j].rvAbbreviation){
  		       	
  		        	 $scope.enseignants[i].pays = $scope.pays[j].rvMeaning;
  		       	
  		       	console.log("Dans if pour le service : "+ $scope.enseignants[i].pays);
  		       	
  		       	}
  		       	}
  		       	}
  		  
////////////////////////////////////////////////////////////////////////
		    
		    	
		      $scope.searchKeywords = '';
		      $scope.filteredEnseignant = [];
		      $scope.row = '';
		      $scope.select = function(page) {
		        var end, start;
		        start = (page - 1) * $scope.numPerPage;
		        end = start + $scope.numPerPage;
		        return $scope.currentPageEnseignant = $scope.filteredEnseignant.slice(start, end);
		      };
		      $scope.onFilterChange = function() {
		        $scope.select(1);
		        $scope.currentPage = 1;
		        return $scope.row = '';
		      };
		      $scope.onNumPerPageChange = function() {
		        $scope.select(1);
		        return $scope.currentPage = 1;
		      };
		      $scope.onOrderChange = function() {
		        $scope.select(1);
		        return $scope.currentPage = 1;
		      };
		      $scope.search = function() {
		        $scope.filteredEnseignant = $filter('filter')($scope.enseignants, $scope.searchKeywords);
		        return $scope.onFilterChange();
		      };
		      $scope.order = function(rowName) {
		        if ($scope.row === rowName) {
		          return;
		        }
		        $scope.row = rowName;
		        $scope.filteredEnseignant = $filter('orderBy')($scope.enseignants, rowName);
		        return $scope.onOrderChange();
		      };
		      $scope.numPerPageOpt = [3, 5, 10, 20];
		      $scope.numPerPage = $scope.numPerPageOpt[2];
		      $scope.currentPage = 1;
		      $scope.currentPageEnseignant = [];
		      init = function() {
		        $scope.search();
		        return $scope.select($scope.currentPage);
		      };
		      return init();
		  }
		)
		.error(function(data) {
			 $scope.error = 'unable to get the poneys';
		  }
		);
      // la liste globale des enseignants
      //$scope.enseignants = enseignantsFactory.all();          

      //$scope.formations = [];
      
      /*listfrm.fetchPopular(function(data) {
        	  console.log("TODO 1: entrer fetchpopular");
    			$scope.formations = data;
    			console.log("TODO 2: donnee " + data);
    			console.log("TODO 3: fin fetchpopular");
    		});*/
      
      // Crée la page permettant d'ajouter un enseignant
      // TODO Lien vers la page permettant de créer un enseignant /admin/enseignant/nouveau
      $scope.ajoutEnseignant = function(){
          $location.path('/admin/enseignant/nouveau'); 
       }
      // accès à la modification
      $scope.edition = function(enseignant){
    	  ensService.ens = enseignant;
          $location.path('/admin/enseignant/modif'); 
       }
      
      // affiche les détail d'un enseignant
      // TODO Lien vers la page permettant d'éditer un enseignant /admin/enseignant/ + no_enseignant
      $scope.edit = function (ens){
    	  ensService.ens = ens;
    	  $location.path("/admin/enseignant/"+ ens.noEnseignant);
    	  //alert(enseignant.no_enseignant);
      }

      // supprime un enseignant
      $scope.supprime = function(enseignant){ 
    	// TODO Suppression d'un enseignant de la liste
    	  /////////////////////////////
          	swal({
          		title: "Voulez-vous supprimer cet ENSEIGNANT ?",
          		type: "warning",   
        		  	showCancelButton: true,   
        		  	confirmButtonColor: "#eec95a",   
        		  	confirmButtonText: "Oui!",  
        		  	cancelButtonText: "Non!",   
        		  	closeOnConfirm: false,   closeOnCancel: false },
        		  function(isConfirm){
        		  	 if (isConfirm) {       	
        		  		var promise= enseignantsFactory.delete(enseignant.noEnseignant);
        	           	  promise.success(function(data,statut){
        	           		//$scope.currentPageRubrique.removeValue("idRubrique",rubrique.idRubrique);
        	           		swal("Supprimé!", "rubrique supprimé", "success");
        	          // 		$scope.refresh();
        	           		var index = $scope.currentPageEnseignant.indexOf(enseignant);
        	           		$scope.currentPageEnseignant.splice(index,1);
        	           	  });
        	           	promise.error(function(data,statut, headers, config){
        	           	swal("Erreur!", "Impossible de supprimer l'enseignant choisi", "error");
              });
        		  	 }else{
        		  		swal("Ignorer", "", "error");
        		  		}
        		  	 });
      }
      ////////////////////////////////////
    }]
  );

  app.controller('EnsDetailsController', 
    ['$scope', '$routeParams', '$location', 'enseignantsFactory','ensService','rvDomainService',
    function($scope, $routeParams, $location, enseignantsFactory, ensService, rvDomainService){      
      $scope.edit= false;    

      // si creation d'un nouvel enseignant
      if($routeParams.id == "nouveau"){
        $scope.enseignant= { };
        $scope.type= rvDomainService.type;
        $scope.pays= rvDomainService.pays;
        $scope.edit= true;    
      } else { // sinon on edite un enseignant existant
    	  
    	  $scope.enseignant = ensService.ens;
    	  

            var promise= enseignantsFactory.getPromotion($scope.enseignant.noEnseignant);
            promise.success(function(data,statut){
          	  $scope.enseignant.promotions = data ;
            })
            .error(function(data,statut){
          	  console.log("impossible de recuperer les details de l'enseignant choisi");
            });
            

            var promise1= enseignantsFactory.getUE($scope.enseignant.noEnseignant);
            promise1.success(function(data,statut){
          	  $scope.enseignant.ue = data ;
            })
            .error(function(data,statut){
          	  console.log("impossible de recuperer les details de l'enseignant choisi");
            });

      }
      
      if($routeParams.id == "modif"){
    	  $scope.edit = true;
    	  $scope.enseignant = ensService.ens;
    	  $scope.type= rvDomainService.type;
          $scope.pays= rvDomainService.pays;
      }

      $scope.edition = function(){
        $scope.edit = true;
      }

      // valide le formulaire d'édition d'un enseignant
      $scope.submit = function(){    	 
  	  if($scope.enseignant.noEnseignant){

       	   var $maPromise1 = enseignantsFactory.set($scope.enseignant);
   		   	   $maPromise1.success(function(data){
   			   console.log("N°  "+ $scope.enseignant.noEnseignant);
  			   $location.path('/admin/enseignants');
   		   }).error(function(error){
   			   console.log("Erreur  "+error);
   		   })
   		   
      	  }else{
       		  
    		   var $maPromise1 = enseignantsFactory.set($scope.enseignant);
     		   $maPromise1.success(function(data){
    			   console.log("N° ens  "+ $scope.enseignant.noEnseignant);
    			   $location.path('/admin/enseignants');
    		   }).error(function(error){
     			   console.log("Erreur saisit"+error);
     		   })
     		     
           }
       	  
       	  $scope.edit = false;
      }

      // annule l'édition
      $scope.cancel = function(){
        // si ajout d'un nouvel enseignant => retour à la liste des enseignants
    	  
    	  $location.path('/admin/enseignants');
    	  
//        if(!$scope.enseignant.noEnseignant){
//          $location.path('/admin/enseignants');
//        } else {
//          var e = enseignantsFactory.get($routeParams.id);
//          $scope.enseignant = JSON.parse(JSON.stringify(e));
//          $scope.edit = false;
//        }
      }      
    }]
  );
}).call(this);
