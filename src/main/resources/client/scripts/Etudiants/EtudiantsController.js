(function() {
  'use strict';

  var app = angular.module('app.etudiants', []);

  Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? null : v;
	   });
	   this.length = 0; //clear original array
	   this.push.apply(this, array); //push all elements except the one we want to delete
	}
  
  Array.prototype.retourValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? v : null;
	   });
	   return array[0];
	}
  
  app.service('etudService',function(){
	  var etud = {};
	  
//	  return{
//		  set:function(form, annee){
//			  codeF = form;
//			  anneeU = annee;
//			  return ""+codeF+""+anneeU;
//		  
//	  }
//	  };
  })
  
  app.factory('etudiantsFactory', ['$http',function($http){
    var list = function() {

        return  $http.get('http://localhost:8090/etudiants')
       };
       
    
               
    var details = [ 
      // Constituer le délail de la liste des etudiants ici
    ];

    return {
      // renvoi la liste de tous les etudiants
      all:list,// function() { //return list; 

     // },
      // renvoi l'etudiant avec le no_etudiant demandé
      get: function(noEtudiant) { 
    	  // TODO retourner les etudiants
    	  console.log("TODO : get etudiant",noEtudiant);
    	  //return list.retourValue("no_etudiant",idx);
    	  return  $http.get('http://localhost:8090/getEtud/'+noEtudiant);
    	  
   	  },
	   	getEtudiantsPromo: function() { 
	  	  // TODO retourner les etudiants
	  	  return  $http.get('http://localhost:8090/promotion/getEtuPro/M2DOSI/2014-2015');
	  	  
	 	  },
      set: function(etudiant) {
        var idx = etudiant.noEtudiant;
        // si modification d'un etudiant existant
        if(idx){
          // TODO alimenter l'objet etudiant trouvé
        	console.log("TODO : update etudiant",idx);
        	//list.removeValue("no_etudiant",etudiant.no_etudiant);
        	//return list.push(etudiant);
        	
        	var newEtudiant = {
          		      "noEtudiant" : idx,
         			  "nom" : etudiant.nom,
         			  "prenom" : etudiant.prenom,
         			  "sexe" : etudiant.sexe,
         			  "adresse" : etudiant.adresse,
         			  "nationalite" : etudiant.nationalite,
         			  "ville" : etudiant.ville,
         			  "dateNaissance" : etudiant.dateNaissance,
         			  "lieuNaissance" : etudiant.lieuNaissance,
         			  "paysOrigine" : etudiant.paysOrigine,
         			  "email" : etudiant.email,
         			  "universiteOrigine" : etudiant.universiteOrigine,
         		  };      	  
  	        	$http.post('http://localhost:8090/updateEtudiant',newEtudiant);
        } else { // si ajout d'un nouvel etudiant
          // TODO ajouter un etudiant à la liste
        	
        	  var newetudiant = {
        		  "noEtudiant" : etudiant.num,
       			  "nom" : etudiant.nom,
       			  "prenom" : etudiant.prenom,
       			  "sexe" : etudiant.sexe,
       			  "adresse" : etudiant.adresse,
       			  "nationalite" : etudiant.nationalite,
       			  "ville" : etudiant.ville,
       			  "dateNaissance" : etudiant.dateNaissance,
       			  "lieuNaissance" : etudiant.lieuNaissance,
       			  "paysOrigine" : etudiant.paysOrigine,
       			  "email" : etudiant.email,
       			  "universiteOrigine" : etudiant.universiteOrigine,
       		  };      	  
	        	$http.post('http://localhost:8090/ajouterEtudiant',newetudiant);

        }
      },
      delete: function(noEtudiant) { 
        // TODO Supprimer 
    	  console.log("TODO : supprimer etudiant",noEtudiant);
    	  return  $http.get('http://localhost:8090/deleteEtudiant/'+noEtudiant)
      },

//      getPromotion : function(noEtudiant){
//    	  var url = "http://localhost:8090/getpromotionetudiant/"+noEtudiant;
//    	  return $http.get(url);
//      }      
    };
  }]);

  

  app.controller('EtudiantsController', 
    ['$scope', '$filter','$location', 'etudiantsFactory','etudService',
    function($scope, $filter, $location, etudiantsFactory, etudService){
    	//pagination etudiant
    	var init;
//    	var $state = $injector.get('$state');
//    	$state.reload();
    	var promise44 = etudiantsFactory.getEtudiantsPromo();
    	promise44
		.success(function(data) {
		    $scope.etudiants = data;
		      $scope.searchKeywords = '';
		      $scope.filteredEtudiant = [];
		      $scope.row = '';
		      $scope.select = function(page) {
		        var end, start;
		        start = (page - 1) * $scope.numPerPage;
		        end = start + $scope.numPerPage;
		        return $scope.currentPageEtudiant = $scope.filteredEtudiant.slice(start, end);
		      };
		      $scope.onFilterChange = function() {
		        $scope.select(1);
		        $scope.currentPage = 1;
		        return $scope.row = '';
		      };
		      $scope.onNumPerPageChange = function() {
		        $scope.select(1);
		        return $scope.currentPage = 1;
		      };
		      $scope.onOrderChange = function() {
		        $scope.select(1);
		        return $scope.currentPage = 1;
		      };
		      $scope.search = function() {
		        $scope.filteredEtudiant = $filter('filter')($scope.etudiants, $scope.searchKeywords);
		        return $scope.onFilterChange();
		      };
		      $scope.order = function(rowName) {
		        if ($scope.row === rowName) {
		          return;
		        }
		        $scope.row = rowName;
		        $scope.filteredEtudiant = $filter('orderBy')($scope.etudiants, rowName);
		        return $scope.onOrderChange();
		      };
		      $scope.numPerPageOpt = [3, 5, 10, 20];
		      $scope.numPerPage = $scope.numPerPageOpt[2];
		      $scope.currentPage = 1;
		      $scope.currentPageEtudiant = [];
		      init = function() {
		        $scope.search();
		        return $scope.select($scope.currentPage);
		      };
		      return init();
		  }
		)
		.error(function(data) {
			 $scope.error = 'unable to get the poneys';
		  }
		);
      
      // Crée la page permettant d'ajouter un etudiant
      // TODO Lien vers la page permettant de créer un etudiant /admin/etudiant/nouveau
      $scope.ajoutEtudiant = function(){
          $location.path('/admin/etudiant/nouveau'); 
       }
      
      // affiche les détail d'un etudiant
      // TODO Lien vers la page permettant d'éditer un etudiant /admin/etudiant/ + no_etudiant
      $scope.edit = function (etud){
    	  etudService.etud = etud;
    	  $location.path("/admin/etudiant/"+ etud.noEtudiant);
    	  //alert(etudiant.no_etudiant);
      }

      // supprime un etudiant
      $scope.supprime = function(noEtudiant){ 
    	// TODO Suppression d'un etudiant de la liste
    	  
    	  
    	  var promise= etudiantsFactory.delete(noEtudiant);
          promise.success(function(data,statut){
        	  //$scope.etudiant.promotions = data ;
        	  $scope.currentPageEtudiant.removeValue("noEtudiant",noEtudiant);
          })
          .error(function(data,statut){
        	  console.log("impossible de supprimer l'etudiant choisi");
          });
    	  
      }
    }]
  );

  app.controller('EtudDetailsController', 
    ['$scope', '$routeParams', '$location', 'etudiantsFactory','etudService',
    function($scope, $routeParams, $location, etudiantsFactory, etudService){      
      $scope.edit= false;    

      // si creation d'un nouvel etudiant
      if($routeParams.id == "nouveau"){
        $scope.etudiant= { };
        $scope.edit= true;    
      } else { // sinon on edite un etudiant existant
    	  
    	  $scope.etudiant = etudService.etud;
    	  
      }('x')

      $scope.edition = function(){
        $scope.edit = true;
      }

      // valide le formulaire d'édition d'un etudiant
      $scope.submit = function(){    	 
        etudiantsFactory.set($scope.etudiant);        
        $scope.edit = false;        
      }

      // annule l'édition
      $scope.cancel = function(){
        // si ajout d'un nouvel etudiant => retour à la liste des etudiants
        if(!$scope.etudiant.noEtudiant){
          $location.path('/admin/etudiants');
        } else {
          var e = etudiantsFactory.get($routeParams.id);
          $scope.etudiant = JSON.parse(JSON.stringify(e));
          $scope.edit = false;
        }
      }      
    }]
  );
}).call(this);
