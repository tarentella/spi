(function() {
  'use strict';

  var app = angular.module('app.evaluations', []);
  var ideva;
  var idEvaCourante;
  app.factory('evaeFactory',['$http','$rootScope', function($http,$rootScope){
    var list = [ 
      // TODO    
    ];
            
    return {
      // TODO
	    	all: function() { // TODO retourner la liste 
		    	  return  $http.get('http://localhost:8090/getAllEval')
		      },
	      set: function(evaluation) {
        
		       // si ajout d'une nouvelle rubrique
		          // TODO ajouter une rubrique à la liste
		        	
		        	  var newEval = {
		        				"annee": evaluation.annee,
		        			    "code_ec":evaluation.codeEc,
		        			    "code_formation":evaluation.codeFormation,
		        			    "code_ue":evaluation.codeUe,
		        			    "debutReponse":evaluation.debutReponse,
		        			    "designation":evaluation.designation,
		        			    "etat":evaluation.etat,
		        			    "finReponse":evaluation.finReponse,
		        			    "noEnseignant":"http://localhost:8090/getens/"+$rootScope.user.noEns,
		        			    "noEvaluation":evaluation.noEvaluation,
		        			    "periode":evaluation.periode
		        			}; 
		        	  console.log(newEval);
		        	  return $http.post('http://localhost:8090/addEval',newEval);
		      },
		      setrubeva: function(rubEva) {
		          
			       // si ajout d'une nouvelle rubrique
			          // TODO ajouter une rubrique à la liste
			        	
		    	  	console.log(rubEva);
			        	  var newRubEva = {
			        			  "designation": rubEva.designation,
			        			  "idEvaluation": "http://localhost:8090/getEval/"+idEvaCourante,
			        			  "idRubrique": "http://localhost:8090/rubrique/getRubrique/"+rubEva.idRubrique,
			        			  "ordre": rubEva.ordre
			        			  }; 
			        	  console.log(newRubEva);
			        	  return $http.post('http://localhost:8090/rubriqueEvaluation/addRubriqueEvaluation/',newRubEva);
			      },
		     getAllFrm: function(){
		    	 return  $http.get('http://localhost:8090/formations/');
		     },
		     getPromoByFrm: function(codeFormation){
		    	 return  $http.get('http://localhost:8090/getPromoByFrm/'+codeFormation);
		     },
		     getUeByFrm: function(codeFormation) {
		    	 return  $http.get('http://localhost:8090/getUEF/'+codeFormation);
		     },
		     getEcByUe: function(codeUe) {
		    	 return  $http.get('http://localhost:8090/getECUE/'+codeUe);
		     },
		     
		     geteva: function(idEvaluation) {
		    	 return $http.get('http://localhost:8090/getEval/'+idEvaluation);
		     },
		     listRubEva: function(idEvaluation) {
		    	 return $http.get('http://localhost:8090/rubriqueEvaluation/getRubEvalByEval/'+idEvaluation);
		    	 
		  
		     },
		     getRub: function(idRubEva){
		    	 return $http.get('http://localhost:8090/rubrique/getRubrique/'+idRub);
		     },
		     listRub: function(idEva){
		    	 return $http.get('http://localhost:8090/rubrique/getRubriqueByEva/'+idEva);
		     },
		     getAllRub:function(){
		    	 return $http.get('http://localhost:8090/rub');
		     },
		     deleteRub: function(idRub){

		    	 return $http.get('http://localhost:8090/rubriqueEvaluation/deleteByRubEval/'+idEvaCourante+'/'+idRub);

		     },
		     delete: function(idEvaluation){
		    	 return $http.get('http://localhost:8090/deleteEval/'+idEvaluation);
		     }
		     
    };
  }]);
  

  app.controller('EvaluationController', 
    ['$scope', '$location', 'evaeFactory','$filter','$http',
    function($scope, $location, evaeFactory, $filter,$http){
      // la liste globale des ue
      // TODO
    	var init;
    	var promise = evaeFactory.all();
    	promise.success(function(data) {
    		    $scope.evaluations = data;
    		    $scope.searchKeywords = '';
  		      $scope.filteredEvaluation = [];
  		      $scope.row = '';
  		      $scope.select = function(page) {
  		        var end, start;
  		        start = (page - 1) * $scope.numPerPage;
  		        end = start + $scope.numPerPage;
  		        return $scope.currentPageEvaluation = $scope.filteredEvaluation.slice(start, end);
  		      };
  		      $scope.onFilterChange = function() {
  		        $scope.select(1);
  		        $scope.currentPage = 1;
  		        return $scope.row = '';
  		      };
  		      $scope.onNumPerPageChange = function() {
  		        $scope.select(1);
  		        return $scope.currentPage = 1;
  		      };
  		      $scope.onOrderChange = function() {
  		        $scope.select(1);
  		        return $scope.currentPage = 1;
  		      };
  		      $scope.search = function() {
  		        $scope.filteredEvaluation = $filter('filter')($scope.evaluations, $scope.searchKeywords);
  		        return $scope.onFilterChange();
  		      };
  		      $scope.order = function(rowName) {
  		        if ($scope.row === rowName) {
  		          return;
  		        }
  		        $scope.row = rowName;
  		        $scope.filteredEvaluation = $filter('orderBy')($scope.evaluations, rowName);
  		        return $scope.onOrderChange();
  		      };
  		      $scope.numPerPageOpt = [3, 5, 10, 20];
  		      $scope.numPerPage = $scope.numPerPageOpt[2];
  		      $scope.currentPage = 1;
  		      $scope.currentPageEvaluation = [];
  		      init = function() {
  		        $scope.search();
  		        return $scope.select($scope.currentPage);
  		      };
  		      return init(); 
    		    
    		  }
    		)
    		.error(function(data) {
    			 $scope.error = 'unable to get the poneys';
    		  }
    		);
    	
    	
        
    	// affiche le formulaire d'une evaluation
        $scope.ajouter = function(){
          $location.path("/admin/evaluation/nouveau");
        }
        
        $scope.edit = function (eva){
         	  $location.path("/admin/evaluation/"+ eva.idEvaluation);
         	  idEvaCourante = eva.idEvaluation;
         	  
           }
       

      
        
        $scope.supprimer = function(eva){
        		
        		swal({
            		title: "Voulez-vous supprimer cette evaluation ?",
            		type: "warning",   
          		  	showCancelButton: true,   
          		  	confirmButtonColor: "#DD6B55",   
          		  	confirmButtonText: "Oui!",  
          		  	cancelButtonText: "Non!",   
          		  	closeOnConfirm: false,   closeOnCancel: false },
          		  function(isConfirm){
          		  	 if (isConfirm) {       	
          	           	  var promise= evaeFactory.delete(eva.idEvaluation);
          	           	  promise.success(function(data,statut){
          	           		swal("Supprimé!", "Rubrique supprimée", "success");
          	        		$scope.currentPageEvaluation.removeValue("idEvaluation", eva.idEvaluation);
          	           	  });
          	           	promise.error(function(data,statut, headers, config){
          	           	swal("Erreur!", "Impossible de supprimer l'evaluation choisie", "error");
                });
          		  	 }else{
          		  		swal("Ignorer", "", "error");
          		  		}
          		  	 });
        		
        	}
    }]
  );

  app.controller('EvaluationDetailsController', 
    ['$scope', '$routeParams', '$location', 'evaeFactory','$filter','rvDomainService',
    function($scope, $routeParams, $location, evaeFactory,$filter, rvDomainService){ 
    	
    	
    	
    	
    	
    	
    	
    	var promiserub = evaeFactory.getAllRub();
		
	        promiserub
			.success(function(data,status) {
				$scope.rubriques = data ;
				
			  });
	        
	        $scope.submitrub = function(){

           
   				  var $maPromise1 = evaeFactory.setrubeva($scope.rubEva);
   	      		   $maPromise1.success(function(data){
   	      			   
   	      			$location.path('/admin/evaluation/'+idEvaCourante);
   	      		});
   				    $maPromise1.error(function(error){
   	       			   console.log("Erreur saisit"+error);
   	       		   });  
   			    	  
           }
    	
      // TODO
    	$scope.add=false;
    	$scope.edit=false;
    	if($routeParams.id == "nouveau"){
    		$scope.IsHidden = true;
    		$scope.add=true;
    		$scope.edit=false;
            $scope.evaluation= { };
            $scope.formations = {};
            $scope.promotions = {};
            $scope.etat = rvDomainService.etat;
            $scope.ue = {};
            $scope.ec = {};
          }
    	else{
    		
    		
    		
   		 $scope.IsHidden = true;
    		 $scope.ShowHide = function () {
    	           
    	            $scope.IsHidden = $scope.IsHidden ? false : true;
    	        	
    	            //$scope.IsHidden =false;
    	        }
    		
    		
    		
    		
    		$scope.add=false;
    		$scope.edit=true;
    		
    		var promise = evaeFactory.geteva($routeParams.id);
    		
 	        promise
 			.success(function(data,status) {
 				$scope.eva = data ;
 				ideva=$scope.eva.idEvaluation;
 			  });
 	        
 	       $scope.ajoutRub = function(){
 	         

 	          $location.path("/admin/evaluations/ajoutRub");

 	        }
 	        
 	      $scope.cancelrub = function(){
 	    	  
 	    	
 	         $location.path("/admin/evaluation/"+ideva)
 	         
 	          
 	         
 	        }
 	      
 	     $scope.supprimerRub = function(rub){
//         	console.log(rub.idRubrique);
//         	var promiseRub = evaeFactory.deleteRub(rub.idRubrique);
//         	promiseRub.success(function(){
//         		$scope.currentPageQuestion.removeValue("idRubrique", rub.idRubrique);
//         		
         		swal({
            		title: "Voulez-vous supprimer cette rubrique ?",
            		type: "warning",   
          		  	showCancelButton: true,   
          		  	confirmButtonColor: "#DD6B55",   
          		  	confirmButtonText: "Oui!",  
          		  	cancelButtonText: "Non!",   
          		  	closeOnConfirm: false,   closeOnCancel: false },
          		  function(isConfirm){
          		  	 if (isConfirm) {       	
          	           	  var promise= evaeFactory.deleteRub(rub.idRubrique);
          	           	  promise.success(function(data,statut){
          	           		swal("Supprimé!", "Rubrique supprimée", "success");
          	           	$scope.currentPageQuestion.removeValue("idRubrique", rub.idRubrique);
          	           	  });
          	           	promise.error(function(data,statut, headers, config){
          	           	swal("Erreur!", "Impossible de supprimer la rubrique choisie", "error");
                });
          		  	 }else{
          		  		swal("Ignorer", "", "error");
          		  		}
          		  	 });
         		
         	}
 	     
 	       
 	        
 	       var promise = evaeFactory.listRub(idEvaCourante);
 	       
	       
	        promise
			.success(function(data,status) {
				//$scope.rub = data ;
				var init;
		    
		    		    $scope.questions = data;
		    	 	       console.log($scope.questions);

		    		    $scope.searchKeywords = '';
		  		      $scope.filteredQuestion = [];
		  		      $scope.row = '';
		  		      $scope.select = function(page) {
		  		        var end, start;
		  		        start = (page - 1) * $scope.numPerPage;
		  		        end = start + $scope.numPerPage;
		  		        return $scope.currentPageQuestion = $scope.filteredQuestion.slice(start, end);
		  		      };
		  		      $scope.onFilterChange = function() {
		  		        $scope.select(1);
		  		        $scope.currentPage = 1;
		  		        return $scope.row = '';
		  		      };
		  		      $scope.onNumPerPageChange = function() {
		  		        $scope.select(1);
		  		        return $scope.currentPage = 1;
		  		      };
		  		      $scope.onOrderChange = function() {
		  		        $scope.select(1);
		  		        return $scope.currentPage = 1;
		  		      };
		  		      $scope.search = function() {
		  		        $scope.filteredQuestion = $filter('filter')($scope.questions, $scope.searchKeywords);
		  		        return $scope.onFilterChange();
		  		      };
		  		      $scope.order = function(rowName) {
		  		        if ($scope.row === rowName) {
		  		          return;
		  		        }
		  		        $scope.row = rowName;
		  		        $scope.filteredQuestion = $filter('orderBy')($scope.questions, rowName);
		  		        return $scope.onOrderChange();
		  		      };
		  		      $scope.numPerPageOpt = [3, 5, 10, 20];
		  		      $scope.numPerPage = $scope.numPerPageOpt[2];
		  		      $scope.currentPage = 1;
		  		      $scope.currentPageQuestion = [];
		  		      init = function() {
		  		        $scope.search();
		  		        return $scope.select($scope.currentPage);
		  		      };
		  		      return init(); 
		    		    
		    		  }
		    		)
		    		.error(function(data) {
		    			 $scope.error = 'unable to get the poneys';
		    		  }
		    		);
				
				
				
				
// });
	        
	        
	        
	        
	        
	   
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	      
	        
	        
	        
	        
	        
 	        
 	        

    		
    		
    	}
    	
    	var $maPromise1 = evaeFactory.getAllFrm();
		   
    		$maPromise1.success(function(data){
			   $scope.formations = data;
			   console.log($scope.formations[1].codeFormation);
		   }).error(function(error){
			   console.log("Erreur saisit"+error);
		   });
    		
    		$scope.chargerPromoFrm = function(codeFormation){
    			 var $maPromise2 = evaeFactory.getPromoByFrm(codeFormation);
	       		   $maPromise2.success(function(data){
	       			 $scope.promotions = data
	       			 
	       		   }).error(function(error){
	       			   console.log("Erreur saisit"+error);
	       		   }); 
	       		   
		       		var $maPromise3 = evaeFactory.getUeByFrm(codeFormation);
		       		   $maPromise3.success(function(data){
		       			   $scope.ue = data;
		       		   }).error(function(error){
		       			   console.log("Erreur saisit"+error);
		       		   });
    		}
    		
    		$scope.chargerEc = function(ue){
    			var $maPromise4 = evaeFactory.getEcByUe(ue);
     		   $maPromise4.success(function(data){
     			   $scope.ec = data         			   
     			  
     		   }).error(function(error){
     			   console.log("Erreur saisit"+error);
     		  });
    		}
    	
    	// valide le formulaire d'édition d'une rubrique
//        $scope.submit = function(){
//
//         		   
//        	$scope.convert = function(){
//    		  $scope.evaluation.debutReponse = $filter('date')(data.debutReponse, "dd-MM-yyyy");
//    		  $scope.evaluation.finReponse = $filter('date')(data.finReponse, "dd-MM-yyyy");
//    	  }         		  
//       		   var $maPromise1 = evaeFactory.set($scope.evaluation);
//       		   $maPromise1.success(function(data){
//       			   console.log("Evaluation  "+ data);
//       			$location.path('/admin/evaluations');
//       			   
//       			  
//       		   }).error(function(error){
//       			   console.log("Erreur saisit"+error);
//       		   })
//       		     
//         	  
//        }
    		
    		
    		//Nouvelle version de laila
    		
    	     $scope.convert = function(){
         		  //je vais convertir la date
         		  var fin = new Date($scope.evaluation.finReponse);
     			  var debut = new Date($scope.evaluation.debutReponse);
     			  if (fin < debut) {
     				  console.log("ERREUR : début supérieur  à fin");
     	       		
     			  } else  {
     				  console.log("GOOD : début est inféreur à fin");
     				 
     			  }
            	    	       
         	  }
    		
    		// valide le formulaire d'ajout d'une évaluation
            $scope.submit = function(){

            	 var fin = new Date($scope.evaluation.finReponse);
    			  var debut = new Date($scope.evaluation.debutReponse);
    			  if (fin < debut) {
    				  return false;
    			  }else{
    				  var $maPromise1 = evaeFactory.set($scope.evaluation);
    	      		   $maPromise1.success(function(data){
    	      			   console.log("Evaluation  "+ data);
    	      			$location.path('/admin/evaluations');
    	      		});
    				    $maPromise1.error(function(error){
    	       			   console.log("Erreur saisit"+error);
    	       		   });  
    			  }  	  
            }
            ///////////////////////////
    		
    		
    		
    	$scope.cancel = function(){
    		$location.path("/admin/evaluations");
    	}
    }]
  );
})();
