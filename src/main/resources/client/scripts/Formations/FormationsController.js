(function() {
  'use strict';

  var app = angular.module('app.formations', []);
  
  Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? null : v;
	   });
	   this.length = 0; // clear original array
	   this.push.apply(this, array); // push all elements except the one we
										// want to delete
	 }
 
  Array.prototype.retourValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? v : null;
	   });
	   return array[0];
	}
  app.service('ueService',function(){
	  var ue = {};
	  var codeFormation;
	  var codeFrmUe;
	  var codeUe;
  })
  app.service('ecService',function(){
	  var ec = {};
	  var ue = {};
  })

  app.factory('formationsFactory',['$http', function($http){
	/*
	 * var getPoneys = function(callbackFn) {
	 * $http.get('/api/poneys').success(function(data) { callbackFn(data); }); };
	 */
		    
    var list = function() {
        // var defer = $q.defer();

       return  $http.get('http://localhost:8090/frm')
       /*
		 * .then(function(response) { defer.resolve(response.data); });
		 * 
		 * return defer.promise;
		 */
      };
    	/*
		 * function(callbackFn){
		 * $http.get("http://localhost:8090/frm").success(function(response) {
		 * console.log("TODO 1: entrer response" + response);
		 * callbackFn(response); }); }/* [ // TODO alimenter { code : "M2DOSI",
		 * diplome : "M", no_annee : "2", nom :"Master Développement à
		 * l'Offshore des Systèmes d'Information", double_diplome : "O",
		 * debut_accreditation :"01/09/12" , fin_accreditation : "30/09/17" }, {
		 * code : "M1TIIL", diplome : "M", no_annee : "1", nom :"Master
		 * technologie de l'Information et Ingénierie du Logiciel",
		 * double_diplome : "N", debut_accreditation :"01/09/12" ,
		 * fin_accreditation : "30/09/17" }, { code : "M2TIIL", diplome : "M",
		 * no_annee : "2", nom :"Master technologie de l'Information et
		 * Ingénierie du Logiciel", double_diplome : "N", debut_accreditation
		 * :"01/09/12" , fin_accreditation : "30/09/17" }, { code : "M2LSE",
		 * diplome : "M", no_annee : "2", nom :"Master Logiciel pour Système
		 * Embarqué", double_diplome : "N", debut_accreditation :"01/09/12" ,
		 * fin_accreditation : "30/09/17" } ]
		 */ 
    	;
            
    return {
      // renvoi la liste de tous les enseignants
      all: function() { // TODO retourner la liste
    	  return  $http.get('http://localhost:8090/formations') // list,//;
    	  // console.log("TODO : retourner la liste des formations",
			// formation);
    	  },
      // renvoi l'enseignant avec le code demandé
      get: function(codeFormation) { 
        // TODO Retourner un enregistrement
    	  console.log("TODO : get formation",codeFormation);
    	  // return list.retourValue("no_enseignant",idx);
    	  return  $http.get('http://localhost:8090/formation/'+codeFormation);
    	  // return list.retourValue("code",code);
    	  // console.log("TODO : retourner formation", formation);
      },
      
      getDomain: function(domaine) {
   	 return  $http.get('http://localhost:8090/getRvDomain/'+domaine);
     },
      
      
      getDomain: function(domaine) { 
      	  return  $http.get('http://localhost:8090/getRvDomain/'+domaine);
        },
      
      
     getUe: function(codeFormation) { 
          
      	  return  $http.get('http://localhost:8090/getUEF/'+codeFormation);
      	  
        },
        
    allEns: function() { 
        
    	  return  $http.get('http://localhost:8090/ens/');
    	  
      },
        
      getEc: function(codeUe) { 
    	  
        	  return  $http.get('http://localhost:8090/getECUE/'+codeUe);
          },
      
      
      
       deleteUe : function(ue){
        	  return $http.get('http://localhost:8090/ue/delete/'+ ue.uniteEnseignementPK.codeFormation +'/'+ ue.uniteEnseignementPK.codeUe);
        	  
          },
          
       deleteEc: function(Ec) { 
              return  $http.get('http://localhost:8090/deleteEC/'+Ec.elementConstitutifPK.codeFormation+"/"+Ec.elementConstitutifPK.codeUe+"/"+Ec.elementConstitutifPK.codeEc);
        },
      
      
      
      
      set: function(formation) {
        console.log("TODO : enregistrer formation", formation);
        
        	return $http.post('http://localhost:8090/formation/ajouterformation',formation);
          },
      setUE: function(ue,isModifue,isAjoutue) {
    	  
    	  var codeFormation = ue.uniteEnseignementPK.codeFormation;
          var codeUe = ue.uniteEnseignementPK.codeUe;
          if(codeFormation != null && codeUe != null && isModifue == true){
        	  
        	  console.log("TODO : code formation ue", ue.uniteEnseignementPK.codeFormation);
              console.log("TODO : code  ue", ue.uniteEnseignementPK.codeUe);
              console.log("TODO : code enseignant ue",ue.noEnseignant.noEnseignant);
              
              var noEns = 'http://localhost:8090/getue/'+ue.noEnseignant.noEnseignant;
    		  var formation = 'http://localhost:8090/formation/'+ue.uniteEnseignementPK.codeFormation;
    		  
    		  var newUE = {
  		        	 
    		          "description":ue.description,
    		          "designation":ue.designation,
    		        	 "formation":formation,
    		        	 "nbhCm":ue.nbhCm,
    		        	 "nbhTd":ue.nbhTd,
    		        	 "nbhTp":ue.nbhTp,
    		        	 "noEnseignant":noEns,
    		        	 "semestre":ue.semestre,
    		        	 "uniteEnseignementPK":{"codeFormation":ue.uniteEnseignementPK.codeFormation,"codeUe":ue.uniteEnseignementPK.codeUe}
    		      
    		          }; 
    		  
    		  return $http.post('http://localhost:8090/updateUE/',newUE);
          }else { // si ajout d'une nouvelle ue
            // TODO ajouter un formation à la liste
    	  
    	  console.log("TODO : code formation ue", ue.uniteEnseignementPK.codeFormation);
          console.log("TODO : code enseignant ue",ue.noEnseignant.noEnseignant);
          
          var noEns = 'http://localhost:8090/getens/'+ue.noEnseignant.noEnseignant;
		  var formation = 'http://localhost:8090/formation/'+ue.uniteEnseignementPK.codeFormation;
		  
		  console.log("Enseignant : "+ noEns);
		  console.log("Formation : "+ formation);
          	
          var newUE = {
        		  
        		  "description":ue.description,
        		  "designation":ue.designation,
        		  "formation":formation,
        		  "nbhCm":ue.nbhCm,
        		  "nbhTd":ue.nbhTd,
        		  "nbhTp":ue.nbhTp,
        		  "noEnseignant":noEns,
        		  "semestre":ue.semestre,
        		  "uniteEnseignementPK":{"codeFormation":ue.uniteEnseignementPK.codeFormation,"codeUe":ue.uniteEnseignementPK.codeUe}
     			  
     		  }; 
          
          
          	return $http.post('http://localhost:8090/ajouterUE',newUE);
          }
        },
        
        
        setEc: function(Ec,isModifec,isAjoutec) {
        	
        	var codeFormation = Ec.elementConstitutifPK.codeFormation;
            var codeUe = Ec.elementConstitutifPK.codeUe;
            var codeEc = Ec.elementConstitutifPK.codeEc;
            // si modification d'une formation existante
            if(codeFormation != null && codeUe != null && codeEc != null && isModifec == true){
            	
            	var ens = 'http://localhost:8090/getens/'+Ec.noEnseignant.noEnseignant;	  
            	var newEc = {
          			           "description":Ec.description,
          			           "designation":Ec.designation,
      	           			   "elementConstitutifPK":{"codeFormation":Ec.elementConstitutifPK.codeFormation, "codeUe":Ec.elementConstitutifPK.codeUe, "codeEc":Ec.elementConstitutifPK.codeEc},
      	           			   "nbhCm":Ec.nbhCm,
      	           			   "nbhTd":Ec.nbhTd,
      	           			   "nbhTp":Ec.nbhTp,
      	           			   "noEnseignant":ens
      	           		  };   
          	  
          	  		console.log(newEc);
      	          	return $http.post('http://localhost:8090/updateEC/',newEc);
            	
            }else { // si ajout d'un nouvel ec
        	
        	 console.log("TODO : code formation ue", Ec.elementConstitutifPK.codeFormation);
             console.log("TODO : code formation ue", Ec.elementConstitutifPK.codeUe);
             console.log("TODO : code formation ue", Ec.elementConstitutifPK.codeEc);
             console.log("TODO : code enseignant ue",Ec.noEnseignant.noEnseignant);
            
              var noEns = 'http://localhost:8090/getens/'+Ec.noEnseignant.noEnseignant;
	  		  
	  		  console.log("Enseignant : "+ noEns);
	            	
	            var newEC = {
	            			 "description":Ec.description,
	            			 "designation":Ec.designation,
	            			 "elementConstitutifPK":{"codeFormation": Ec.elementConstitutifPK.codeFormation,"codeUe":Ec.elementConstitutifPK.codeUe,"codeEc":Ec.elementConstitutifPK.codeEc},
	            			 "nbhCm":Ec.nbhCm,
	            			 "nbhTd":Ec.nbhTd,
	            			 "nbhTp":Ec.nbhTp,
	            			 "noEnseignant":noEns
	            	     	};
            
            	return $http.post('http://localhost:8090/ajouterEC',newEC);
            }
          },
      
      delete: function(formation) { 
        console.log("TODO : supprimer formation", formation.codeFormation);
        return $http.get('http://localhost:8090/formation/delete/'+formation.codeFormation);
      }
    };
  }]);
  
  app.controller('FormationsController', 
		  ['$scope', '$location', 'formationsFactory','$filter', '$http', 'ecService','ueService','rvDomainService',
		    function($scope, $location, formationsFactory, $filter, $http, ecService, ueService,rvDomainService){
			  $scope.ouiNon= {};
			
	           //pagination formation
			  	var init;
				  formationsFactory.all()
					.success(function(data) {
					    $scope.formations = data;
				
					    $scope.searchKeywords = '';
					      $scope.filteredFormation = [];
					      $scope.row = '';
					      $scope.select = function(page) {
					        var end, start;
					        start = (page - 1) * $scope.numPerPage;
					        end = start + $scope.numPerPage;
					        return $scope.currentPageFormation = $scope.filteredFormation.slice(start, end);
					      };
					      $scope.onFilterChange = function() {
					        $scope.select(1);
					        $scope.currentPage = 1;
					        return $scope.row = '';
					      };
					      $scope.onNumPerPageChange = function() {
					        $scope.select(1);
					        return $scope.currentPage = 1;
					      };
					      $scope.onOrderChange = function() {
					        $scope.select(1);
					        return $scope.currentPage = 1;
					      };
					      $scope.search = function() {
					        $scope.filteredFormation = $filter('filter')($scope.formations, $scope.searchKeywords);
					        return $scope.onFilterChange();
					      };
					      $scope.order = function(rowName) {
					        if ($scope.row === rowName) {
					          return;
					        }
					        $scope.row = rowName;
					        $scope.filteredFormation = $filter('orderBy')($scope.formations, rowName);
					        return $scope.onOrderChange();
					      };
					      $scope.numPerPageOpt = [3, 5, 10, 20];
					      $scope.numPerPage = $scope.numPerPageOpt[2];
					      $scope.currentPage = 1;
					      $scope.currentPageFormation = [];
					      init = function() {
					        $scope.search();
					        return $scope.select($scope.currentPage);
					      };
					      return init();
					  }
					)
					.error(function(data) {
						 $scope.error = 'unable to get the poneys';
					  }
					);  
		 

		    	// la liste globale des formations
		    	var promise = formationsFactory.all();
		    	promise.success(function(data) {
		    		  $scope.formations = data;
		    		  
		    		  // Pour changer le code du double diplome
		    		  
		    		  $scope.ouiNon = rvDomainService.ouinon;
		    		  
		    		  for(var i=0; i<$scope.formations.length; i++){
			        		for(var j=0; j<$scope.ouiNon.length; j++){
			        			if($scope.formations[i].doubleDiplome == $scope.ouiNon[j].rvAbbreviation){
			        				
			        				$scope.formations[i].doubleDiplome = $scope.ouiNon[j].rvMeaning;
			        				
			        			}
			        		}
			        	}
		    		  
		    		  //////////////////////////////////////
                      $scope.formations.debutAccreditation = $filter('date')(data.debutAccreditation, "dd-MM-yyyy");
	    			  $scope.formations.finAccreditation = $filter('date')(data.finAccreditation, "dd-MM-yyyy");
	    			  
		  			 
//////////////////////////////////////////////////////////////////////////////////////////		  			    
	    			  $scope.searchKeywords = '';
				      $scope.filteredFormation = [];
				      $scope.row = '';
				      $scope.select = function(page) {
				        var end, start;
				        start = (page - 1) * $scope.numPerPage;
				        end = start + $scope.numPerPage;
				        return $scope.currentPageFormation = $scope.filteredFormation.slice(start, end);
				      };
				      $scope.onFilterChange = function() {
				        $scope.select(1);
				        $scope.currentPage = 1;
				        return $scope.row = '';
				      };
				      $scope.onNumPerPageChange = function() {
				        $scope.select(1);
				        return $scope.currentPage = 1;
				      };
				      $scope.onOrderChange = function() {
				        $scope.select(1);
				        return $scope.currentPage = 1;
				      };
				      $scope.search = function() {
				        $scope.filteredFormation = $filter('filter')($scope.formations, $scope.searchKeywords);
				        return $scope.onFilterChange();
				      };
				      $scope.order = function(rowName) {
				        if ($scope.row === rowName) {
				          return;
				        }
				        $scope.row = rowName;
				        $scope.filteredFormation = $filter('orderBy')($scope.formations, rowName);
				        return $scope.onOrderChange();
				      };
				      $scope.numPerPageOpt = [2, 3, 4, 5];
				      $scope.numPerPage = $scope.numPerPageOpt[2];
				      $scope.currentPage = 1;
				      $scope.currentPageFormation = [];
				      init = function() {
				        $scope.search();
				        return $scope.select($scope.currentPage);
				      };
				      return init();	  
				          			    	
		 
		    	       }
		    		)
    		.error(function(data) {
    			 $scope.error = 'unable to get the poneys';
    		  }
    		);

		    	var promise = formationsFactory.getUe();
		    	promise.success(function(data) {
		    		    $scope.ue = data;
		    	});
		    	promise.error(function(data) {
		    			 $scope.error = 'unable to get the poneys';
		    	});
		    	
		    	//affiche les détails d'un ue
		    	$scope.editue = function(ue){
		        ueService.ue = ue;
		          $location.path("/admin/ue/"+ ue.uniteEnseignementPK.codeFormation +"/"+ue.uniteEnseignementPK.codeUe);
		        }
		    	
		    	// supression UE
		    	$scope.supprimeue = function(ue){
		    		
////////////////////////////////////////
		    	   	swal({
		    	         	title: "Voulez-vous supprimer cette Unité d'enseignement ?",
		    	         	type: "warning",   
		    	       	 	showCancelButton: true,   
		    	       	 	confirmButtonColor: "#eec95a",   
		    	       	 	confirmButtonText: "Oui!",  
		    	       	 	cancelButtonText: "Non!",   
		    	       	 	closeOnConfirm: false,   closeOnCancel: false },
		    	       	 function(isConfirm){
		    	       	 	if (isConfirm) {       
		    	       	 	var promise= formationsFactory.deleteUe(ue);
		    	       	           promise.success(function(data,statut){
		    	       	          //$scope.currentPageRubrique.removeValue("idRubrique",rubrique.idRubrique);
		    	       	          swal("Supprimé!", "Unité d'enseignement supprimée", "success");
		    	       	         // $scope.refresh();
		    	       	          var index = $scope.currentPageUnit.indexOf(ue);
		    	       	          $scope.currentPageUnit.splice(index,1);
		    	       	           });
		    	       	          promise.error(function(data,statut, headers, config){
		    	       	          swal("Erreur!", "Impossible de supprimer l'Unité d'enseignement choisie", "error");
		    	             });
		    	       	 	}else{
		    	       	 	swal("Ignorer", "", "error");
		    	       	 	}
		    	       	 	});
		    	   	////////////////////////////////////////
		    	}
		    	
		    // affiche les détails d'un EC
	           $scope.editec = function(ec){
	    		ecService.ec = ec;
	              $location.path("/admin/ec/"+ec.elementConstitutifPK.codeFormation+"/"+ec.elementConstitutifPK.codeUe+"/"+ec.elementConstitutifPK.codeEc);
	            }
	           
	           
	           
	           $scope.modif = function(ec){
		    		  ecService.ec = ec;
		              $location.path("/admin/ec/"+ec.elementConstitutifPK.codeFormation+"/"+ec.elementConstitutifPK.codeUe+"/modifEC");
		            }
	           
	          
	
		   $scope.UeFormation=function (codeFormation){
			   
			   ueService.codeFormation = codeFormation;
		    		
		    		var promise2= formationsFactory.getUe(codeFormation);
		            promise2.success(function(data,statut){
		          	  $scope.UebyFormation = data ;
		          	if($scope.UebyFormation == '') {$scope.IsHiddenUe = true;};
		          	
		          	$scope.filteredStores = [];
				      $scope.row = '';
				      $scope.selectUE = function(page) {
				        var end, start;
				        start = (page - 1) * $scope.numPerPageUE;
				        end = start + $scope.numPerPageUE;
				        return $scope.currentPageUnit = $scope.filteredStores.slice(start, end);
				      };
				      $scope.onFilterChangeUE = function() {
				        $scope.selectUE(1);
				        $scope.currentPageUE = 1;
				        return $scope.row = '';
				      };
				      $scope.onNumPerPageChangeUE = function() {
				        $scope.selectUE(1);
				        return $scope.currentPageUE = 1;
				      };
				      $scope.onOrderChange = function() {
				        $scope.selectUE(1);
				        return $scope.currentPageUE = 1;
				      };
				      $scope.search = function() {
					        $scope.filteredStores = $filter('filter')($scope.UebyFormation);
					        return $scope.onFilterChangeUE();
					      };
				      $scope.order = function(rowName) {
				        if ($scope.row === rowName) {
				          return;
				        }
				        $scope.row = rowName;
				        $scope.filteredStores = $filter('orderBy')($scope.UebyFormation, rowName);
				        return $scope.onOrderChangeUE();
				      };
				      $scope.numPerPageOptUE = [2, 3, 4, 5];
				      $scope.numPerPageUE = $scope.numPerPageOptUE[2];
				      $scope.currentPageUE = 1;
				      $scope.currentPageUnit = [];
				      init = function() {
				        $scope.search();
				        return $scope.selectUE($scope.currentPageUE);
				      };
				      return init(); 
		          	 
		            })
		            .error(function(data,statut){
		          	  console.log("impossible de recuperer les details de l'Ue");
		            });
		    		
		    	}
		    	
		    	
		         $scope.EcUe=function (ue){
		        	 ueService.codeFrmUe = ue.uniteEnseignementPK.codeFormation;
		        	 ueService.codeUe = ue.uniteEnseignementPK.codeUe;

		    		
		    		var promise3= formationsFactory.getEc(ue.uniteEnseignementPK.codeUe);


		            promise3.success(function(data,statut){
		          	  $scope.EcbyUe = data;
		          	  
		          	$scope.filteredEC = [];
				      $scope.row = '';
				      $scope.selectEC = function(page) {
				        var end, start;
				        start = (page - 1) * $scope.numPerPageEC;
				        end = start + $scope.numPerPageEC;
				        return $scope.currentPageElem = $scope.filteredEC.slice(start, end);
				      };
				      $scope.onFilterChangeEC = function() {
				        $scope.selectEC(1);
				        $scope.currentPageEC = 1;
				        return $scope.row = '';
				      };
				      $scope.onNumPerPageChangeEC = function() {
				        $scope.selectEC(1);
				        return $scope.currentPageEC = 1;
				      };
				      $scope.onOrderChange = function() {
				        $scope.selectUE(1);
				        return $scope.currentPageEC = 1;
				      };
				      $scope.search = function() {
					        $scope.filteredEC = $filter('filter')($scope.EcbyUe);
					        return $scope.onFilterChangeEC();
					      };
				      $scope.order = function(rowName) {
				        if ($scope.row === rowName) {
				          return;
				        }
				        $scope.row = rowName;
				        $scope.filteredEC = $filter('orderBy')($scope.EcbyUe, rowName);
				        return $scope.onOrderChangeEC();
				      };
				      $scope.numPerPageOptEC = [1, 2, 3, 4];
				      $scope.numPerPageEC = $scope.numPerPageOptEC[2];
				      $scope.currentPageEC = 1;
				      $scope.currentPageElem = [];
				      init = function() {
				        $scope.search();
				        return $scope.selectEC($scope.currentPageEC);
				      };
				      return init(); 
		          	 
		            })
		            .error(function(data,statut){
		          	  console.log("impossible de recuperer les details de l'Ec ");
		            });
		    		
		    	   }
//		         $scope.active = function () {
//		        	 
//		        	  
//		        	  
//		        	  $scope.myStyle = {
//		        		        "color" : "white",
//		        		        "background-color" : "coral",
//		        		       
//		        		    }
//		        	
////		        	  this.f=
//		        	  
//		         }
		         
		         $scope.selectedRow = null;  // initialize our variable to null
		         $scope.setClickedRow = function(index){  //function that sets the value of selectedRow to current index
		         $scope.selectedRow = index;
		         }
		         
		         $scope.selectedRow1 = null;  // initialize our variable to null
		         $scope.setClickedRow1 = function(index){  //function that sets the value of selectedRow to current index
		         $scope.selectedRow1 = index;
		         }
		        
		         
		    	//fermer la section Ue par defaut
		        $scope.IsHidden = true;
		        $scope.ShowHide = function () {
		            //If DIV is hidden it will be visible and vice versa.
		            //$scope.IsHidden = $scope.IsHidden ? false : true;
		        	
		            $scope.IsHidden =false;
		        }
		    	
		      //fermer la section Ec par defaut
		        $scope.IsHiddenUe = true;
		        $scope.ShowHideUe = function () {
		            //If DIV is hidden it will be visible and vice versa.
		            //$scope.IsHiddenUe = $scope.IsHiddenUe ? false : true;
		            $scope.IsHiddenUe=false;
		        }
		    	
      // Crée la page permettant d'ajouter une formation
      $scope.ajoutFormation = function(){
        $location.path("/admin/formation/nouveau"); 
      }
      
      // Crée la page permettant d'ajouter une ue
      $scope.ajoutUE = function(){
        $location.path("/admin/formation/ajoutue/nouveauUE"); 
      }
      
      $scope.ajoutEC = function(){
          $location.path("/admin/formation/ajoutec/nouveauEC"); 
        }

      // affiche les détails d'une formation
      $scope.edit = function(formation){
        $location.path("/admin/formation/"+ formation.codeFormation);
      }

      // supprime une formation
      $scope.supprime = function(formation){
    	  
///////////////////////////////////////////
    	   	 swal({
    	       	title: "Voulez-vous supprimer cette formation?",
    	       	type: "warning",   
    	     	 	showCancelButton: true,   
    	     	 	confirmButtonColor: "#eec95a",   
    	     	 	confirmButtonText: "Oui!",  
    	     	 	cancelButtonText: "Non!",  
    	     	 	closeOnConfirm: false,   closeOnCancel: false },
    	     	 function(isConfirm){
    	     	 	if (isConfirm) {       
    	     	 	var promiseDelete = formationsFactory.delete(formation);
    	     	  promiseDelete.success(function(data,statut){
    	     	          //$scope.currentPageRubrique.removeValue("idRubrique",rubrique.idRubrique);
    	     	          swal("Supprimé!", "Formation supprimé", "success");
    	     	         // $scope.refresh();
    	     	          var index = $scope.currentPageFormation.indexOf(formation);
    	     	          $scope.$scope.currentPageFormation.splice(index,1);
    	     	           });
    	       promiseDelete.error(function(data,statut, headers, config){
    	     	          swal("Erreur!", "Impossible de supprimer la formation choisie", "error");
    	           });
    	     	 	}else{
    	     	 	swal("Ignorer", "", "error");
    	     	 	}
    	     	 	});
    	   	 //////////////////////////////////////////
      }
      
    //edition dans une page
      $scope.editionUE = function(ue){
    	  ueService.ue = ue;
      	  $location.path("/admin/ue/modif");
      }
      
   // supprime un Ec
      $scope.supprimeec = function(Ec){ 
    	  
         	swal({
             	title: "Voulez-vous supprimer cet Elément constitutif ?",
             	type: "warning",   
           	 	showCancelButton: true,   
           	 	confirmButtonColor: "#eec95a",   
           	 	confirmButtonText: "Oui!",  
           	 	cancelButtonText: "Non!",   
           	 	closeOnConfirm: false,   closeOnCancel: false },
           	 function(isConfirm){
           	 	if (isConfirm) {       
           	 	var promise = formationsFactory.deleteEc(Ec);
           	           promise.success(function(data,statut){
           	          //$scope.currentPageRubrique.removeValue("idRubrique",rubrique.idRubrique);
           	          swal("Supprimé!", "Elément constitutif supprimé", "success");
           	         // $scope.refresh();
           	          var index = $scope.currentPageElem.indexOf(Ec);
           	          $scope.currentPageElem.splice(index,1);
           	           });
           	          promise.error(function(data,statut, headers, config){
           	          swal("Erreur!", "Impossible de supprimer l'élément constitutif choisi", "error");
                 });
           	 	}else{
           	 	swal("Ignorer", "", "error");
           	 	}
           	 	});
       	 ////////////////////////////////
        
      }
      
   }]);

  app.controller('FormationDetailsController', 
		  ['$routeParams', '$location', '$filter','formationsFactory', 'ecService','ueService','rvDomainService',
		    function($routeParams, $location, $filter, formationsFactory, ecService, ueService, rvDomainService){
			  var ctrl = this;
			  
			  ctrl.editue= false; 
			  ctrl.editec= false;
			  ctrl.edit= false;
			  ctrl.ec = ecService.ec;
			  ctrl.ue = ueService.ue;
			  ctrl.isModifue = false;
			  ctrl.isAjoutue = false;
			  ctrl.isModifec = false;
			  ctrl.isAjoutec = false;
		      
		      
			  ctrl.ouiNon={};
		      // si creation d'une nouvelle formation
		      if($routeParams.id == "nouveau"){
		    	  ctrl.formation= { };
		    	  ctrl.diplome= rvDomainService.diplome;
		    	  ctrl.doubleDiplome= rvDomainService.ouinon;
		        console.log("Avant l'ajout : "+ctrl.doubleDiplome[0].rvAbbreviation);
		        ctrl.edit= true;    
		      } else{ // sinon on edite une formation existante
		        // clone de l'objet pour conserver l'objet initial
		        var promise = formationsFactory.get($routeParams.id);
		        console.log($routeParams.id);
		        promise.success(function(data,status) {
		        	ctrl.formation = data ;
					
//////////////////////Domaines //////////////////////////////////////////////////////
					if(ctrl.formation.doubleDiplome == 'O')
						{
						ctrl.formation.doubleDiplome= 'OUI';
						}
					else if(ctrl.formation.doubleDiplome == 'N')
	
						{
						ctrl.formation.doubleDiplome= 'NON';
						}
					
					if(ctrl.formation.diplome == 'L')
					{
						ctrl.formation.diplome= 'Licence';
					}
					else if(ctrl.formation.diplome == 'M')

					{
						ctrl.formation.diplome= 'Master';
					}

					else if(ctrl.formation.diplome == 'D')

					{
						ctrl.formation.diplome= 'Doctorat';
					}

				  
/////////////////////////////////////////////////////////////////////////////////////////////
		
				    
				    promise.error(function(error){
				    	console.log("Impossible de récupérer les domaines : "+ error);
				    });
				    //ctrl.pay=ctrl.cgref.rvMeaning;
				    
				    			
				   
				    ctrl.formation.debutAccreditation = $filter('date')(data.debutAccreditation, "dd-MM-yyyy");
				    ctrl.formation.finAccreditation = $filter('date')(data.finAccreditation, "dd-MM-yyyy");	
		        	console.log("Dans FormationDetailsController : success : "+ctrl.formation.nomFormation);
				  }
				)
		 .error(function(data,status){
	        	console.log("erreur de recupere la formation choisi");
	      });
		        
      }
		      
		     
		 
		      
		      if($routeParams.id == "nouveauUE"){
		    	  ctrl.isAjoutue = true;
		    	  ctrl.ue= { };
		    	  ctrl.semestre= rvDomainService.semestre;
		    	  ctrl.ue.uniteEnseignementPK = {};
		    	  ctrl.ue.uniteEnseignementPK.codeFormation = ueService.codeFormation;
			        console.log("Dans détail "+ctrl.ue.uniteEnseignementPK.codeFormation);
			        var promise = formationsFactory.allEns();
			    	promise.success(function(data) {
			    		ctrl.ens = data;
			    		    console.log("Dans détail "+ctrl.ens[0].nom);
			    	});
			    	promise.error(function(data) {
			    		ctrl.error = 'unable to get the poneys';
			    	});
			    	ctrl.editue= true;    
			  }
		      
		      if($routeParams.id == "modif") { // sinon on edite une UE existante
		    	  ctrl.isModifue = true;
		    	  ctrl.editue = true;
		    	  ctrl.ue = ueService.ue;
		    	  ctrl.semestre= rvDomainService.semestre;
		    	  var $maPromise1 = formationsFactory.allEns();
				   
		    		$maPromise1.success(function(data){
		    			ctrl.enseignants = data;
		    		   console.log(ctrl.enseignants[1].noEnseignant);
		    	   }).error(function(error){
		    		   console.log("Erreur saisit"+error);
		    	   });
		      }
		      
		      
		      if($routeParams.id == "nouveauEC"){
		    	  ctrl.isAjoutec = true;
		    	  ctrl.ec= { };
		    	  ctrl.ec.elementConstitutifPK = {};
		    	  ctrl.ec.elementConstitutifPK.codeFormation = ueService.codeFrmUe;
		    	  ctrl.ec.elementConstitutifPK.codeUe = ueService.codeUe;
			        console.log("Dans détail "+ctrl.ec.codeFormation);
			        console.log("Dans détail "+ctrl.ec.codeUe);
			        var promise = formationsFactory.allEns();
			    	promise.success(function(data) {
			    		ctrl.ens = data;
			    		    console.log("Dans détail EC "+ctrl.ens[0].nom);
			    	});
			    	promise.error(function(data) {
			    		ctrl.error = 'unable to get the poneys';
			    	});
			    	ctrl.editue= true;    
			  }
		      
		      
		      if($routeParams.codeEc == "modifEC"){
			        	
		    	  ctrl.isModifec = true;
		    	  ctrl.editec = true;
		    	  ctrl.ec = ecService.ec;
		    	  ctrl.ec.elementConstitutifPK.codeFormation = ecService.ec.elementConstitutifPK.codeFormation;
		    	  ctrl.ec.elementConstitutifPK.codeUe = ecService.ec.elementConstitutifPK.codeUe;
		    	  ctrl.ec.elementConstitutifPK.codeEc = ecService.ec.elementConstitutifPK.codeEc;
			        	
			        	var promiseens = formationsFactory.allEns();
					    promiseens.success(function(data) {
					    	ctrl.ens = data;
					   	});
					   	promiseens.error(function(data) {
					   		ctrl.error = 'unable to get the poneys';
					   	});
			        }
	  

		      ctrl.editionEC = function(){
		    	  ctrl.editec = true;
      }
      
		      ctrl.edition = function(){
		    	  ctrl.edit = true;
        }
     
      
		      ctrl.editionUE = function(){
		    	  ctrl.editue = true;
        }

      // valide le formulaire d'édition d'une formation
//      $scope.submit = function(){
//    	  $scope.convert = function(){
//    		  $scope.formation.debutAccreditation = $filter('date')(data.debutAccreditation, "dd-MM-yyyy");
//    		  $scope.formation.finAccreditation = $filter('date')(data.finAccreditation, "dd-MM-yyyy");
//    	  }
//    	  var promise = formationsFactory.set($scope.formation);
//    	  	  promise.success(function(data){
//    	  		  console.log("Formation ajoutée avec succès!!!");
//    	  		  $scope.edit = false;  
//    	          $location.path('/admin/formations');
//    	  	  });
//    	  	  
//    	  	  promise.error(function(error){
//    	  		  console.log("Ajout impossible de la formation : "+ error);
//    	  	  }); 
//        }
      
      // Nouvelle version de laila
      
		      ctrl.convert = function(){
    	  var fin = new Date(ctrl.formation.finAccreditation);
		  var debut = new Date(ctrl.formation.debutAccreditation);
		  if (fin < debut) {
			  console.log("ERREUR : début supérieur  à fin");
			 
    	  	  
		  } else  {
			  console.log("GOOD : début est inféreur à fin");
		  }
      }
      
      // valide le formulaire d'édition d'une formation
		      ctrl.submit = function(){
    	  
    	  var fin = new Date(ctrl.formation.finAccreditation);
		  var debut = new Date(ctrl.formation.debutAccreditation);
		  if (fin < debut) {
			  return false;
		  }else{
			  var promise = formationsFactory.set(ctrl.formation);
    	  	  promise.success(function(data){
    	  		  console.log("Formation ajoutée avec succès!!!");
    	  		ctrl.edit = false;  
    	          $location.path('/admin/formations');
    	  	  });
    	  	  
			  promise.error(function(error){
    	  		  console.log("Ajout impossible de la formation : "+ error);
    	  	  }); 
		  }  
		}
      ///////////////////////
      
      
		      ctrl.submitUE = function(){
    	  
    	  if(ctrl.ue.uniteEnseignementPK.codeFormation != null && 
    			  ctrl.ue.uniteEnseignementPK.codeUe != null && 
    			  ctrl.isModifue == true){
  			// formationsFactory.setUE(ctrl.ue);
      		 var promise = formationsFactory.setUE(ctrl.ue,ctrl.isModifue,ctrl.isAjoutue);
   	        promise.success(function(data) {
   	       	console.log("UE a été modifiée");
   	     ctrl.editue = false;  
   	                $location.path('/admin/formations');
   	       	});
   	       	promise.error(function(data) {
   	       	ctrl.error = 'unable to get the poneys';
   	       	});
      	 } else{
      		 
//      	  formationsFactory.setUE(ctrl.ue);
      		var promise = formationsFactory.setUE(ctrl.ue,ctrl.isModifue,ctrl.isAjoutue);
   	    	promise.success(function(data) {
   	    		 console.log("UE ajoutée!!! ");
   	    		ctrl.editue = false;  
   	             $location.path('/admin/formations');
   	    	});
   	    	promise.error(function(data) {
   	    		ctrl.error = 'unable to get the poneys';
   	    	}); 
      	 }
    	  
 
        }
      
      
		      ctrl.submitec = function(){
    	  
    	  if(ctrl.ec.elementConstitutifPK.codeFormation != null && 
    			  ctrl.ec.elementConstitutifPK.codeUe != null &&
    			  ctrl.ec.elementConstitutifPK.codeEc != null &&
    			  ctrl.isModifec == true){
    		  
    		  var promise= formationsFactory.setEc(ctrl.ec,ctrl.isModifec,ctrl.isAjoutec);
    	         
              promise.success(function(data,statut){
              	//console.log("La chaine de retour  "+data);
            	  ctrl.editec = false;
              	 $location.path('/admin/formations'); 
              })
              .error(function(data,statut){
            	  console.log("impossible d'ajouter le Ec "+statut);
              });
    		  
    	  }else{
    		  
    		  //formationsFactory.setEc(ctrl.ec);
	     	  var promise = formationsFactory.setEc(ctrl.ec,ctrl.isModifec,ctrl.isAjoutec);
	 	    	promise.success(function(data) {
	 	    		 console.log("EC ajoutée!!! ");
	 	    		ctrl.editec = false;  
	 	             $location.path('/admin/formations');
	 	    	});
	 	    	promise.error(function(data) {
	 	    		ctrl.error = 'unable to get the poneys';
	 	    	});
    	  } 	  
    }


      // annule l'édition
		      ctrl.cancel = function(){ 
		    	  ctrl.ec = {};

          $location.path('/admin/formations');
    } 

    }]
  );
})();
