(function() {
  'use strict';
  
  var app = angular.module('app.parametrage', []);
 
  app.service('parametrageService',function(){
		var rubrique ;
		var question;
		var qualificatif ;
		var rubdiv=false;
		var qualdiv=false;
		var quediv=false;
	  });
  app.factory('qualFactory', ['$http', function($http){
//	    var list = function() {
//	        return  $http.get('http://localhost:8090/qualificatifs') 
//	       };
//	       
//	       var details = [ 
//	                      
//	                    ];
    return {
//	    	   
//	    	   
//	       allQualif:list,
  	   // renvoi la liste de tous les ue
	      all: function() { // TODO retourner la liste 
	    	  return  $http.get('http://localhost:8090/getAllQualif')
	      },
	      set: function(qual) {
	          var idx = qual.idQualificatif;
	          if(idx){
	              // TODO alimenter lobjet trouvé
	          	console.log("TODO : update rubrique",idx);
	          	
	          	var newQualificatif = {
	          			  "idQualificatif": qual.idQualificatif, 
	           			  "maximal" : qual.maximal,
	           			  "minimal" : qual.minimal,
	           			  
	           		  };      	  
	          	return $http.post('http://localhost:8090/updateQualificatif',newQualificatif);
	          } else{
	    	  console.log("Dans set  "+qual.minimal);
	       	  var newQualificatif = {
	        		  //"idQualificatif" : "20",
	       			  "maximal" : qual.maximal,
	       			  "minimal" : qual.minimal,
	       			  
	       		  };  
	       	console.log("Dans le nouvel objet  "+newQualificatif.maximal);
	       	return	$http.post('http://localhost:8090/addQualif',newQualificatif);
//	       	return $http({
//	                method: 'POST',
//	                url: 'http://localhost:8090/addQualif',
//	                data: newQualificatif,
//	                headers: { 'Content-Type': 'application/json' }
//	       		});
	      }
	      },
	      delete: function(qualificatif) { 
	          // TODO Supprimer 

	    	  console.log("TODO : supprimer Qualificatif",qualificatif.idQualificatif);
	    	  return  $http.get('http://localhost:8090/deleteQualif/'+qualificatif.idQualificatif)

	    	  
//	    	      var r = confirm("Voulez vous vraiment supprimer cet élément ?");
//	    	      if (r == true) {
//			    	  return  $http.get('http://localhost:8090/deleteQualif/'+qualificatif.idQualificatif)
//	    	      } 


	      }
	      
	     };
	     
	    
}]);
  
  app.factory('parametrageFactory',['$http',function($http){
	  
	    return {
	      // renvoi la liste de tous les enseignants
	    	 all: function() { // TODO retourner la liste 
	       	  return  $http.get('http://localhost:8090/questions') //list,//;
	       	
	         },
	         	
	        getAllQualif: function() { // TODO retourner la liste 
		    	  return  $http.get('http://localhost:8090/getAllQualif')
		      }, 
	           
	     get: function(idQuestion) { 
		    	 
		    	  return  $http.get('http://localhost:8090/question/'+idQuestion);
		    	  
		   	  },
//	      set: function(question) {
//	        
//	        $http.post('http://localhost:8090/question/ajouterquestion',question);
//	      },
		   	  
		   	set: function(question) {
		        var idx = question.idQuestion;
		       
		        if(idx){
		          
//		        	var idqua = question.qualificatif.split('.'); 
//		        	var qualif = 'http://localhost:8090/getQualif/'+idqua[0];
		        	var newQuestion = {
		        			  "idQuestion": idx, 
		         			  "type" : "QUS",
		         			  "intitule" : question.intitule,
		         			  "idQualificatif" : question.qualificatif
		         			  
		         		  }; 
		        } else { 
		        	
        			var idqua = question.qualificatif.split('.'); 
		        	var qualif = 'http://localhost:8090/getQualif/'+idqua[0];
		        	
		        	  var newQuestion = {
		        			  
		         			  "type" : "QUS",
		         			  "intitule" : question.intitule,
		         			  "idQualificatif" : qualif
		         			  
		       		  };      	  
		        	  
		        }
	        	  return $http.post('http://localhost:8090/ajouterQuestion',newQuestion);

		      }, 	  
		   	  
	      
	      delete: function(question) { 
	      
	       return $http.get('http://localhost:8090/deleteQuestion/'+question.idQuestion);
	        
	      }
	    };
	  }
  
  ]);
  
  app.factory('rubriqueFactory', ['$http',function($http){
	    var list = function() {
	        //var defer = $q.defer();

	        return  $http.get('http://localhost:8090/rub')
	        /*.then(function(response) {
	           defer.resolve(response.data);
	         });

	         return defer.promise;*/
	       };
	    	/*[ 
	      // TODO Constituer la liste des enseignants ici
	      {
	    	  no_enseignant : "1", nom : "SALIOU", prenom : "Philippe", 
	    	  email :"philippe.saliou@gmail.com", sexe : "H", adresse :"6 rue de l'Argoat" ,
	    	  code_postal : "29860", ville : "LE DRENNEC", telephone : "06.29.24.01.00",
	    	  pays : "FR", type : "MCF"
	    		  
	      },
	      {
	    	  no_enseignant : "2", nom : "LALLALI", prenom : "Mounir", 
	    	  email :"mouni.lallali@gmail.com", sexe : "H", adresse :"18rue Jean Jaurès" ,
	    	  code_postal : "29200", ville : "BREST", telephone : "06.32.03.56.32",
	    	  pays : "FR", type : "MCF"
	      },
	      {
	    	  no_enseignant : "3", nom : "LEROUX", prenom : "Pierre", 
	    	  email :"pileroux@gmail.com" , sexe : "H", adresse :"65 route de Gouesnou" ,
	    	  code_postal : "29200", ville : "BREST", telephone : "06.45.95.47.29",
	    	  pays : "FR", type : "INT"
	      }
	    ];*/
	            
	    var details = [ 
	      // Constituer le délail de la liste des enseignants ici
	    ];

	    return {
	    	
	      // renvoi la liste de tous les enseignants
	      allRubrique:list,// function() { //return list; 

	     // },
	      // renvoi l'enseignant avec le no_enseignant demandé
	      get: function(idRubrique) { 
	    	  // TODO retourner les enseignants
	    	  console.log("TODO : get rubrique",idRubrique);
	    	  //return list.retourValue("no_enseignant",idx);
	    	  return  $http.get('http://localhost:8090/getrub?idRubrique='+idRubrique);
	    	  
	   	  },
	      set: function(rubrique) {
	        var idx = rubrique.idRubrique;
	        // si modification d'une rubrique existante
	        if(idx){
	          // TODO alimenter l'objet rubrique trouvée
	        	console.log("TODO : update rubrique",idx);
	        	
	        	var newRubrique = {
	        			  "idRubrique": idx, 
	         			  "type" : "RBS",
	         			  "designation" : rubrique.designation,
	         			  "ordre" : rubrique.ordre
	         			  
	         		  };      	  
	        	return $http.post('http://localhost:8090/updateRubrique',newRubrique);
	        } else { // si ajout d'une nouvelle rubrique
	          // TODO ajouter une rubrique à la liste
	        	
	        	  var newRubrique = {
	         			  "type" : "RBS",
	         			  "designation" : rubrique.designation,
	         			  "ordre" : rubrique.ordre
	         			  
	       		  };      	  
	        	  return $http.post('http://localhost:8090/ajouterRubrique',newRubrique);
	        }
	      },
	      delete: function(rubrique) { 
	        // TODO Supprimer 
	    	  console.log("TODO : supprimer rubrique",rubrique.idRubrique);
	    	  return  $http.get('http://localhost:8090/deleteRubrique?idRubrique='+rubrique.idRubrique)
	      }
	    };
	  }]);
  
  

 app.controller('QuestionController', 
		    ['$scope', '$location', 'parametrageFactory','$filter', '$http','parametrageService',
		    function($scope, $location, parametrageFactory,$filter, $http,parametrageService){
		    	var init;
		    	var promise = parametrageFactory.all();
		    	promise.success(function(data) {
		    		    $scope.questions = data;
		    		    $scope.searchKeywords = '';
		  		      $scope.filteredQuestion = [];
		  		      $scope.row = '';
		  		      $scope.select = function(page) {
		  		        var end, start;
		  		        start = (page - 1) * $scope.numPerPage;
		  		        end = start + $scope.numPerPage;
		  		        return $scope.currentPageQuestion = $scope.filteredQuestion.slice(start, end);
		  		      };
		  		      $scope.onFilterChange = function() {
		  		        $scope.select(1);
		  		        $scope.currentPage = 1;
		  		        return $scope.row = '';
		  		      };
		  		      $scope.onNumPerPageChange = function() {
		  		        $scope.select(1);
		  		        return $scope.currentPage = 1;
		  		      };
		  		      $scope.onOrderChange = function() {
		  		        $scope.select(1);
		  		        return $scope.currentPage = 1;
		  		      };
		  		      $scope.search = function() {
		  		        $scope.filteredQuestion = $filter('filter')($scope.questions, $scope.searchKeywords);
		  		        return $scope.onFilterChange();
		  		      };
		  		      $scope.order = function(rowName) {
		  		        if ($scope.row === rowName) {
		  		          return;
		  		        }
		  		        $scope.row = rowName;
		  		        $scope.filteredQuestion = $filter('orderBy')($scope.questions, rowName);
		  		        return $scope.onOrderChange();
		  		      };
		  		      $scope.numPerPageOpt = [3, 5, 10, 20];
		  		      $scope.numPerPage = $scope.numPerPageOpt[2];
		  		      $scope.currentPage = 1;
		  		      $scope.currentPageQuestion = [];
		  		      init = function() {
		  		        $scope.search();
		  		        return $scope.select($scope.currentPage);
		  		      };
		  		      return init(); 
		    		    
		    		  }
		    		)
		    		.error(function(data) {
		    			 $scope.error = 'unable to get the poneys';
		    		  }
		    		);
		    	
		    	
		         
		      // supprime une Question
		            $scope.supprime = function(question){ 
		          	// TODO Suppression d'une rubrique de la liste
		            	
		            	
		            	swal({
		            		title: "Voulez-vous supprimer cet qualificatif ?",
		            		type: "warning",   
		          		  	showCancelButton: true,   
		          		  	confirmButtonColor: "#DD6B55",   
		          		  	confirmButtonText: "Oui!",  
		          		  	cancelButtonText: "Non!",   
		          		  	closeOnConfirm: false,   closeOnCancel: false },
		          		  function(isConfirm){
		          		  	 if (isConfirm) {       	
		          	           	  var promise= parametrageFactory.delete(question);
		          	           	  promise.success(function(data,statut){
		          	           		//$scope.currentPageRubrique.removeValue("idRubrique",rubrique.idRubrique);
		          	           		swal("Supprimé!", "Question supprimée", "success");
		          	          // 		$scope.refresh();
		          	           		var index = $scope.currentPageQuestion.indexOf(question);
		          	           	$scope.currentPageQuestion.splice(index,1);
		          	           	  });
		          	           	promise.error(function(data,statut, headers, config){
		          	           	swal("Erreur!", "Impossible de supprimer la Question choisie", "error");
		                });
		          		  	 }else{
		          		  		swal("Ignorer", "", "error");
		          		  		}
		          		  	 });
//		          	  var promise= parametrageFactory.delete(question);
//		                promise.success(function(data,statut){
//		              	  $scope.currentPageQuestion.removeValue("idQuestion",question.idQuestion);
//		                	
//		                })
//		                .error(function(data,statut){
//		              	  console.log("impossible de supprimer la Question choisie");
//		                });
		          	  
		            }      
		         
		         
		    	
		    	$scope.ajoutQuestion = function(){
		            $location.path('/admin/parametrage/question/nouveau');
		         
		            };
		            
		            
		         // affiche les détail 
		            // TODO Lien vers la page permettant d'éditer une question 
		            $scope.edit = function (question){
		          	  parametrageService.question = question;
		          	  $location.path("/admin/parametrage/question/"+ question.idQuestion);
		            }
		            
		            $scope.edition = function (question){
			          	  parametrageService.question = question;
			          	  $location.path("/admin/parametrage/question/modif");
			            }
  
  
  
    }]
  );
  
 /////////////////////////////////////////////////////////////////////////////////////////////// 
 app.controller('RubriqueController', 
		    ['$scope', '$location', 'rubriqueFactory','$filter', '$http','parametrageService',
		    function($scope, $location, rubriqueFactory,$filter, $http,parametrageService){
		    	 var init;
		            var promise2=rubriqueFactory.allRubrique();
		            promise2.success(function(data) {
		    		    $scope.rubriques = data;
		    		      $scope.searchKeywords = '';
		    		      $scope.filteredRubrique = [];
		    		      $scope.row = '';
		    		      $scope.select = function(page) {
		    		        var end, start;
		    		        start = (page - 1) * $scope.numPerPage;
		    		        end = start + $scope.numPerPage;
		    		        return $scope.currentPageRubrique = $scope.filteredRubrique.slice(start, end);
		    		      };
		    		      $scope.onFilterChange = function() {
		    		        $scope.select(1);
		    		        $scope.currentPage = 1;
		    		        return $scope.row = '';
		    		      };
		    		      $scope.onNumPerPageChange = function() {
		    		        $scope.select(1);
		    		        return $scope.currentPage = 1;
		    		      };
		    		      $scope.onOrderChange = function() {
		    		        $scope.select(1);
		    		        return $scope.currentPage = 1;
		    		      };
		    		      $scope.search = function() {
		    		        $scope.filteredRubrique = $filter('filter')($scope.rubriques, $scope.searchKeywords);
		    		        return $scope.onFilterChange();
		    		      };
		    		      $scope.order = function(rowName) {
		    		        if ($scope.row === rowName) {
		    		          return;
		    		        }
		    		        $scope.row = rowName;
		    		        $scope.filteredRubrique = $filter('orderBy')($scope.rubriques, rowName);
		    		        return $scope.onOrderChange();
		    		      };
		    		      $scope.numPerPageOpt = [3, 5, 10, 20];
		    		      $scope.numPerPage = $scope.numPerPageOpt[2];
		    		      $scope.currentPage = 1;
		    		      $scope.currentPageRubrique = [];
		    		      init = function() {
		    		        $scope.search();
		    		        return $scope.select($scope.currentPage);
		    		      };
		    		      return init();
		    		  }
		    		)
		    		.error(function(data) {
		    			 $scope.error = 'unable to get the poneys';
		    		  }
		    		);
		            
		            
		         // TODO Lien vers la page permettant de créer une rubrique /admin/rubrique/nouveau
		            $scope.ajoutRubrique = function(){
		                $location.path('/admin/rubrique/nouveau'); 
		             }
		            
		            // affiche les détail 
		            // TODO Lien vers la page permettant d'éditer une rubrique /admin/rubrique/ + idRubrique
		            $scope.edit = function (rubrique){
		          	  parametrageService.rubrique = rubrique;
		          	  $location.path("/admin/rubrique/"+ rubrique.idRubrique);
		            }		            
		            
		            $scope.edition = function (rubrique){
			          	  parametrageService.rubrique = rubrique;
			          	  $location.path("/admin/rubrique/modif");
			            }
		            // supprime une rubrique
		            $scope.supprime = function(rubrique){ 
		            	swal({
		            		title: "Voulez-vous supprimer cet rubrique ?",
		            		type: "warning",   
		          		  	showCancelButton: true,   
		          		  	confirmButtonColor: "#eec95a",   
		          		  	confirmButtonText: "Oui!",  
		          		  	cancelButtonText: "Non!",   
		          		  	closeOnConfirm: false,   closeOnCancel: false },
		          		  function(isConfirm){
		          		  	 if (isConfirm) {       	
		          	           	  var promise= rubriqueFactory.delete(rubrique);
		          	           	  promise.success(function(data,statut){
		          	           		//$scope.currentPageRubrique.removeValue("idRubrique",rubrique.idRubrique);
		          	           		swal("Supprimé!", "rubrique supprimé", "success");
		          	          // 		$scope.refresh();
		          	           		var index = $scope.currentPageRubrique.indexOf(rubrique);
		          	           	$scope.currentPageRubrique.splice(index,1);
		          	           	  });
		          	           	promise.error(function(data,statut, headers, config){
		          	           	swal("Erreur!", "Impossible de supprimer la rubrique choisi", "error");
		                });
		          		  	 }else{
		          		  		swal("Ignorer", "", "error");
		          		  		}
		          		  	 });
		            }            
		    }]
 );
 
 
 app.controller('RubDetailsController', 
    ['$scope', '$routeParams', '$location', 'rubriqueFactory','parametrageService',
    function($scope, $routeParams, $location, rubriqueFactory,parametrageService){      
      $scope.edit= false;    

      // si creation d'une nouvelle rubrique
      if($routeParams.id == "nouveau"){
        $scope.rubrique= { };
        $scope.edit= true;    
      } else if($routeParams.id == "modif") { // sinon on edite une rubrique existante
    	  $scope.edit = true;
    	  $scope.rubrique = parametrageService.rubrique;
      }

      $scope.edition = function(){
        $scope.edit = true;
      }

      // valide le formulaire d'édition d'une rubrique
      $scope.submit = function(){
    	  if($scope.rubrique.idRubrique){

       	   var $maPromise1 = rubriqueFactory.set($scope.rubrique);
   		   	   $maPromise1.success(function(data){
  			  
   			   
			   $location.path('/admin/rubriques');
			   
			   
   		   }).error(function(error){
   			   console.log("Erreur  "+error);
   		   })
   		   
       	  }else{
       		  
     		   var $maPromise1 = rubriqueFactory.set($scope.rubrique);
     		   $maPromise1.success(function(data){
     			   console.log("N° rubrique  "+ data);
     			   
     			 $scope.IsHiddenQual=true;
     			 $scope.IsHiddenQue=true;
     			 $scope.IsHiddenRub=false;

     			 $location.path('/admin/rubriques');
     			   
     			  
     		   }).error(function(error){
     			   console.log("Erreur saisit"+error);
     		   })
     		     
           }
       	  
       	  $scope.edit = false;
      }

      // annule l'édition
      $scope.cancel = function(){
        // si ajout d'une nouvelle rubrique => retour à la liste des rubriques
        if(!$scope.rubrique.idRubrique){
          $location.path('/admin/rubriques');
        } else {
			 $location.path('/admin/rubriques');
        	$scope.rubrique = parametrageService.rubrique;
          $scope.edit = false;
        }
      }      
      $scope.edition = function (qualificatif){
      	  parametrageService.qualificatif = qualificatif;
      	  $location.path("/admin/qualificatif/modif");
        }
    }]
);

/////////////////////////////////////////////////////////////////////////////////////////////////////
 app.controller('QualificatifController',
		    ['$scope','$location','$filter', 'qualFactory','$http','parametrageService','$window',
		    function($scope, $location,$filter, qualFactory, $http,parametrageService,$window){

		       	var init;
		       	qualFactory.all()
				.success(function(data) {
				    $scope.qual = data;
				      $scope.searchKeywords = '';
				      $scope.filteredQualif = [];
				      $scope.row = '';
				      $scope.select = function(page) {
				        var end, start;
				        start = (page - 1) * $scope.numPerPage;
				        end = start + $scope.numPerPage;
				        return $scope.currentPageQualif = $scope.filteredQualif.slice(start, end);
				      };
				      $scope.onFilterChange = function() {
				        $scope.select(1);
				        $scope.currentPage = 1;
				        return $scope.row = '';
				      };
				      $scope.onNumPerPageChange = function() {
				        $scope.select(1);
				        return $scope.currentPage = 1;
				      };
				      $scope.onOrderChange = function() {
				        $scope.select(1);
				        return $scope.currentPage = 1;
				      };
				      $scope.search = function() {
				        $scope.filteredQualif = $filter('filter')($scope.qual, $scope.searchKeywords);
				        return $scope.onFilterChange();
				      };
				      $scope.order = function(rowName) {
				        if ($scope.row === rowName) {
				          return;
				        }
				        $scope.row = rowName;
				        $scope.filteredQualif = $filter('orderBy')($scope.qual, rowName);
				        return $scope.onOrderChange();
				      };
				      $scope.numPerPageOpt = [3, 5, 10, 20];
				      $scope.numPerPage = $scope.numPerPageOpt[2];
				      $scope.currentPage = 1;
				      $scope.currentPageQualif = [];
				      init = function() {
				        $scope.search();
				        return $scope.select($scope.currentPage);
				      };
				      return init();
				  }
				)
				.error(function(data) {
					 $scope.error = 'unable to get the poneys';
				  }
				);
		    	//Ajout
		        $scope.ajoutQualificatif = function(){
		            $location.path('/admin/qualificatif/nouveau'); 
		         }
		        //supprimer qualificatif
		        $scope.supprime = function(qual){

		        	swal({
	            		title: "Voulez-vous supprimer cet qualificatif ?",
	            		type: "warning",   
	          		  	showCancelButton: true,   
	          		  	confirmButtonColor: "#DD6B55",   
	          		  	confirmButtonText: "Oui!",  
	          		  	cancelButtonText: "Non!",   
	          		  	closeOnConfirm: false,   closeOnCancel: false },
	          		  function(isConfirm){
	          		  	 if (isConfirm) {       	
	          	           	  var promise= qualFactory.delete(qual);
	          	           	  promise.success(function(data,statut){
	          	           		//$scope.currentPageRubrique.removeValue("idRubrique",rubrique.idRubrique);
	          	           		swal("Supprimé!", "qualificatif supprimé", "success");
	          	          // 		$scope.refresh();
	          	           		var index = $scope.currentPageQualif.indexOf(qual);
	          	           	$scope.currentPageQualif.splice(index,1);
	          	           	  });
	          	           	promise.error(function(data,statut, headers, config){
	          	           	swal("Erreur!", "Impossible de supprimer le qualificatif choisi", "error");
	                });
	          		  	 }else{
	          		  		swal("Ignorer", "", "error");
	          		  		}
	          		  	 });
//		      	  var promise= qualFactory.delete(qual);
//		            promise.success(function(data,statut){
//		          	  $scope.currentPageRubrique.removeValue("idQualificatif",qual.idQualificatif);
//		            })
//		            .error(function(data,statut){
//		          	  console.log("impossible de supprimer le Qualificatif choisi");
//		            });
		        };
		        
//		        $scope.edit = function (qualificatif){
//		      	  parametrageService.qualificatif = qualificatif;
//		      	  $location.path("/admin/qualificatif/"+ qualificatif.idQualificatif);
//		        }
		        
		        $scope.edition = function (qualificatif){
			      	  parametrageService.qualificatif = qualificatif;
			      	  $location.path("/admin/qualificatif/modif");
			        }
}]);
 
 app.controller('QualDetailsController', 
		    ['$scope', '$routeParams', '$location', 'qualFactory','parametrageService',
		    function($scope, $routeParams, $location, qualFactory,parametrageService){      
		      $scope.edit= false; 
		   // si creation d'un nouveau qualificatif
		      if($routeParams.id == "nouveau"){
		        $scope.qualificatif= { };
		        $scope.edit= true;
		        //nouveu edit
		      }
		      else if($routeParams.id == "modif") { // sinon on edite une rubrique existante
		    	  $scope.edit = true;
		    	  $scope.qualificatif = parametrageService.qualificatif;
		      }
		      
//		      $scope.edition = function(){
//		          $scope.edit = true;
//		        }
		      
		      $scope.submit = function(){    	 
		    	  if($scope.qualificatif.idQualificatif){   
		    	  
		          //$location.path('/admin/qualificatifs'); 
		          var promise= qualFactory.set($scope.qualificatif);
		          console.log("Dans submit "+$scope.qualificatif.minimal);
		            promise.success(function(data,statut){
		            	console.log("La chaine de retour  "+data);
		            	$location.path('/admin/qualificatifs'); 
		            })
		            .error(function(data,statut){
		          	  console.log("impossible d'ajouter le Qualificatif "+statut);
		            });
		      }else{
	       		  
	     		   var $maPromise1 = qualFactory.set($scope.qualificatif);
	     		   	   $maPromise1.success(function(data){
	     			   console.log("N° qualificatif  "+ data);
	     			   $location.path('/admin/qualificatifs');
	     		   }).error(function(error){
	     			   console.log("Erreur saisit"+error);
	     		   })
	     		     
	           }
		            $scope.edit = false;
		        }
		      
		      $scope.cancel = function(){
		          // si ajout d'un qualificatif => retour à la liste des qualificatifs
		          //if(!$scope.qualificatif.idQualificatif){
		            $location.path('/admin/qualificatifs');
		          //} else {
		          	
		          	$scope.qualificatif = parametrageService.qualificatif;
		            $scope.edit = false;
		          //}
		        }
	
		    }]
 
 
 );
 
 
//////////////////////////////////////////////////////////////////////////////////////////////////////// 
 
 
 
 app.controller('ParametrageController', 
    ['$scope', '$location', 'parametrageFactory','$filter', '$http','parametrageService',
    function($scope, $location, parametrageFactory,$filter, $http,parametrageService){

    	
    	 $scope.IsHiddenQue = true;
         $scope.IsHiddenQual = true;
         $scope.IsHiddenRub = true;
    		
//        $scope.IsHiddenQue = parametrageService.quediv;
//        $scope.IsHiddenQual = parametrageService.qualdiv;
//        $scope.IsHiddenRub = parametrageService.rubdiv;
        
        $scope.ShowHideQue = function () {
            
//           $scope.IsHiddenQue = $scope.IsHiddenQue ? false : true;
//        	$scope.IsHiddenQue=false;
//           $scope.IsHiddenQual = true;
//           $scope.IsHiddenRub = true;
            $location.path('/admin/questions');

        	
        	
        }
        $scope.ShowHideQual = function () {
            
//            $scope.IsHiddenQual = $scope.IsHiddenQual ? false : true;
//            $scope.IsHiddenQual=false;
//            $scope.IsHiddenQue = true;
//            $scope.IsHiddenRub = true;
            $location.path('/admin/qualificatifs');

         	
         	
         }
      $scope.ShowHideRub = function () {
            
//            $scope.IsHiddenRub = $scope.IsHiddenQual ? false : true;
//    	  $scope.IsHiddenRub=false;
//            $scope.IsHiddenQual = true;
//            $scope.IsHiddenQue = true;
          $location.path('/admin/rubriques');

         	
         	
         }
      

      
    }]
  );

  app.controller('ParametrageDetailsController', 
    ['$scope', '$routeParams', '$location', 'parametrageFactory','parametrageService',
    function($scope, $routeParams, $location, parametrageFactory,parametrageService){      

//      $scope.submit = function(){
//    	  parametrageFactory.set($scope.questions);        
////          $scope.edit = false;  
//          $location.path('admin/parametrage'); 
//        }
    	 $scope.edit= false; 
    	 
    	 
    	 var $maPromiseQual = parametrageFactory.getAllQualif();
	   	   $maPromiseQual.success(function(data){
	   		   $scope.qualificatifs = data;
	   }).error(function(error){
		   console.log("Erreur  "+error);
	   })
    	 
         // si creation d'une nouvelle rubrique
         if($routeParams.id == "nouveau"){
           $scope.question= { };
           $scope.edit= true;    
         } else { // sinon on edite une rubrique existante
       	  
       	  $scope.question = parametrageService.question;
         }
	   	   
	   	 if($routeParams.id == "modif"){
	   		 $scope.edit = true;
	   		 $scope.question = parametrageService.question;
	   	 }

//         $scope.edition = function(){
//           $scope.edit = true;
//         }

         // valide le formulaire d'édition d'une question
         $scope.submit = function(){
       	  if($scope.question.idQuestion){

          	   var $maPromise1 = parametrageFactory.set($scope.question);
          	 console.log("N° question  "+ $scope.question.idQuestion);
      		   	   $maPromise1.success(function(data){
     			   //console.log("N° question  "+ $scope.question.idQuestion);
      			   $location.path('/admin/questions');
      		   }).error(function(error){
      			   console.log("Erreur  "+error);
      		   })
      		   
          	  }else{
          		  
        		   var $maPromise1 = parametrageFactory.set($scope.question);
        		   $maPromise1.success(function(data){
//        			   console.log("N° rubrique  "+ data);
        			   $location.path('/admin/questions');
        		   }).error(function(error){
        			   console.log("Erreur saisit"+error);
        		   })
        		     
              }
          	  
          	  $scope.edit = false;
         }

         // annule l'édition
         $scope.cancel = function(){
           // si ajout d'une nouvelle rubrique => retour à la liste des rubriques
           if(!$scope.question.idQuestion){
             $location.path('/admin/questions');
           } else {
               $location.path('/admin/questions');
           	$scope.rubrique = parametrageService.question;
             $scope.edit = false;
           }
         }      
    }]
  );
  app.directive('ngConfirmBoxClick', [

                                      function () {
                                          return {
                                              link: function (scope, element, attr) {
                                                  var msg = attr.ngConfirmBoxClick;
                                                  var clickAction = attr.confirmedClick;
                                                  element.bind('click', function (event) {
                                                      if (window.confirm(msg)) {
                                                          scope.$eval(clickAction)
                                                      }
                                                  });
                                              }
                                          };
                                      }
                                ]);

})();
