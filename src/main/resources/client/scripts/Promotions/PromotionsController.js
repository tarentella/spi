(function() {
  'use strict';

  var app = angular.module('app.promotions', []);
  var numShow = 0;
  var promoNewEtu;
  Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? null : v;
	   });
	   this.length = 0; //clear original array
	   this.push.apply(this, array); //push all elements except the one we want to delete
	}
  
  Array.prototype.retourValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? v : null;
	   });
	   return array[0];
	}
  
  app.service('promoService',function(){
	  var pro = {};
  })
  
  app.factory('promoFactory', ['$http',function($http){
    var list = function() {

        return  $http.get('http://localhost:8090/promotion/getAll')

       };
    	
            
    var details = [ 
      // Constituer le délail de la liste des enseignants ici
    ];

    return {
      // renvoi la liste de tous les enseignants
      all:list,// function() { //return list; 

     // },
      // renvoi l'enseignant avec le no_enseignant demandé
      getEtuPromo: function(codeF, anneeU) { 
          
      	  return  $http.get('http://localhost:8090/promotion/getEtuPro/'+codeF+'/'+anneeU);
      	  
        },
        
        allEns: function() { 
            
      	  return  $http.get('http://localhost:8090/ens/');
      	  
        },
        
      get: function(noEnseignant) { 
    	  // TODO retourner les enseignants
    	  console.log("TODO : get enseignant",noEnseignant);
    	  //return list.retourValue("no_enseignant",idx);
    	  return  $http.get('http://localhost:8090/getens/'+noEnseignant);
    	  
   	  },
      set: function(promo) {
        // si modification d'un enseignant existant
          // si ajout d'un nouvel enseignant
          // TODO ajouter un enseignant à la liste
        	
        	  var newPromotion = {
        			  
        			  
        			  "commentaire":promo.commentaire,
        			  "dateRentree":promo.dateRentree,
        			  "dateReponseLalp":promo.dateReponseLalp,
        			  "dateReponseLp":promo.dateReponseLp,
        			  "formation":"http://localhost:8090/formation/"+promo.codeFormation,
        			  "lieuRentree":promo.lieuRentree,
        			  "nbMaxEtudiant":promo.nbMaxEtudiant,
        			  "noEnseignant":"http://localhost:8090/getens/"+promo.noEnseignant,
        			  "processusStage":promo.processusStage,
        			  "promotionPK":{
        			  "codeFormation":promo.codeFormation,
        			  "anneeUniversitaire":promo.anneeUniversitaire
        			  },
        			  "siglePromotion":promo.siglePromotion
        			  
//       			  "siglePromotion" : promotion.siglePromotion,
//       			  "nbMaxEtudiant" : promotion.nbMaxEtudiant,
//       			  "promotionPK" : {
//       				  "codeFormation" : promotion.codeFormation,
//       				  "anneeUniversitaire" : promotion.anneeUniversitaire
//       			  }
       		  }; 
        	  
        	  console.log("Avant d'ajouter promo : "+newPromotion);
        	  
	        	$http.post('http://localhost:8090/promotion/ajouterPromotion/',newPromotion);
        	
        	//return list.push(enseignant);
        
      },
      
      delete: function(promotion) { 
        // TODO Supprimer 
    	  
    	  return $http.get('http://localhost:8090/promotion/deletePromotion/'+promotion.promotionPK.codeFormation+'/'+promotion.promotionPK.anneeUniversitaire);
      },
      getFormations : function(){
    	  return $http.get('http://localhost:8090/formations');
      }
    };
  }]);

  app.service('etudService',function(){
	  var etud = {};
	  
//	  return{
//		  set:function(form, annee){
//			  codeF = form;
//			  anneeU = annee;
//			  return ""+codeF+""+anneeU;
//		  
//	  }
//	  };
  })
  
  app.factory('etudiantsFactory', ['$http',function($http){
    var list = function() {

        return  $http.get('http://localhost:8090/etudiants')
       };
       
    
               
    var details = [ 
      // Constituer le délail de la liste des etudiants ici
    ];


    return {
      // renvoi la liste de tous les etudiants
      all:list,// function() { //return list; 

     // },
      // renvoi l'etudiant avec le no_etudiant demandé
      get: function(noEtudiant) { 
    	  // TODO retourner les etudiants
    	  console.log("TODO : get etudiant",noEtudiant);
    	  //return list.retourValue("no_etudiant",idx);
    	  return  $http.get('http://localhost:8090/getEtud/'+noEtudiant);
    	  
   	  },
	   	getEtudiantsPromo: function() { 
	  	  // TODO retourner les etudiants
	  	  return  $http.get('http://localhost:8090/promotion/getEtuPro/M2DOSI/2014-2015');
	  	  
	 	  },
      set: function(etudiant) {
        var idx = etudiant.noEtudiant;
        // si modification d'un etudiant existant
        if(idx){
          // TODO alimenter l'objet etudiant trouvé
        	console.log("TODO : update etudiant",idx);
        	//list.removeValue("no_etudiant",etudiant.no_etudiant);
        	//return list.push(etudiant);
        	
        	var newEtudiant ={       			
        			
        		  "adresse":etudiant.adresse,
        		  "codePostal":etudiant.codePostal,
          		  "dateNaissance":etudiant.dateNaissance,
          		  "email":etudiant.email,
          		 "emailUbo":etudiant.emailUbo,
          		 "groupeAnglais":etudiant.groupeAnglais,
          		 "groupeTp":etudiant.groupeTp,
          		  "lieuNaissance":etudiant.lieuNaissance,
          		 "mobile":etudiant.mobile,
          		  "nationalite":etudiant.nationalite,
          		  "noEtudiant":idx,
          		  "nom":etudiant.nom,
          		  "paysOrigine":etudiant.paysOrigine,
          		  "prenom":etudiant.prenom,
          		  "promotion":"http://localhost:8090/promotion/getPromo/M2DOSI_2014-2015",
          		  "sexe":etudiant.sexe,
          		  "universiteOrigine":etudiant.universiteOrigine,
          		  "ville":etudiant.ville
   			
        			
         		  };      	  
  	        	$http.post('http://localhost:8090/updateEtudiant',newEtudiant);
        } else { // si ajout d'un nouvel etudiant
          // TODO ajouter un etudiant à la liste
        	  var promoEtu = "http://localhost:8090/promotion/getPromo/"+promoNewEtu.promotionPK.codeFormation+"_"+promoNewEtu.promotionPK.anneeUniversitaire;


        	  var newetudiant =    	  {
        			  "adresse":etudiant.adresse,
            		  "codePostal":etudiant.codePostal,
              		  "dateNaissance":etudiant.dateNaissance,
              		  "email":etudiant.email,
              		 "emailUbo":etudiant.emailUbo,
              		 "groupeAnglais":etudiant.groupeAnglais,
              		 "groupeTp":etudiant.groupeTp,
              		  "lieuNaissance":etudiant.lieuNaissance,
              		 "mobile":etudiant.mobile,
              		  "nationalite":etudiant.nationalite,
              		  "noEtudiant":etudiant.num,
              		  "nom":etudiant.nom,
              		  "paysOrigine":etudiant.paysOrigine,
              		  "prenom":etudiant.prenom,
              		  "promotion":promoEtu,
        		  
        		  "sexe":etudiant.sexe,
        		  "universiteOrigine":etudiant.universiteOrigine,
        		  "ville":etudiant.ville
        		  };    
//        	  console.log("aaaaaaaa "+ newetudiant.promotion.promotionPK.anneeUniversitaire);
	        	return $http.post('http://localhost:8090/ajouterEtudiant',newetudiant);
	        	


        }
      },
      delete: function(noEtudiant) { 
        // TODO Supprimer 
    	  console.log("TODO : supprimer etudiant",noEtudiant);
    	  return  $http.get('http://localhost:8090/deleteEtudiant/'+noEtudiant)
      }

//      getPromotion : function(noEtudiant){
//    	  var url = "http://localhost:8090/getpromotionetudiant/"+noEtudiant;
//    	  return $http.get(url);
            
    };
  }]);

  

  app.controller('EtudiantsController', 
    ['$scope', '$filter','$location', 'etudiantsFactory','etudService',
    function($scope, $filter, $location, etudiantsFactory, etudService){
    	//pagination etudiant
    	var init;
//    	var $state = $injector.get('$state');
//    	$state.reload();

//    	var promise55 = etudiantsFactory.getEtudiantsPromo();
//    	promise55
//		.success(function(data) {
//		    $scope.etudiants = data;
//		      $scope.searchKeywords = '';
//		      $scope.filteredEtudiant = [];
//		      $scope.row = '';
//		      $scope.select = function(page) {
//		        var end, start;
//		        start = (page - 1) * $scope.numPerPage;
//		        end = start + $scope.numPerPage;
//		        return $scope.currentPageEtudiant = $scope.filteredEtudiant.slice(start, end);
//		      };
//		      $scope.onFilterChange = function() {
//		        $scope.select(1);
//		        $scope.currentPage = 1;
//		        return $scope.row = '';
//		      };
//		      $scope.onNumPerPageChange = function() {
//		        $scope.select(1);
//		        return $scope.currentPage = 1;
//		      };
//		      $scope.onOrderChange = function() {
//		        $scope.select(1);
//		        return $scope.currentPage = 1;
//		      };
//		      $scope.search = function() {
//		        $scope.filteredEtudiant = $filter('filter')($scope.etudiants, $scope.searchKeywords);
//		        return $scope.onFilterChange();
//		      };
//		      $scope.orderEt = function(rowName) {
//		        if ($scope.row === rowName) {
//		          return;
//		        }
//		        $scope.row = rowName;
//		        $scope.filteredEtudiant = $filter('orderBy')($scope.etudiants, rowName);
//		        return $scope.onOrderChange();
//		      };
//		      $scope.numPerPageOpt = [3, 5, 10, 20];
//		      $scope.numPerPage = $scope.numPerPageOpt[2];
//		      $scope.currentPage = 1;
//		      $scope.currentPageEtudiant = [];
//		      init = function() {
//		        $scope.search();
//		        return $scope.select($scope.currentPage);
//		      };
//		      return init();
//		  }
//		)
//		.error(function(data) {
//			 $scope.error = 'unable to get the poneys';
//		  }
//		);
      
      // Crée la page permettant d'ajouter un etudiant
      // TODO Lien vers la page permettant de créer un etudiant /admin/etudiant/nouveau
//      $scope.ajoutEtudiant = function(){
//          console.log("adil");
//
////          $location.path('/admin/etudiant/nouveau'); 
//          
//       }
      
      // affiche les détail d'un etudiant
      // TODO Lien vers la page permettant d'éditer un etudiant /admin/etudiant/ + no_etudiant
//      $scope.edite = function (etud){
//    	  etudService.etud = etud;
//    	  $location.path("/admin/etudiant/"+ etud.noEtudiant);
//    	  //alert(etudiant.no_etudiant);
//      }


     
    }]
  );
  
  app.controller('EtudDetailsController', 

		    ['$scope', '$routeParams', '$location', 'etudiantsFactory','etudService','rvDomainService',
		    function($scope, $routeParams, $location, etudiantsFactory, etudService,rvDomainService){      
		      $scope.edit= false;    

		      // si creation d'un nouvel etudiant
		      if($routeParams.id == "nouveau"){
		        $scope.etudiant= { };

		        $scope.univ= rvDomainService.universite;
		        $scope.pays= rvDomainService.pays;
		        $scope.edit= true;    
		      }else
		    	  {
		    	  $scope.etudiant = etudService.etud;
		    	  }
		     
		      if($routeParams.id == "modif"){
		    	  $scope.edit = true;
		    	  $scope.etudiant = etudService.etud;
		    	  $scope.pays= rvDomainService.pays;
		    	  $scope.univ= rvDomainService.universite;
		    	  $scope.dateN=etudService.etud.dateNaissance;
		    	  console.log($scope.dateN);
		    	
		    	  console.log(etudService.etud.paysOrigine);
		      }


		      $scope.edition = function(){
		        $scope.edit = true;
		      }

		      
		      

		      // valide le formulaire d'édition d'un etudiant
		      $scope.submit = function(){    	 
		        etudiantsFactory.set($scope.etudiant);        

		        $scope.edit = false; 
		        $location.path('/admin/promotions');
		      }

		      // annule l'édition
		      $scope.cancel = function(){
		        // si ajout d'un nouvel etudiant => retour à la liste des etudiants

		    	  $location.path('/admin/promotions');
//		        if(!$scope.etudiant.noEtudiant){
//		          $location.path('/admin/etudiants');
//		        } else {
//		          var e = etudiantsFactory.get($routeParams.id);
//		          $scope.etudiant = JSON.parse(JSON.stringify(e));
//		          $scope.edit = false;
//		        }
		      }      
		    }]
		  );

  app.controller('PromotionsController', 

    ['$scope', '$filter','$location', 'promoFactory','promoService','$http','$controller','etudiantsFactory','etudService', 'rvDomainService',
    function($scope, $filter, $location, promoFactory, promoService,$http, $controller,etudiantsFactory,etudService, rvDomainService){
    	
    	$scope.pays = {};
    	$scope.universite = {};

    	//Selection
    	 $scope.selectedRow = null;  // initialize our variable to null
         $scope.setClickedRow = function(index){  //function that sets the value of selectedRow to current index
         $scope.selectedRow = index;
         }
    	
    	//pagination enseignant
    	var init;
    	var promise11=promoFactory.all();
    	promise11
		.success(function(data) {
		    $scope.promotions = data;
		    $scope.nbreEtudiant = 10;
		      $scope.searchKeywords = '';
		      $scope.filteredPromotions = [];
		      $scope.row = '';
		      $scope.select = function(page) {
		        var end, start;
		        start = (page - 1) * $scope.numPerPage;
		        end = start + $scope.numPerPage;
		        return $scope.currentPagePromotions = $scope.filteredPromotions.slice(start, end);
		      };
		      $scope.onFilterChange = function() {
		        $scope.select(1);
		        $scope.currentPage = 1;
		        return $scope.row = '';
		      };
		      $scope.onNumPerPageChange = function() {
		        $scope.select(1);
		        return $scope.currentPage = 1;
		      };
		      $scope.onOrderChange = function() {
		        $scope.select(1);
		        return $scope.currentPage = 1;
		      };
		      $scope.search = function() {
		        $scope.filteredPromotions = $filter('filter')($scope.promotions, $scope.searchKeywords);
		        return $scope.onFilterChange();
		      };
		      $scope.order = function(rowName) {
		        if ($scope.row === rowName) {
		          return;
		        }
		        $scope.row = rowName;
		        $scope.filteredPromotions = $filter('orderBy')($scope.promotions, rowName);
		        return $scope.onOrderChange();
		      };
		      $scope.numPerPageOpt = [1, 2, 4, 10];
		      $scope.numPerPage = $scope.numPerPageOpt[2];
		      $scope.currentPage = 1;
		      $scope.currentPagePromotions = [];
		      init = function() {
		        $scope.search();
		        return $scope.select($scope.currentPage);
		      };
		      return init();
		  }
		)
		.error(function(data) {
			 $scope.error = 'unable to get the poneys';
		  }
		);

  
    
    	//Suppression
    	$scope.supprime = function(promotion){
      	  
            
      	swal({
    		title: "Voulez-vous supprimer cette promotion ?",
    		type: "warning",   
  		  	showCancelButton: true,   
  		  	confirmButtonColor: "#DD6B55",   
  		  	confirmButtonText: "Oui!",  
  		  	cancelButtonText: "Non!",   
  		  	closeOnConfirm: false,   closeOnCancel: false },
  		  function(isConfirm){
  		  	 if (isConfirm) {       	
  	           	  var promise= promoFactory.delete(promotion);
  	           	  promise.success(function(data,statut){
  	           		//$scope.currentPageRubrique.removeValue("idRubrique",rubrique.idRubrique);
  	           		swal("Supprimé!", "Promotion supprimée", "success");
  	          // 		$scope.refresh();
  	          	  $scope.currentPagePromotions.removeValue("promotionPK",promotion.promotionPK);

  	           	  });
  	           	promise.error(function(data,statut, headers, config){
  	           	swal("Erreur!", "Impossible de supprimer la promotion choisie", "error");
        });
  		  	 }else{
  		  		swal("Ignorer", "", "error");
  		  		}
  		  	 });
      	  
        }
    	

    	 // supprime un etudiant
        $scope.supprimeEtu = function(noEtudiant){ 
      	// TODO Suppression d'un etudiant de la liste
      	  
      	          
            swal({
        		title: "Voulez-vous supprimer cet étudiant ?",
        		type: "warning",   
      		  	showCancelButton: true,   
      		  	confirmButtonColor: "#DD6B55",   
      		  	confirmButtonText: "Oui!",  
      		  	cancelButtonText: "Non!",   
      		  	closeOnConfirm: false,   closeOnCancel: false },
      		  function(isConfirm){
      		  	 if (isConfirm) {       	
      	           	  var promise= etudiantsFactory.delete(noEtudiant);
      	           	  promise.success(function(data,statut){
      	           		//$scope.currentPageRubrique.removeValue("idRubrique",rubrique.idRubrique);
      	           		swal("Supprimé!", "Etudiant supprimé", "success");
      	          // 		$scope.refresh();
      	          	  $scope.currentPageEtudiant.removeValue("noEtudiant",noEtudiant);

      	           	  });
      	           	promise.error(function(data,statut, headers, config){
      	           	swal("Erreur!", "Impossible de supprimer l'étudiant choisi", "error");
            });
      		  	 }else{
      		  		swal("Ignorer", "", "error");
      		  		}
      		  	 });
      	  
        }
    	
    	 $scope.edite = function (etud){
       	  etudService.etud = etud;
       	  $location.path("/admin/etudiant/"+ etud.noEtudiant);
       	  //alert(etudiant.no_etudiant);
         }
    	
    	
    	
    	
      //Traitement étudiants
    	
    	$scope.ajoutEtudiant = function(){
            $location.path('/admin/etudiant/nouveau'); 
         }
    	
    	
    	//modification etudiant
    	$scope.modifEtu = function(etud){
    		etudService.etud = etud;
              $location.path('/admin/etudiant/modif'); 
           }

      $scope.afficherEtudiants = function(data){
    	  
    	  //Show hide
    	  promoNewEtu = data; 
    	  if($scope.IsHiddenEtu == true && data.promotionPK == numShow){
    		  $scope.IsHiddenEtu=false;
    	  }
    	  else{
	    	  $scope.IsHiddenEtu = true;
    	  } 
    	  
    	  numShow = data.promotionPK;
    	  //Traitement
    	  var promise44 = promoFactory.getEtuPromo(data.promotionPK.codeFormation,data.promotionPK.anneeUniversitaire);
      	promise44
  		.success(function(data) {
  		    $scope.etudiants = data;

  		    // Pour changer les codes
  		    
  		    $scope.pays = rvDomainService.pays;
  		  	$scope.universite = rvDomainService.universite;
		  
		  for(var i=0; i<$scope.etudiants.length; i++){
        		for(var j=0; j<$scope.pays.length; j++){
        			if($scope.etudiants[i].paysOrigine == $scope.pays[j].rvAbbreviation){
        				
        				$scope.etudiants[i].paysOrigine = $scope.pays[j].rvMeaning;
        				
        			}
        		}
        		
        		for(var j=0; j<$scope.universite.length; j++){
        			if($scope.etudiants[i].universiteOrigine == $scope.universite[j].rvAbbreviation){
        				
        				$scope.etudiants[i].universiteOrigine = $scope.universite[j].rvMeaning;
        				
        			}
        		}
        	}
  		    
  		    /////////////////////////
		      $scope.searchKeywordsEt = '';
		      $scope.filteredEtudiant = [];
		      $scope.rowEt = '';
		      $scope.selectEt = function(page) {
		        var end, start;
		        start = (page - 1) * $scope.numPerPageEt;
		        end = start + $scope.numPerPageEt;
		        return $scope.currentPageEtudiant = $scope.filteredEtudiant.slice(start, end);
		      };
		      $scope.onFilterChangeEt = function() {
		        $scope.selectEt(1);
		        $scope.currentPageEt = 1;
		        return $scope.rowEt = '';
		      };
		      $scope.onNumPerPageChangeEt = function() {
		        $scope.selectEt(1);
		        return $scope.currentPageEt = 1;
		      };
		      $scope.onOrderChangeEt = function() {
		        $scope.selectEt(1);
		        return $scope.currentPageEt = 1;
		      };
		      $scope.searchEt = function() {
		        $scope.filteredEtudiant = $filter('filter')($scope.etudiants, $scope.searchKeywordsEt);
		        return $scope.onFilterChangeEt();
		      };
		      $scope.orderEt = function(rowName) {
		        if ($scope.rowEt === rowName) {
		          return;
		        }
		        $scope.rowEt = rowName;
		        $scope.filteredEtudiant = $filter('orderBy')($scope.etudiants, rowName);
		        return $scope.onOrderChangeEt();
		      };
		      $scope.numPerPageOptEt = [3, 5, 10, 20];
		      $scope.numPerPageEt = $scope.numPerPageOptEt[2];
		      $scope.currentPageEt = 1;
		      $scope.currentPageEtudiant = [];
		      init = function() {
		        $scope.searchEt();
		        return $scope.selectEt($scope.currentPageEt);
		      };
		      return init();
		  }
		)
		.error(function(data) {
			 $scope.error = 'unable to get the poneys';
		  }
		);

      }

      
     
      // affiche les détail d'un enseignant
      // TODO Lien vers la page permettant d'éditer un enseignant /admin/enseignant/ + no_enseignant
      $scope.edit = function (promotion){
    	  promoService.pro = promotion;
    	  $location.path("/admin/promotion/detail");
      }

 
      
      $scope.ajoutPromotion = function(){
          $location.path("/admin/promotion/nouveau"); 
        }
    }]
  );

  app.controller('PromoDetailsController', 
    ['$scope', '$routeParams', '$location','promoFactory','$http','rvDomainService','promoService',
    function($scope, $routeParams, $location,promoFactory,$http,rvDomainService,promoService){      
    	 $scope.edit= false;
    	var p=promoFactory.getFormations();
    	p.success(function(data){
    		$scope.formations=data;
    	});
    	 
    	p.error(function(data)
    	{
    		$scope.error='unable!';
    	});
    	
    	 
    	 // si creation d'une nouvelle promotion
	      if($routeParams.id == "nouveau"){
	        $scope.promo= { };
	        $scope.proces = rvDomainService.processus;
	        var promise = promoFactory.allEns();
	    	promise.success(function(data) {
	    		$scope.ens = data;
	    		    console.log("Dans détail "+$scope.ens[0].nom);
	    	});
	    	promise.error(function(data) {
	    		$scope.error = 'unable to get the poneys';
	    	});
	        $scope.edit= true;
	      } 
	      
	      if($routeParams.id == "detail") {
	    	  
				$scope.promo = promoService.pro ;
	      }
	     
	      $scope.submit = function(){
	    	  promoFactory.set($scope.promo);        
	          $scope.edit = false;  
	          $location.path('/admin/promotions/'); 
	        }
	      
	      $scope.cancel = function(){
	          $scope.edit = false;  
	          $location.path('/admin/promotions/');
	      }
	      
	      
    }]);
}).call(this);


