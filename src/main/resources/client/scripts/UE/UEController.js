(function() {
  'use strict';

  var app = angular.module('app.ue', []);

  app.factory('ueFactory', ['$http', function($http){
	  return {// renvoi la liste de tous les ue
	      all: function() { // TODO retourner la liste 
	    	  return  $http.get('http://localhost:8090/getAllUe')
	      },

	     };
	    
  }]);
  
  app.service('ueService',function(){
	var ue ;
  });
  
  
  app.controller('UEController', 
    ['$scope', '$location', 'ueFactory','$http','ueService',
    function($scope, $location, ueFactory,$http,ueService){
    	
    	var promise = ueFactory.all();
    	promise.success(function(data) {
    		    $scope.ue = data;
    	});
    	promise.error(function(data) {
    			 $scope.error = 'unable to get the poneys';
    	});
    	
    	// affiche les détails d'une ue
        $scope.edit = function(ue){
        	ueService.ue = ue;
          $location.path("/admin/ue/"+ ue.uniteEnseignementPK.codeFormation +"/"+ue.uniteEnseignementPK.codeUe);
        }
    }]);
  	//Details UE
  
  app.controller('UEDetailsController', 
		  ['$scope', '$routeParams', '$location', '$filter','ueFactory','ueService',
		    function($scope, $routeParams, $location, $filter, ueFactory,ueService){
			  
			  //$scope.ue = ueService.ue;
			  $scope.edit= false; 
		      // si creation d'une nouvelle ue à partir d'une formation
		      if($routeParams.codeUe == "nouveau"){
		        $scope.ue= { };
		        $scope.edit= true;    
		      } else { // sinon on edite une formation existante
		        //var f = formationsFactory.get($routeParams.id);
		        // clone de l'objet pour conserver l'objet initial
		        //$scope.formation = JSON.parse(JSON.stringify(f));
//		        var promise = ueFactory.get($routeParams.codeFormation+"-"+$routeParams.codeUe);
//		        promise
//				.success(function(data,status) {
//					$scope.ue = data ;
////					$scope.formation.debutAccreditation = $filter('date')(data.debutAccreditation, "dd-MM-yyyy");
////					$scope.formation.finAccreditation = $filter('date')(data.finAccreditation, "dd-MM-yyyy");	
////		        	console.log("Dans FormationDetailsController : success : "+$scope.formation.nomFormation);
//					//$scope.formation = JSON.parse(JSON.stringify(data));
//				  }
				//)
//		 .error(function(data,status){
//	        	console.log("erreur de recupere la formation choisi");
//	      });
      }      

      $scope.edition = function(){
        $scope.edit = true;
      }

      // valide le formulaire d'édition d'une formation
      $scope.submit = function(){
    	  ueFactory.set($scope.ue);        
          $scope.edit = false;  
          $location.path('/admin/ue'); 
        }
      
      // TODO coder une fonction submit permettant de modifier une formation et rediriger vers /admin/formations 


      // annule l'édition
      $scope.cancel = function(){ 
    	  $scope.ue = {};
          $location.path('/admin/ue');
    } 
      // TODO coder une fonction cancel permettant de modifier une ue et rediriger vers /admin/ue 

    }]
  );
  	
})();