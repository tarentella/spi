(function() {
	'use strict';
	var app = angular.module(
			'app',
			[ 'ngRoute', 'ngAnimate', 'ui.bootstrap', 'easypiechart',
					'mgo-angular-wizard', 'textAngular', 'ui.tree',
					'app.promotions',
					'ngTagsInput', 'app.authentication', 'app.evaluations', 'app.enseignants',
					'app.formations', 'app.ue', 'app.parametrage' , 'app.ui.ctrls',
					'app.ui.directives', 'app.ui.services', 'app.controllers',
					'app.directives', 'app.form.validation',
					'app.ui.form.ctrls', 'app.ui.form.directives',
					'app.tables', 'app.task', 'app.localization',
					'app.chart.ctrls', 'app.chart.directives',
					'app.page.ctrls', 'app.auth' ]).config(
			[ '$routeProvider', function($routeProvider, $urlRouterProvider) {
				return $routeProvider.when('/', {
					redirectTo : '/dashboard'
				}).when('/admin/qualificatif/:id', {
					templateUrl : 'views/parametrage/details.html'
				}).when('/admin/questions', {
					templateUrl : 'views/parametrage/listques.html'	
				}).when('/admin/etudiant/:id', {
					templateUrl : 'views/etudiants/details.html'	
				}).when('/admin/promotions/', {
					templateUrl : 'views/promotions/list.html'
				}).when('/admin/promotion/:id', {
					templateUrl : 'views/promotions/details.html'
				}).when('/admin/rubriques', {
					templateUrl : 'views/parametrage/listrub.html'		
				}).when('/admin/qualificatifs', {
					templateUrl : 'views/parametrage/listqual.html'
				}).when('/admin/rubrique/:id', {
					templateUrl : 'views/parametrage/detailsRubrique.html'
				}).when('/admin/evaluations', {
					templateUrl : 'views/evaluation/list.html'
				}).when('/admin/evaluations/ajoutRub', {
					templateUrl : 'views/evaluation/ajoutRub.html'
				}).when('/admin/evaluation/:id', {
					templateUrl : 'views/evaluation/ajoutEvaluation.html'
				}).when('/admin/parametrage/question/:id', {
					templateUrl : 'views/parametrage/Detail.html'		
				}).when('/admin/parametrage', {
					templateUrl : 'views/parametrage/parametrage.html'
				}).when('/admin/enseignants', {
					templateUrl : 'views/enseignants/list.html'
				}).when('/admin/enseignant/:id', {
					templateUrl : 'views/enseignants/details.html'
				}).when('/admin/rubrique/:id', {
					templateUrl : 'views/parametrage/detailsRubrique.html'
				}).when('/admin/formations', {
					templateUrl : 'views/formations/list.html'
				}).when('/admin/ec/:codeFormation/:codeUe/:codeEc', {
					templateUrl : 'views/formations/detailsEC.html'
				}).when('/admin/formation/:id', {
					templateUrl : 'views/formations/details.html'
				}).when('/admin/ue/:id', {
					templateUrl : 'views/ue/details.html'
				}).when('/admin/formation/ue/:id', {
					templateUrl : 'views/ue/details.html'
				}).when('/admin/formation/ajoutue/:id', {
					templateUrl : 'views/formations/ajoutUE.html'
				}).when('/admin/formation/ajoutec/:id', {
					templateUrl : 'views/formations/ajoutEC.html'
				}).when('/admin/ue', {
					templateUrl : 'views/ue/list.html'
				}).when('/admin/ue/:codeFormation/:codeUe', {
					templateUrl : 'views/ue/details.html'
				}).when('/dashboard', {
					templateUrl : 'views/dashboard.html'
				}).when('/admin/parametrage', {
					templateUrl : 'views/parametrage/parametrage.html'
				}).when('/ui/typography', {
					templateUrl : 'views/ui/typography.html'
				}).when('/ui/buttons', {
					templateUrl : 'views/ui/buttons.html'
				}).when('/ui/icons', {
					templateUrl : 'views/ui/icons.html'
				}).when('/ui/grids', {
					templateUrl : 'views/ui/grids.html'
				}).when('/ui/widgets', {
					templateUrl : 'views/ui/widgets.html'
				}).when('/ui/components', {
					templateUrl : 'views/ui/components.html'
				}).when('/ui/timeline', {
					templateUrl : 'views/ui/timeline.html'
				}).when('/ui/nested-lists', {
					templateUrl : 'views/ui/nested-lists.html'
				}).when('/ui/pricing-tables', {
					templateUrl : 'views/ui/pricing-tables.html'
				}).when('/forms/elements', {
					templateUrl : 'views/forms/elements.html'
				}).when('/forms/layouts', {
					templateUrl : 'views/forms/layouts.html'
				}).when('/forms/validation', {
					templateUrl : 'views/forms/validation.html'
				}).when('/forms/wizard', {
					templateUrl : 'views/forms/wizard.html'
				}).when('/tables/static', {
					templateUrl : 'views/tables/static.html'
				}).when('/tables/responsive', {
					templateUrl : 'views/tables/responsive.html'
				}).when('/tables/dynamic', {
					templateUrl : 'views/tables/dynamic.html'
				}).when('/charts/others', {
					templateUrl : 'views/charts/charts.html'
				}).when('/charts/morris', {
					templateUrl : 'views/charts/morris.html'
				}).when('/charts/flot', {
					templateUrl : 'views/charts/flot.html'
				}).when('/mail/inbox', {
					templateUrl : 'views/mail/inbox.html'
				}).when('/mail/compose', {
					templateUrl : 'views/mail/compose.html'
				}).when('/mail/single', {
					templateUrl : 'views/mail/single.html'
				}).when('/pages/features', {
					templateUrl : 'views/pages/features.html'
				}).when('/pages/signin', {
					templateUrl : 'views/pages/signin.html',
					notLoggedNeeded : true
				}).when('/pages/signup', {
					templateUrl : 'views/pages/signup.html',
					notLoggedNeeded : true
				/*
				 * .when('/pages/signin', { templateUrl:
				 * 'views/pages/signin.html' }) .when('/pages/signup', {
				 * templateUrl: 'views/pages/signup.html'
				 */
				}).when('/pages/forgot', {
					templateUrl : 'views/pages/forgot-password.html'
				}).when('/pages/lock-screen', {
					templateUrl : 'views/pages/lock-screen.html'
				}).when('/pages/profile', {
					templateUrl : 'views/pages/profile.html'
				}).when('/404', {
					templateUrl : 'views/pages/404.html'
				}).when('/pages/500', {
					templateUrl : 'views/pages/500.html'
				}).when('/pages/blank', {
					templateUrl : 'views/pages/blank.html'
				}).when('/pages/invoice', {
					templateUrl : 'views/pages/invoice.html'
				}).when('/pages/services', {
					templateUrl : 'views/pages/services.html'
				}).when('/pages/about', {
					templateUrl : 'views/pages/about.html'
				}).when('/pages/contact', {
					templateUrl : 'views/pages/contact.html'
				}).when('/tasks', {
					templateUrl : 'views/tasks/tasks.html'
				});/*
					 * .otherwise({ redirectTo: '/404' });
					 */
				$urlRouterProvider.otherwise(function($injector, $location) {
					var AuthService = $injector.get('AuthService');

					AuthService.getUser().success(function(data) {
						if (data) {
							$location.path("/dashboard");
						} else {
							$location.path("/pages/signin");
						}

					}).error(function(data) {
						$location.path("/pages/signin");
					});

				});
			} ]).service('rvDomainService',function(){
				   this.diplome = {};
				   this.ouinon = {};
				   this.semestre = {};
				   this.pays = {};
				   this.universite = {};
				   this.processus = {};
				   this.type = {};
				   this.etat = {};
			  }).factory('rvDomainFactory',['$http', function($http){
				return {
					getDiplome: function() { 
				      	  return  $http.get('http://localhost:8090/getRvDomain/DIPLOME');
				        },
				        
			        getOuiNon: function() { 
				      	  return  $http.get('http://localhost:8090/getRvDomain/OUI_NON');
				        },
				        
			        getSemestre: function() { 
				      	  return  $http.get('http://localhost:8090/getRvDomain/SEMESTRE');
				        },
				        
			        getPays: function() { 
				      	  return  $http.get('http://localhost:8090/getRvDomain/PAYS');
				        },
				        
			        getUniversite: function() { 
				      	  return  $http.get('http://localhost:8090/getRvDomain/UNIVERSITE');
				        },
				        
			        getProcess: function() { 
				      	  return  $http.get('http://localhost:8090/getRvDomain/PROCESSUS_STAGE');
				        },
				        
			        getType: function() { 
				      	  return  $http.get('http://localhost:8090/getRvDomain/TYPE_ENSEIGNANT');
				        },
				        
			        getEtat: function() { 
				      	  return  $http.get('http://localhost:8090/getRvDomain/ETAT-EVALUATION');
				        }
				};
			}]).run(function($rootScope, $route, $location, AuthService, rvDomainFactory, rvDomainService) {
		$rootScope.$on("$routeChangeStart", function(e, to) {
			if (to.notLoggedNeeded) {
				return;
			}
			AuthService.getUser().success(function(data) {
				if (data) {
					e.preventDefault();
					$rootScope.user=data;
					
					
				} else {
					$location.path("/pages/signin");
				}
			}).error(function(data) {
				$location.path("/pages/signin");
			});
			
			// Domaine Diplome
			var maPromise = rvDomainFactory.getDiplome();
				maPromise.success(function(data){
					rvDomainService.diplome = data;
					console.log("Dans app.js : "+rvDomainService.diplome.length);
				});
				
				maPromise.error(function(error){
					console.log("Diplome non récupéré !!! "+error);
				});
				
			// Domaine OuiNon	
			var maPromise1 = rvDomainFactory.getOuiNon();
				maPromise1.success(function(data){
					rvDomainService.ouinon = data;
					console.log("Dans app.js : "+rvDomainService.ouinon.length);
				});
				
				maPromise1.error(function(error){
					console.log("Double diplome non récupéré !!! "+error);
				});
				
			// Domaine Semestre	
			var maPromises = rvDomainFactory.getSemestre();
				maPromises.success(function(data){
					rvDomainService.semestre = data;
					console.log("Dans app.js : "+rvDomainService.semestre.length);
				});
				
				maPromises.error(function(error){
					console.log("Semestre non récupéré !!! "+error);
				});
				
				
			// Domaine Pays	
			var maPromise3 = rvDomainFactory.getPays();
				maPromise3.success(function(data){
					rvDomainService.pays = data;
					console.log("Dans app.js : "+rvDomainService.pays.length);
				});
				
				maPromise3.error(function(error){
					console.log("Semestre non récupéré !!! "+error);
				});
				
				
			// Domaine Université	
			var maPromise4 = rvDomainFactory.getUniversite();
				maPromise4.success(function(data){
					rvDomainService.universite = data;
					console.log("Dans app.js : "+rvDomainService.universite.length);
				});
				
				maPromise4.error(function(error){
					console.log("Semestre non récupéré !!! "+error);
				});
				
				
			// Domaine Processus Stage	
			var maPromise5 = rvDomainFactory.getProcess();
				maPromise5.success(function(data){
					rvDomainService.processus = data;
					console.log("Dans app.js : "+rvDomainService.processus.length);
				});
				
				maPromise5.error(function(error){
					console.log("Semestre non récupéré !!! "+error);
				});
				
				
			// Domaine Type Enseignant	
			var maPromise6 = rvDomainFactory.getType();
				maPromise6.success(function(data){
					rvDomainService.type = data;
					console.log("Dans app.js : "+rvDomainService.type.length);
				});
				
				maPromise6.error(function(error){
					console.log("Semestre non récupéré !!! "+error);
				});
				
				
			// Domaine Etat Evaluation	
			var maPromise7 = rvDomainFactory.getEtat();
				maPromise7.success(function(data){
					rvDomainService.etat = data;
					console.log("Dans app.js : "+rvDomainService.etat.length);
				});
				
				maPromise7.error(function(error){
					console.log("Semestre non récupéré !!! "+error);
				});
		});
	});

}).call(this);
