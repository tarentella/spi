(function() {
  'use strict';
  angular.module('app.controllers', ['app.auth']).controller('AppCtrl', [
    '$scope', '$location', 'AuthService', '$rootScope',function($scope, $location, AuthService, $rootScope) {
      $scope.isSpecificPage = function() {
        var path;
        path = $location.path();
        return _.contains(['/404', '/pages/500', '/pages/login', '/pages/signin', '/pages/signin1', '/pages/signin2', '/pages/signup', '/pages/signup1', '/pages/signup2', '/pages/forgot', '/pages/lock-screen'], path);
      };
      $scope.deconnexion = function() {
    	  
    	  AuthService.deconnexion().success(function() {
    		  delete $rootScope.user ;
    		  $location.path('/pages/signin');
    	  });
    	  
      };
      
      $scope.login = function() {
    	  $location.path('/pages/signin');
      };
      
      return $scope.main = {
        brand: 'Square',
        name: 'Lisa Doe'
      };
    }
  ]).factory('nombreFactory',['$http', function($http){
	  
	  return {
	      // renvoi la liste de tous les enseignants
	      allEtu: function() { 
	   	  		return $http.get("http://localhost:8090/nbrEtu")
	    	  					
	      },
	      
	      allFrm: function() { 
				return $http.get("http://localhost:8090/nbrFrm")
	      },
	      
	      allUe: function() { 
				return $http.get("http://localhost:8090/nbrUe")
	      },
		  
		  allEns: function() { 
				return $http.get("http://localhost:8090/nbrEns")
		  }
	    };
	  
	  
  	}
  ]).controller('NavCtrl', [
    '$scope','taskStorage', 'filterFilter', '$http', 'AuthService', '$routeParams','$rootScope', function($scope, taskStorage, filterFilter, $http, AuthService, $routeParams,$rootScope) {
      var tasks;
      tasks = $scope.tasks = taskStorage.get();
      $scope.taskRemainingCount = filterFilter(tasks, {
        completed: false
      }).length;
      return $scope.$on('taskRemaining:changed', function(event, count) {
        return $scope.taskRemainingCount = count;
      });
    }
  ]).controller('DashboardCtrl', ['$scope','nombreFactory', function($scope,nombreFactory) {
	  var $maPromise1 = nombreFactory.allEtu();
	  	  $maPromise1.success(function(data,statut){
	  		  $scope.nbrEtu = data;
	  	  });
	  	$maPromise1.error(function(error){
	  		  console.log(error);
	  	  });
	  	
	  	var $maPromise2 = nombreFactory.allFrm();
	  	  $maPromise2.success(function(data,statut){
	  		  $scope.nbrFrm = data;
	  	  });
	  	$maPromise2.error(function(error){
	  		  console.log(error);
	  	  });
	  	
	  	var $maPromise3 = nombreFactory.allUe();
	  	  $maPromise3.success(function(data,statut){
	  		  $scope.nbrUe = data;
	  	  });
	  	$maPromise3.error(function(error){
	  		  console.log(error);
	  	  });
	  	
	  	var $maPromise4 = nombreFactory.allEns();
	  	  $maPromise4.success(function(data,statut){
	  		  $scope.nbrEns = data;
	  	  });
	  	$maPromise4.error(function(error){
	  		  console.log(error);
	  	  });
	  	  
  }]);

}).call(this);

