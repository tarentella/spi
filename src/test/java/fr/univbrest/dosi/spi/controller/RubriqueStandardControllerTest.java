package fr.univbrest.dosi.spi.controller;

import static org.junit.Assert.*;

import java.io.IOException;
import java.math.BigInteger;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import fr.univbrest.dosi.spi.bean.Rubrique;

public class RubriqueStandardControllerTest {

	@Test
	public void testIntegrationAddRubrique() throws ClientProtocolException, IOException{
		
//		Rubrique rubrique = new Rubrique();
//		BigInteger myOrdre = new BigInteger("10");
//		
//		rubrique.setIdRubrique(10l);
//		rubrique.setType("RBS");
//		rubrique.setNoEnseignant(null);
//		rubrique.setDesignation("CM/TD/CM/TP");
//		rubrique.setOrdre(myOrdre);
		
		Rubrique rubrique = new Rubrique(10l, "RBS", "CM/TD/CM/TP");
		
		
		// Création d'un client HTTP qui servira de plateforme pour envoyer nos requêtes HTTP
		final HttpClient client = HttpClientBuilder.create().build();
		
		final HttpPost httpPost = new HttpPost("http://localhost:8090/ajouterRubrique");
		httpPost.addHeader("content-type","application/json");
		
		final ObjectWriter mapperWrite = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
		String monObjetJson = mapperWrite.writeValueAsString(rubrique);
		
		httpPost.setEntity(new StringEntity(monObjetJson));
		
		// On lance la requête sur notre client et on reçoit en retour la Réponse HTTP du webservice
		final HttpResponse maResponse = client.execute(httpPost);

		// On commence par tester qu'on a bien un retour OK en vérifiant le code de retour HTTP (qui doit être 200 pour un succès)
		Assert.assertEquals(200, maResponse.getStatusLine().getStatusCode());
	}

}
