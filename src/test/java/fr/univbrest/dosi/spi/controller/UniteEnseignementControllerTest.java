package fr.univbrest.dosi.spi.controller;

import java.io.IOException;

import junit.framework.Assert;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

/**
 * @author DOSI
 *
 */
public class UniteEnseignementControllerTest {

	@Test
	public final void getAllUeTest() throws ClientProtocolException, IOException {

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpGet mockRequest = new HttpGet("http://localhost:8090/getAllUe");
		final HttpResponse mockResponse = client.execute(mockRequest);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		// final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		// final ObjectMapper mapper = new ObjectMapper();
		// final Iterable<UniteEnseignement> ue = mapper.readValue(rd, Iterable.class);
		//
		// Assert.assertNotNull(ue);

	}
}
