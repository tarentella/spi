package fr.univbrest.dosi.spi.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Qualificatif;

/**
 * @author DOSI
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class QualificatifServiceTest {
	Long codeQualif;
	Qualificatif qualif;
	/**
	 *
	 */
	@Autowired
	private QualificatifService qualificatifService;

	@Test
	public void addQualif() {
		try {
			// Test de l'ajout
			qualificatifService.addQualificatif(qualif);
			Assert.assertNotNull(qualificatifService.getQualificatif(qualif.getIdQualificatif()));

			// Test d'ajout d'un existant
			qualificatifService.addQualificatif(qualif);
			Assert.fail("La méthode doit renvoyer une exception");
		} catch (final Exception e) {

		}
	}

	@Test
	public void getAll() {
		final Iterable<Qualificatif> listQualif = qualificatifService.getAll();
		Assert.assertNotNull(listQualif);
	}

	@Test
	public void getQualif() {

		final Qualificatif qualif = qualificatifService.getQualificatif(codeQualif);
		Assert.assertNotNull(qualif);
		Assert.assertEquals(codeQualif, qualif.getIdQualificatif());
	}

	@Before
	public final void init() {
		codeQualif = new Long(14l);
		qualif = new Qualificatif(codeQualif, "max", "min");
	}

//	@Test
//	public void updateQualif() {
//		try {
//			// Test de la modification
//			final String max = "nmax";
//			final String min = "nmin";
//			qualif.setMinimal(min);
//			qualif.setMaximal(max);
//			qualificatifService.updateQualificati(qualif);
//			Assert.assertEquals(max, qualificatifService.getQualificatif(codeQualif).getMaximal());
//			Assert.assertEquals(min, qualificatifService.getQualificatif(codeQualif).getMinimal());
//
//			// Test modification non existant
//			qualif.setIdQualificatif(100l);
//			qualificatifService.updateQualificati(qualif);
//			Assert.fail("La méthode doit renvoyer une exception");
//		} catch (final Exception e) {
//
//		}
//	}

}
