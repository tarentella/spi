package fr.univbrest.dosi.spi.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.bean.Question;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class QuestionServiceTest {
	Long  idQuestion;
	Question qst;
	Qualificatif qualificatif;
	
	@Autowired
	QuestionService questionService;
	@Autowired
	QualificatifService qualificatifService;
	
	@Test
//	public void addQst(){
//		
//		// test d'ajout d'une question
//		try{
//		questionService.addQuestion(qst);
//		}catch(Exception e){
//			System.out.println("erreur d'insertion");
//			
//		}
//		Assert.assertNotNull(questionService.getQuestion(qst.getIdQuestion()));
//		
//	}
	@Before
	public final void init() {
		//idQuestion = (long) 23;
		qst= new Question((long) 23, "QUS", "bla bla bla !!");
		qualificatif = qualificatifService.getQualificatif((long)1);
		
		qst.setIdQualificatif(qualificatif);
	}
	
}
