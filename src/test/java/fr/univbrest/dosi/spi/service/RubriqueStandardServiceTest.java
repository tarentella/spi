package fr.univbrest.dosi.spi.service;

import java.math.BigInteger;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Iterables;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Rubrique;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class RubriqueStandardServiceTest {

	@Autowired
	RubriqueStandardService rubriqueStandardService;
	
	@Test
	public void testAddRubrique() {
		//fail("Not yet implemented");
		
		Rubrique rubrique = new Rubrique();
		BigInteger myOrdre = new BigInteger("10");
		
		//rubrique.setIdRubrique(9l);
		rubrique.setType("RBS");
		rubrique.setNoEnseignant(null);
		rubrique.setDesignation("CM/TD");
		rubrique.setOrdre(myOrdre);
		
		Rubrique myRubrique = rubriqueStandardService.addRubrique(rubrique);
		
		Assert.assertNotNull(myRubrique);
		
		Assert.assertEquals(myRubrique.getDesignation(), "CM/TD");
		
	}

//	@Test
//	public void testDeleteRubrique() {
//		
//		
//		
//	}
//
	@Test
	public void testUpdateRubrique() {
		
		Rubrique rubrique = new Rubrique();
		BigInteger myOrdre = new BigInteger("9");
		
		rubrique.setIdRubrique(9l);
		rubrique.setType("RBS");
		rubrique.setNoEnseignant(null);
		rubrique.setDesignation("CM/TD/TP");
		rubrique.setOrdre(myOrdre);
		
		Rubrique myRubrique = rubriqueStandardService.updateRubrique(rubrique);
		
		Assert.assertNotNull(myRubrique);
		
		Assert.assertEquals(myRubrique.getDesignation(), "CM/TD/TP");
		
	}

	@Test
	public void testListrub() {
		
		Iterable<Rubrique> mesRubriques = null;
		
		mesRubriques = rubriqueStandardService.listrub();
		
		Assert.assertNotNull(mesRubriques);
		
		Assert.assertEquals(9,Iterables.size(mesRubriques));
		
	}
//
//	@Test
//	public void testGetRubrique() {
//		fail("Not yet implemented");
//	}

}
