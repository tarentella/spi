package fr.univbrest.dosi.spi.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.UniteEnseignement;

/**
 * @author DOSI
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class UniteEnseignementServiceTests {
	/**
	 *
	 */
	@Autowired
	private UniteEnseignementService uniteEnseignementService;

	@Before
	public final void init() {

	}

	@Test
	public void listeEnseignants() {
		final Iterable<UniteEnseignement> listeUE = uniteEnseignementService.getAll();
		Assert.assertNotNull(listeUE);
	}

}
